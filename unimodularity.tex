\chapter{Unimodular Matrices}\label{ch:unimodularity}

In  this chapter  we  will look  at  the special  class of  unimodular
matrices.  The nice  property of such matrices is  that polyhedra that
have a unimodular constraint matrix (and integral right hand side) are
integral.  We  will see in the  next chapter that  many graph problems
can  be described  by a  linear  program with  a (totally  unimodular)
constraint matrix,  and that this property, together  with the duality
theorem, gives  particularly nice and simple proofs  of some important
theorems  in   graph  theory.   In  particular,  we   will  prove  the
MaxCut-MinFlow-Theorem, and \textsc{Menger}'s Theorem.

\begin{definition}\label{def:unimod}
  Let $A\in\Z^{m\times n}$ be an integral matrix.  Let $B$ be a square
  sub-matrix  $B$    of     $A$.       Then   $\det(B)$     is       a
  \emph{minor}\mdef{minor\\maximal   minor     \\totally    unimodular
    \\unimodular} of  $A$.     It   is  a  \emph{maximal minor}     if
  $B\in\Z^{s\times s}$ for $s=\min(m,n)$.
  \begin{compactenum}
  \item  $A\in\Z^{m\times  n}$  is \emph{totally  unimodular}  if  all
    minors are either $0$ or $\pm 1$.
  \item $A$ is  \emph{unimodular} if $\rank\, A =  \min(m,n)$ and each
    maximal minor is $0$ or $\pm 1$.
  \end{compactenum}
\end{definition}
Observe that this is a  generalization of our previous definition of a
unimodular  transformation to  the case  of non-square  matrices.  Let
$A\in\Z^{m\times n}$  be totally unimodular.  The following operations
preserve total unimodularity:
\begin{compactenum}
\item taking the transpose,
\item adding a rwo or column that is a unit vector,
\item deleting a row or column that is a unit vector,
\item multiplying a row or column with $-1$,
\item interchanging two rows or columns,
\item duplicating a row or column.
\end{compactenum}

\begin{proposition}
  A matrix $A\in\Z^{m\times  n}$ is totally unimodular if  and only if
  $\left( \,\id\;\; A\,\right)$ is unimodular.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  ``$\Rightarrow$'': Let  $B$ be  a maximal sub-matrix  of $\left(\id\
    A\right)$.  Let  $J\subseteq[m]$ be the set of  indices of columns
  from $\id$  in $B$.   Let $B'$  be the matrix  obtained from  $B$ by
  deleting the  first $|J|$  columns and the  rows with index  in $J$.
  Using   the    Laplace   formula   for    determinants   we   obtain
  $\eps\in\{0,1\}$ with
  \begin{align*}
    \det(B)=(-1)^\eps\det(B').
  \end{align*}
  By  total    unimodularity of    $A$,       $\det(B')\in\{0,\pm1\}$.
  
  ``$\Leftarrow$'': Let $B$ be a  square sub-matrix of $A$ and let $J$
  be the  row indices missed by  $B$.  Let $B'$ be  the maximal square
  sub-matrix  obtained   by  taking   the  complete  columns   of  $A$
  corresponding  to columns  of $B$  and the  unit vectors  $e_j$, for
  $j\in J$,  from $\id$.   Again using the  Laplace formula,  there is
  $\eps\in\{0,\pm 1\}$, such that
  \begin{align*}
    \det(B)&=(-1)^\eps\det(B')\in\{0,\pm1\}\,.
  \end{align*} %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{theorem}\label{thm:poly-integral}
  Let $P=\{\vx\mid  A\vx\le \vb\}$  for $A\in\Z^{m\times n}$  and $\vb
  \in\Z^m$.  If $A$ is totally unimodular, then $P$ is integral.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Let $I\subseteq[m]$ correspond to a  minimal face $F$ of $P$. We may
  assume that $I$  is a minimal system defining  $F$, so that $A_{I*}$
  has full rank.  By reordering  the columns of $A_{I*}$ we may assume
  that  $A_{I*}=\left(\, U\;\; V\,\right)$  for a  non-singular matrix
  $U$.  Since $A$ is totally unimodular, $\det(U)=\pm 1$ and
  \begin{align*}
    \vx&=\left(
      \begin{array}{c}
        U^{-1}\vb_I\\0
      \end{array}\right)\ \in \ \Z^n
  \end{align*}
  is integral.  Hence, the face $F$ contains an integral point.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{corollary}\label{cor:poly-integral}
  Let  $A\in\Z^{m\times n}$, $\vb\in\Z^m$,  and $\vc\in  \Z^n$. Assume
  that $A$ is totally unimodular. Then
   \begin{align*}
     \max(\vc^t\vx\mid  A\vx\le \vb)=\min(\vy^t\vb\mid \vy^tA=\vc^t,\,
     \vy\ge \0)
   \end{align*}
   with integral optimal solutions.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Integrality  of the primal  problem is  immediate from  the previous
  Theorem~\ref{thm:poly-integral}.  We can  represent the dual program
  using the matrix
  \begin{align*}
    \ov A:=\left[\begin{array}{r}
      A^t\phantom{_m}\\-A^t\phantom{_m}\\ -\id
    \end{array}\right]
  \end{align*}
  which is also totally  unimodular. So also  the dual has an integral
  optimal solution.  The equality is just the LP-duality relation.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%


\begin{theorem}\label{thm:standardisintegral}
  Let  $A\in\Z^{m\times  n}$   and  $\rank(A)=m$.   Then  $P=\{\vx\mid
  A\vx=\vb,\,  \vx\ge \0\}$ is  integral for  any $\vb\in\Z^m$  if and
  only if $A$ is unimodular.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Suppose  first that  $A$ is  unimodular and  that  $\vb\in\Z^m$.  By
  Corollary~\ref{cor:pointed-polyhedra}\,(\ref{item:cor:pointed-std}),
  $P$ is  pointed, so any  minimal face is  a vertex.  Let $\vx$  be a
  vertex of  $P$.  Let $I:=\{i\mid x_i>0\}$.   By our characterization
  of    vertices   of    polyhedra   in    standard    form,   Theorem
  \ref{thm:vertexstandard},  the  columns  of  $A_{*I}$  are  linearly
  independent. Hence, we can extend $I$ to $I'$ such that $B:=A_{*I'}$
  is  a maximal non-singular  square sub-matrix,  and $\vx=B^{-1}\vb$.
  But     $\det(B)=\pm1$,     so      $\vx$     is     integral     by
  Lemma~\ref{prop:integral-sols}.

  Now suppose that $P$ is integral whenever $\vb$ is integral, and let
  $B$  be  a  maximal   non-singular  sub-matrix.   We  will  use  the
  characterization  of  Lemma~\ref{lemma:integral-sols}  to show  that
  $\det B=\pm 1$.   So we need to show that  for any $\vz\in\Z^m$ also
  $B^{-1}\vz$ is integral.  Let  $\vz\in\Z^m$ and choose $\vy\in \Z^m$
  such that $\vx:=\vy+B^{-1}\vz\ge \0$.  Let
  \begin{align*}
    \vb:=B\vx=B\vy+\vz\in\Z^m\,.
  \end{align*}
  We may extend  $\vx$ with zeros to a vector $\ov  \vx$ so that $A\ov
  \vx=B\vx=\vb$. So $\ov \vx\in P$, as by construction $\ov\vx\ge\0$.

  Let $I:=\{i\in[n]\mid  \ov x_i>0\}$.  The columns of  $A_{*I}$ are a
  subset    of    those     of    $B$,    so    $\rank    A_{*I}=|I|$.
  Theorem~\ref{thm:vertexstandard} now implies  that $\vx$ is a vertex
  of     $P$.     By     assumption,    $P$     is     integral,    so
  $B^{-1}\vz=\vx-\vy\in\Z^m$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
This  theorem implies  integrality also  for other  representations of
polyhedra.
\begin{proposition}[Theorem      of      Hoffman     and      Kruskal,
  1956]\label{prop:totunimod-canpoly}
  Let $A\in\Z^{m\times n}$.  The following statements are equivalent:
  \begin{compactenum}
  \item\label{item:thm:fcp1} $A$ is totally unimodular.
  \item\label{item:thm:fcp2} $P:=\{\vx\mid A\vx\le \vb,\, \vx\ge \0\}$
    is integral for any $\vb\in\Z^m$.
  \item\label{item:thm:fcp3}   $P:=\{\vx\mid  \va\le   A\vx\le  \vb,\,
    \vc\le  \vx\le  \vd\}$  is   integral  for  any  $\va,  \vb,  \vc,
    \vd\in\Z^m$.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  $(\ref{item:thm:fcp1})  \Leftrightarrow  (\ref{item:thm:fcp2})$: $A$
  is totally  unimodular  if and only  if $\left(A\   \ \id\right)$ is
  unimodular,             and       by          the           previous
  Theorem~\ref{thm:standardisintegral},   $\left(A\ \ \id\right)$   is
  unimodular if and only if
  \begin{align*}
    Q:=\{(\vx,\vy)\mid  A\vx+\vy=\vb,\, \vx,\vy\ge \0\}
  \end{align*}
  is integral for any right hand side $\vb\in\Z^m$.  So we are done if
  we can show that
  \begin{align*}
    \text{$\vx$           is           a           vertex           of
      $P$}\quad\Longleftrightarrow\quad\text{$(\vx,\vb-A\vx)$   is   a
      vertex of $Q$.}
  \end{align*}
  The        polyhedron        $P$        is        pointed,        by
  Corollary~\ref{cor:pointed-polyhedra}(\ref{item:cor:pointed-can}).
  By our characterization  of vertices, $\vx\in P$ is  a vertex of $P$
  if and  only if $\vx$ satisfies $n$  linear independent inequalities
  of
  \begin{align*}
    \left[\begin{array}{r} A\phantom{_m}\\ -\id
      \end{array}\right]\, \vx\ \le \ \left[
    \begin{array}{r}
      \vb\\ \0
    \end{array}\right]
  \end{align*}
  with equality.  Let $B$ be such a set of $n$ inequalities.  Let
  \begin{align*}
    I&:=B\cap    [m]\,,    && &
    J&:=\left\{j-m\mid j\in B\cap\{m+1,\ldots, m+n\}\right\}\,,\\
    K&:=[n]\setminus    J    &&\text{and}& \ov I&:=[m]\setminus I\,.
  \end{align*}
  Then  $\vx_J=\0$ and  $A_{I*}\vx=\vb_I$, and  the  square sub-matrix
  $A_{IK}$  has full rank  $|I|=|K|$. This  implies that  also $(A\mid
  \id)_{K\cup \ov I}$ has full rank $m$.  Define $\vy\in\Z^m$ by
  \begin{align*}
    \vy_I&:=\0&&\text{and}& \vy_{\ov I}&:=\vb_{\ov I}-A_{\ov I*}\vx\,.
  \end{align*}
  Then   $(\vx,\vy)_{K\cup   \ov    I}=(\vx_K,   \vy_{\ov   I})$   and
  $\vx,\vy)_{J\cup  I}=\0$.  By  our characterization  of  vertices of
  polyhedra  in  standard  form in  Theorem  \ref{thm:vertexstandard},
  $(\vx,\vy)$ is a vertex of  $Q$, so $\vx$ is integral.  The converse
  direction is similar.

  $(\ref{item:thm:fcp1})  \Leftrightarrow (\ref{item:thm:fcp3})$: This
  follows  from  a  simple   translation  of  $P$  and  rewriting  the
  inequalities in the following form:
  \begin{align*}
    P=\{\vx+\vc\mid  \va-A\vc\le   A\vx\le  \vb-A\vc,\,  \0\le  \vx\le
    \vd-\vc\} =\{\vx\mid M\vx\le \vm, \vx\ge \0\}
  \end{align*}
  \begin{flalign*}
    &\text{for}&M&:=\left(
      \begin{array}{r}
        A\phantom{_m}\\-A\phantom{_m}\\\id
      \end{array}\right)&
    \vm&:=\left(
      \begin{array}{r}
        \vb-A\vc\\A\vc-\va\\\vd-\vc
      \end{array}\right)\,.&
  \end{flalign*}
  Now $M$ is  unimodular if and only  if $A$ is  unimodular, so we can
  use (\ref{item:thm:fcp2}).
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
A  direct  consequence  of   the  second  statement  of  the  previous
proposition is  the following characterization  of total unimodularity
by the integrality of solutions of a linear program.
\begin{corollary}
  Let  $A\in\Z^{m\times n}$. Then  $A$ is  totally unimodular,  if and
  only if  for all  $\vb\in\Z^m$, $\vc\in\Z^m$, the  optimal solutions
  $\ov \vx$ and $\ov \vy$ of
  \begin{align*}
    \max(\vc^t\vx\mid  A\vx\le   \vb,\,  \vx\ge  \0)=\min(\vy^t\vb\mid
    \vy^tA\ge \vc^t,\, \vy\ge \0)
  \end{align*}
  are integral, if the values are finite.\mqed
\end{corollary}

With the next theorems we  study criteria that ensure unimodularity of
a  matrix $A$. In  particular we  will see  the incidence  matrices of
bipartite graphs and general directed graphs are always unimodular.
\begin{theorem}[Ghouila, Houri 1962]\label{thm:furthercharactunimod}
  Let $A\in\Z^{m\times n}$. Then $A$ is totally unimodular if and only
  if  for each  $J\subseteq [n]$  there is  $\vxi\in\{0,\pm1\}^n$ with
  $J=\{j\mid \xi_j\ne 0\}$ and
  \begin{align*}
    A\vxi\ \in\ \{0,\pm 1\}^m\,.
  \end{align*}
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  ``$\Rightarrow$'':    Let   $A$    be    totally   unimodular    and
  $\vchi\in\{0,1\}^n$   the  characteristic   vector  of   the  chosen
  collection of columns of $A$.  Consider the polyhedron
  \begin{align*}
    Q:=\{\vx\in\R^n\mid   \lfloor\textstyle\frac12  A\vchi\rfloor  \le
    A\vx  \le  \lceil\textstyle\frac12  A\vchi\rceil,\,  \0\le  \vx\le
    \vchi\,\}\,.
  \end{align*}
  Then $\frac12\vchi\in Q$, so $Q\ne\varnothing$.  Further, $Q$ is
  pointed by
  Theorem~\ref{cor:pointed-polyhedra}(\ref{item:cor:pointed-can}).  So
  $Q$ has a vertex $\vx$. $\vx$ is integral by assumption and $\0\le
  \vx\le \vchi$, so $\vx$ is a $0/1$-vector. Let
  \begin{align*}
    \vxi:=\vchi-2\vx\in\{0,\pm1\}^m\,.
  \end{align*}
  Then $\vxi\equiv \vchi (\mod\ 2)$.  Further, $A\vxi = A\vchi-2A\vx$,
  so
  \begin{align*}
    A\vchi - 2\lceil  \textstyle\frac12 A\vchi\rceil\ \le\ A\vxi\ \le\
    A\vchi - 2\lfloor\textstyle\frac12 A\vchi\rfloor\,.
  \end{align*}
  which  implies   that  $A\vxi\in\{0,\pm1\}^m$.   So   $\vxi$  is  an
  incidence vector  of a partition of  the columns as  required in the
  theorem.

  ``$\Leftarrow$'':  We use  induction to  prove that  every $(k\times
  k)$-sub-matrix has  determinant $0$ or  $\pm1$.  If $k=1$  then this
  follows from our assumption that each column has only entries in $0$
  and $\pm1$.  In particular, all entries of $A$ are $0$ or $\pm1$.

  Let $k\ge  2$ and $B$ a  non-singular sub-matrix of  $A$ with column
  vectors $\vb_1,  \ldots, \vb_k$.  By  Cramer's rule, the  entry $\wt
  b_{ij}$ at position $(i,j)$ of the inverse $B^{-1}$ of $B$ is
  \begin{align*}
    \wt b_{ij}=\frac{\det B_{ij}}{\det B}\,.
  \end{align*}
  where  $B_{ij}$ is  the matrix obtained  from $B$  by  replacing the
  $j$-th  column with the   $i$-th  unit vector. Using  the  induction
  hypothesis     and  the  Laplace     rule   we   know   that   $\det
  B_{ij}\in\{0,\pm1\}$. So
  \begin{align*}
    \ov B:=(\det B)B^{-1}
  \end{align*}
  has  only entries  in $\{0,\pm  1\}$.  Let  $\ov \vb$  be  the first
  column of  $\ov B$ and  $I:=\{i\mid \ov b_i\ne 0\}$.   By assumption
  there  is $\vxi\in\{0,\pm1\}^n$ such  that $I=\{i\mid  \xi_i\ne 0\}$
  and $B\xi\in\{0,\pm1\}^m$.

  By  construction, $B \ov  \vb=(\det B)\ve_1$,  where $\ve_1$  is the
  first unit vector. So for $2\le i\le k$ we get
  \begin{align*}
    0 = \ve_i^tB\ov \vb = \sum_{j\in I} \ov b_j b_{ij}.
  \end{align*}
  As $b_{ij}\in\{0,\pm  1\}$ this implies  that for  all $i$  the  set
  $\{j\in I\mid b_{ij}\ne 0\}$ is even.  Hence
  \begin{align*}
    B\vxi\ &=\ \eps \ve_1&&\text{for some $\eps\in\{0,\pm1\}$}
  \end{align*}
  $\eps=0$ would  imply that  $B$ is singular,  so $\eps=\pm  1$.  Now
  $\frac{1}{\det B} B\ov \vb = \ve_1= \pm B\xi$, so
  \begin{align*}
    \vxi = \pm\frac{1}{\det B} \ov \vb\,.
  \end{align*}
  But both $\vxi$ and $\ov  \vb$ have only entries in $\{0,\pm1\}$, so
  $\det B=\pm 1$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{corollary}\label{thm:unimodfordirected}
  Let $M$ be an $(n\times  m)$-matrix with entries in $\{0,\pm1\}$ and
  the property  that each column contains at  most one $1$ and at most
  one $-1$. Then $M$ is totally unimodular.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  We give two proofs of this:
  \begin{compactenum}
  \item The matrix  $M$ is totally unimodular if and  only if $M^t$ is
    totally unimodular.   By assumption, the sum of  any collection of
    column  vectors of  $M^t$ is  a vector  in $\{0,\pm1\}^m$,  so the
    claim follows from Theorem~\ref{thm:furthercharactunimod}.
  \item Let  $B$ a $(k\times  k)$-sub-matrix.  We prove the  result by
    induction on $k$.  The result is obvious if $k=1$, so assume $k\ge
    2$. 

    If $B$ contains  a column with no non-zero  entry then $\det B=0$.
    If $B$ contains a column  with exactly one non-zero entry then the
    result follows  from the Laplace  formula applied to  this column,
    and the induction hypothesis.  Finally, if all columns of $B$ have
    exactly two non-zero  entries then the sum of  the entries in each
    column is $0$, so $\det B=0$.
  \end{compactenum}
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
% \ifproof%
% \begin{proof}%
%   \ifnotpublic%
%   Let    $B$ a $(k\times   k)$-sub-matrix.   We  prove  the result  by
%   induction on $k$.  The  result is obvious if  $k=1$, so assume $k\ge
%   2$. Then one of the following three cases occurs:
%   \begin{compactenum}
%   \item $B$ contains a column with no non-zero entry. Then $\det B=0$.
%   \item  $B$ contains a column with   exactly one non-zero entry. Then
%     the   result follows from   the   Laplace formula applied to  this
%     column, and the induction hypothesis.
%   \item All columns of $B$ have exactly two non-zero entries. Then the
%     sum of the entries in each column is $0$, so $\det B=0$.
%   \end{compactenum}
%   %
% \else\texttt{missing}\fi%
% \end{proof}%
% \fi%

\begin{definition}
  Let  $G=(V,A)$ be a directed graph   with $n$ vertices $v_1, \ldots,
  v_n$   and $m$   edges   $a_1,  \ldots, a_m$.   The  \emph{incidence
    matrix}\mdef{incidence       matrix,       undirected       graph}
  $D_G:=(d_{ij})\in\{0,\pm1\}^{n\times m}$ of $G$ is defined by
  \begin{align*}
    d_{ij}&:=
    \begin{cases}
      \phantom{-}1\qquad&\text{if } a_j \text{ is an outgoing
        arc of } v_i\\
      -1&\text{if } a_j \text{is an incoming arc of } v_i\\
      \phantom{-}0&\text{otherwise}
    \end{cases}
  \end{align*}
\end{definition}
\begin{corollary}\label{cor:incidence-totunimod}
  The  incidence  matrix of  a  directed graph  $G=(V,A)$  is  totally
  unimodular.\mqed
\end{corollary}

\begin{definition}
  Let $G=(V,E)$ be an undirected graph with $n$ vertices $v_1, \ldots,
  v_n$  and  $m$  edges   $e_1,  \ldots,  e_m$.   The  \emph{incidence
    matrix}\mdef{incidence        matrix,        directed       graph}
  $D_G:=(d_{ij})\in\{0,\pm1\}^{n\times m}$ of $G$ is defined by
    \begin{align*}
      d_{ij}&:=
      \begin{cases}
        \phantom{-}1\qquad&\text{if } a_j \text{ is incident to } v_i\\
        \phantom{-}0&\text{otherwise}
      \end{cases}
    \end{align*}
  \end{definition}
\begin{theorem}\label{thm:totunimod_bipartite}
  The incidence  matrix  of an undirected  graph $G=(V,E)$  is totally
  unimodular if and only if the graph is bipartite.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  First assume that $G$ is bipartite.  By  renumbering the vertices of
  the graph we may assume that $D_G$ splits into
  \begin{align*}
    D_G=\left[
      \begin{array}{r}
        D_1\\D_2
      \end{array}\right]
  \end{align*}
  where $D_1$ corresponds to the vertices in one color class and $D_2$
  to the  vertices in the  other.  So any  column  of $D_1$  and $D_2$
  contains exactly one $1$. Let  $B$ be an $(k\times k)$-sub-matrix of
  $D_G$. We prove the theorem by induction.  If  $k=1$, then the claim
  is easy, so assume $k\ge 2$. We distinguish three cases:
  \begin{compactenum}
  \item If  $B$  contains   a column  with  no non-zero    entry, then
    $\det(B)=0$.
  \item If $B$ contains a column with exactly one non-zero entry, then
    we can use the Laplace formula and the induction hypothesis.
  \item If  all columns of $B$  contain exactly  two non-zero entries,
    then the sum of the rows contained in $D_1$  equals the sum of the
    rows contained in $D_2$. Hence, $\det B=0$.
  \end{compactenum}

  If $G$ is not  bipartite, then it  contains an circuit of odd length
  $k$.  Let $B$ be the $(k\times  k)$-matrix  defined by this circuit.
  Then we can reorder the rows so that
  {%\footnotesize
  \begin{align*}
    B=\left[\scriptsize
      \begin{array}{rrrrr}
        1&0&\cdots&\cdots&1\\
        1&1&&&0\\
        &1&\ddots&&\vdots\\
        &&\ddots&\ddots&\vdots\\
        &&&1&0\\
        &&&1&1
      \end{array}\right]
  \end{align*}}
  Hence, $\det B=\pm 2$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{lemma}\label{lemma:dettwo}
  Let   $B$ be a $m\times m$-matrix   with entries in $\{0,\pm1\}$. If
  $|\det B|>2$, then   $B$ has  a  square sub-matrix  $C$ with  $|\det
  C|=2$.
\end{lemma}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Consider  the  matrix  $\ov B:=\left(\, B\   \id[m]  \,\right)$.  We
  consider the following operations on the matrix $\ov B$:
  \begin{compactenum}
  \item adding or subtracting a row to another row,
  \item multiplying a row with $-1$.
  \end{compactenum}
  We   transform $\ov  B$ into   a  new  matrix  $\wt  B$  using these
  operations in such a way that
  \begin{compactenum}
  \item all entries of $\wt B$ are still in $\{0,\pm 1\}$,
  \item all unit vectors occur among the columns of $\wt B$,
  \item the first $k$ columns of $\wt B$ are unit vectors.
  \end{compactenum}
  Let  $\wt  B$  be such  that  $k$  is  maximal.   Up to  sign  these
  operations  do not  alter the  determinant of  maximal sub-matrices.
  Reordering the first and second $m$  columns of $\wt B$ allows us to
  assume that there is an $(m\times m)$-matrix $B'$ such that
  \begin{align*}
    \ov B'=\left[
      \begin{array}{rrr}
        \begin{array}{r}
          \id[k]\\0
        \end{array}&B'&
        \begin{array}{r}
          0\\\id[l]
        \end{array}
      \end{array}\right]\,.
  \end{align*}
  Up  to  sign  the  first  $m$  columns of  $\wt  B$  have  the  same
  determinant as $B$.  $|\det B|\ge 2$ implies $k<m$.

  If we cannot  transform any further column among the  first $m$ to a
  unit  vector   without  violating   condition  $(1)$,  then   up  to
  multiplication of rows with $-1$  there must be $I=\{i_1, i_2\}$ and
  $J=\{j_1, j_2\}$ such that
  \begin{align*}
    \wt B_{IJ}&=\left[
      \begin{array}{rr}
        1&1\\
        1&-1
      \end{array}\right]&&\text{or}&
    \wt B_{IJ}&=\left[\begin{array}{rr} 1&-1\\-1&-1
    \end{array}\right]\,.
  \end{align*}

  Let  $\wt H$  be the  matrix with  columns $j_1,  j_2$ and  all unit
  vectors  except   $e_{i_1}$  and  $e_{i_2}$  and  let   $H$  be  the
  corresponding  sub-matrix  of $\ov  B$.   Then  $|\det H|=|\det  \wt
  H|=2$, and as all columns of $H$ corresponding to columns of $\ov B$
  with  index $>m$ are  unit vectors,  $B$ must  have a  sub-matrix of
  determinant $\pm 2$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{corollary}\label{cor:det_unimod}
  Let $A\in\{0,\pm1\}^{m\times n}$.  Then $A$ is totally unimodular if
  and only if no square sub-matrix has determinant $\pm2$.\mqed
\end{corollary}

\begin{theorem}\label{thm:oddnononzero}
  Let $A\in\Z^{m\times n}$. Then $A$ is totally unimodular if and only
  if each  non-singular sub-matrix $B$  of $A$ has a  row  with an odd
  number of non-zero entries.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  ``$\Rightarrow$'': The number of non-zero entries in a row is odd if
  and only if the sum of the entries in that row is odd.

  Assume that $B$ is a $(k\times k)$-sub-matrix such that all row sums
  even.      We    have to    show  that      $B$   is  singular.   By
  Theorem~\ref{thm:furthercharactunimod},      there    is  a   vector
  $\xi\in\{\pm1\}^k$ such  that $B\xi\in\{0,\pm1\}^k$.  But  even  row
  sums imply $B\xi=0$, so $B$ is singular.

  ``$\Leftarrow$'':  Let $B$ be  a  non-singular sub-matrix of $A$. By
  induction, any  proper sub-matrix  of  $B$ is unimodular,  so if the
  claim     fails,     then         $|\det     B|\ge     2$.        By
  Corollary~\ref{cor:det_unimod}, $\det B = \pm 2$.

  Consider $B$ as a matrix over $\GF{2}$.   Its determinant is $0$, so
  the   columns of $B$ are linearly   dependent over $\GF{2}$.  Hence,
  there is  $\lambda\in\{0,1\}^m$ such that   $B\lambda$ has only even
  entries.  Let $J:=\{j\mid \lambda_j=1\}$.  Then  the row sums of all
  rows  of   $B_{*J}$ are   even.   Hence,  by assumption,  no maximal
  sub-matrix of  $B_{*J}$    is non-singular.    So   $\det B=0$,    a
  contradiction.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{theorem}
  Let $A\in\R^{m\times n}$ be  a matrix of  full  row rank.  Then  the
  following are equivalent:
  \begin{compactenum}
    \item For each basis $B$ of $A$ the matrix $A_B^{-1}A$ is integral,
    \item for each basis $B$ of  $A$ the matrix $A_B^{-1}A$ is totally
      unimodular,
    \item there is a basis $B$ of $A$ such that the matrix $A_B^{-1}A$
      is totally unimodular.
  \end{compactenum}%\maybehomework
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  We  may assume  that  $A=[\id\, \ov  A]$  as all  three  claims  are
  invariant under multiplication of $A$ by a non-singular matrix.

  $(1)\Rightarrow(2)$:  It suffices  to show  that $A$  is unimodular.
  Choosing $B=[m]$, i.e.\ $A=\id$ shows  that $A$ is integral. For any
  other basis we obtain from $A_B^{-1}\id=A_B^{-1}$ that $A_B^{-1}$ is
  integral. Hence $\det\, A_B=\pm 1$.

  $(2)\Rightarrow(3)$: By specialization.

  $(3)\Rightarrow(1)$: Let  $B$ be such a basis.   Then $A_B^{-1}A$ is
  integral.     For    any    other   basis    $B'$,    $A_{B'}^{-1}A=
  A_{B'}^{-1}A_BA_B^{-1}A$,  and $A_{B'}^{-1}A_B$  is integral  by the
  total  unimodularity   of  $A$  (its   rows  are  the   solution  of
  $A_{B'}^{-1}\vx=A_{*j}$ for $j\in B$).
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{theorem}
  Let $A\in \Z^{m\times n}$. The $A$ is totally unimodular if and only
  if for each  non-singular sub-matrix $B$ and each  vector $\vy$ with
  entries in $\{0,\pm1\}$,  the g.c.d.\ of the entries  of $\vy^tB$ is
  one.%\maybehomework
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Let $B$ and $\vy$  be as in the theorem.  Let $g$  be the g.c.d.\ of
  the entries of $\vy^tB$.  Then $\frac1g\vy^tB$ is integral.  If $A$
  is  totally  unimodular,  then  $B^{-1}$ is  integral,  so  $\frac1g
  \vy^t=\frac1g \vy^tBB^{-1}$ is integral, so $g=1$.

  Now assume  that the g.c.d.\ is  $1$ for every  combination of $\vy$
  and $B$.  Then  $A$ has only entries in $\{0,\pm1\}$.   Let $B$ be a
  non-singular sub-matrix  of $A$.  The common g.c.d.\  of the entries
  of $\1^tB$ is  $1$, so at least  one entry is odd. Hence,  $B$ has a
  row with an  odd number of non-zero entries.   The claim now follows
  from Theorem~\ref{thm:oddnononzero}.
  % 
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{remark}
  Let $G=(V,A)$ be a directed graph and $T=(V,A_0)$ a directed tree on
  the same vertex set as $G$.   Let $M$ be the following ($|A_0|\times
  |A|$)-matrix: For $a_0\in  A_0$ and  $a=(u,v)\in A$  let $P$ be  the
  (unique) undirected path from $u$ to $v$. The entry at $(a_0,a)$ is
  \begin{compactitem}
  \item[$1$] if $a_0$ occurs in $P$ in forward direction,
  \item[$-1$] if $a_0$ occurs in $P$ in backward direction,
  \item[$0$] if the path does not run through $a$.
  \end{compactitem}
  This matrix is the \emph{network matrix}\mdef{network matrix} of $G$
  and  $T$. If  we  allow loops in the  graph,  then class of  network
  matrices is closed under taking sub-matrices.

  In 1980, Seymour proved that  all totally unimodular matrices can be
  built from  network matrices and  two other matrices by  using eight
  types of  operations.  The decomposition  can be done  in polynomial
  time.   This implies that  the decision  problem whether  some given
  matrix $A$ is totally unimodular is in $\P$.
\end{remark}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 

