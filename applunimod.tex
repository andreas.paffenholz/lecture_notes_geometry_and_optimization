\chapter{Applications of Unimodularity}

In this chapter we   want  to present  several applications  of  total
unimodularity and    integer   linear  programming   to  combinatorial
optimization.  We need the following notion.
\begin{definition}
  Let $A\subseteq    B$   be  two  sets.     The  \emph{characteristic
    vector}\mdef{characteristic vector} of $A$   in $B$ is  the vector
  $\chi^A=(\chi^A_j)_{j\in B}\in\{0,1\}^{|B|}$ defined by
  \begin{align*}
    \chi^A_j:=
    \begin{cases}
      1\qquad & \text{if } j\in A\\
      0&\text{otherwise}\,.
    \end{cases}
  \end{align*}
\end{definition}

Let $G:=(V,E)$ be an undirected graph with $n$ vertices and $m$ edges.
A  \emph{matching}\mdef{matching} $M$ in $G$  as a  subset of the edge
set $E$ such that no two edges in $M$ are incident. The \emph{matching
  number}\mdef{matching number} of $G$ is
\begin{align*}
  \matching(G):=\max(|M|\mid M\text{ is a matching in }G)\,.
\end{align*}
Clearly, $\matching(G)\le\frac12|V|$.  The   \textsc{Maximum  Matching
  Problem}\mdef{Maximum   Matching  Problem}  asks   for  the  largest
cardinality  of  a  matching in  a  graph   (observe, that not   every
inclusion maximal  matching realizes  this number).   We  want to  use
integer  linear   programming to  find  this  number,  so   we have to
translate this into a geometric question.

Let  $D$ be the  incidence matrix  of $G$.   Then $\vx\in\R^m$  is the
incidence vector of a matching in $G$ if and only if
\begin{align*}
  D\vx&\le \1,& \vx&\ge \0, &&\text{$\vx$ integer}\,.
\end{align*}
Then the size of a maximum matching is just
\begin{align}
  \matching(G)\  =\   \max(\1^t\vx\mid  D\vx\le  \1,\,   \vx\ge  \0,\,
  \vx\in\Z^m)\,.\label{eq:maxmatching}
\end{align}
Now  we restrict to the case  that  $G$ is  bipartite.  Then  $D$ is a
totally            unimodular         matrix,            so         by
Proposition~\ref{prop:totunimod-canpoly} the polytope
\begin{align*}
  P_m(G):=\{\vx\mid D\vx\le \1,\, \vx\ge \0)
\end{align*}
defined by  $D$ is integral.  Note  that this is  exactly the matching
polytope of  the bipartite graph $G$  that we have  studied already in
Theorem~\ref{thm:bipperfmatch}.  Hence, all basic optimal solutions of
\begin{align}
  \max(\1^t\vx\mid            D\vx\le           \1,\,           \vx\ge
  \0)\,.\label{eq:maxmatchingrelax}
\end{align}
are integral and coincide with those of~(\ref{eq:maxmatching}).  So we
can  find a maximum  matching in  a bipartite  graph with  the simplex
algorithm.  Let us look at the dual linear program:
\begin{align}
  \min(\vy^t\1\mid           \vy^tD\ge           \1,\,          \vy\ge
  \0)\,.\label{eq:vertcoverrelax}
\end{align}
Again, total unimodularity of $D$  implies that all optimal  solutions
of this program  are integral, so~(\ref{eq:vertcoverrelax})  coincides
with
\begin{align}
  \min(\vy^t\1\mid       \vy^tD\ge        \1,\,       \vy\ge       \0,
  \vy\in\Z^n)\,.\label{eq:minvertcover}
\end{align}
Let $\ov \vy\in\R^n$ be an optimal solution.  No entry of $\ov \vy$ is
larger than $1$, as reducing it to $1$ yields a feasible solution with
smaller objective value.  Thus $\ov \vy\ge \0$implies that $\ov\vy$ is
the incidence vector of some subset
\begin{align*}
  W:=\{v\mid y_v=1\}\subseteq V\,.
\end{align*}
of the vertices of $G$.  The condition $\ov \vy^tD\ge \1$ implies that
for each  edge $e=(u,v)\in E$ at least  one of the end  points $u$ and
$v$  is in  $W$.   Such  a set  is  a \emph{vertex  cover}\mdef{vertex
  cover\\vertex     cover     number}     in    $G$.      See     also
Figure~\ref{fig:match-ec}.  The \emph{vertex cover number} of $G$ is
\begin{align*}
  \vertexcov(G):=\min(|W|\mid W\text{ is a vertex cover of }G)\,.
\end{align*}
%%%%%%%%%%%%% Margin figure
\marginnote{\centering\includegraphics[width=.2\textwidth]{match_ec}
\mbox{ }\bigskip
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:match-ec}}
\end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Any integral   optimal solution of~(\ref{eq:minvertcover})   defines a
vertex cover of minimal size $\vertexcov(G)$ in $G$.  Clearly, for any
graph,   not  necessarily bipartite,   this  number is  related to the
matching number via
\begin{align*}
  \matching(G)\le \vertexcov(G)\,,
\end{align*}
since any matching  edge must be covered and no  vertex can cover more
than  one matching  edge.  Already  the graph  $K_3$  shows that  this
inequality may be strict in general.  See Figure~\ref{fig:match-ec2}.

However, if $G$ is  bipartite, then $\matching(G)$ and $\vertexcov(G)$
are  given by  (\ref{eq:maxmatching}) and  (\ref{eq:minvertcover}), or
equivalently,            by~(\ref{eq:maxmatchingrelax})            and
(\ref{eq:vertcoverrelax}).   The  duality  theorem  implies  that  the
values of  these linear programs coincide.  This  proves the following
classical result of K\"onig and Egerv\'ary.
\begin{theorem}[K\"onig, Egerv\'ary 1931]\label{thm:matchingvertcover}
  Let $G$ be a bipartite graph. Then the maximum size of a matching in
  $G$ equals the minimum size of a vertex cover in $G$:
  \begin{align*}
    \matching(G)\ =\ \vertexcov(G)\,.\tag*{\mqed}
  \end{align*}
\end{theorem}

In the  same way as for matchings  we may also define the \emph{vertex
  cover         polytope}\mdef{vertex      cover             polytope}
$P_{vc}(G)\subseteq\R^{|V|}$:
\begin{align*}
  P_{vc}(G):=\conv(\chi^W\mid W\text{ is a vertex cover of } G)\,.
\end{align*}
Here $G$ need not be bipartite.  However, for bipartite graphs we know
from~(\ref{eq:minvertcover})  that $P_{vc}(G)$   is  defined  by   the
inequalities
%%%%%%%%%%%%% Margin figure
\marginnote{\centering\includegraphics[width=.2\textwidth]{k3-matching-vc}
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:match-ec2}}
\end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{subequations}\label{eq:vc}
  \begin{align}
    0\le y_v&\le 1&&\text{ for each $v\in V$}\label{eq:vc:v}\\
    y_u+y_v&\ge 1&&\text{ for each $e=(u,v)\in E$}\label{eq:vc:e}
  \end{align}
\end{subequations}
In analogy to the case of the  matching polytope of a bipartite graph,
we can  characterize bipartite graphs by   the exterior description of
the vertex cover polytope.
\begin{theorem}
  $G$ is  bipartite if  and  only  if  $P_{vc}(G)$ is  determined   by
  (\ref{eq:vc:v}) and (\ref{eq:vc:e}).
\end{theorem}
\ifproof%
\begin{proof}%
  We  have already  seen above  that~(\ref{eq:vc}) suffice  if  $G$ is
  bipartite.   So suppose  $G$ contains  an odd  circuit $C$  with $n$
  vertices.      Define    $\vy:=\frac12\1\in\Z^n$.      Then    $\vy$
  satisfies~(\ref{eq:vc}).  However, any  vertex cover of $G$ contains
  at least  $\frac{n+1}{2}$ vertices of  $C$, but $y(C)=\frac  n2$. So
  $\vy\not \in P_{vc}(G)$.
\end{proof}%
\fi%

Theorem~\ref{thm:matchingvertcover}  has    some   nice  consequences.
Recall   that    a   matching      is      \emph{perfect}\mdef{perfect
  matching\\$k$-regular  graph}   if  each vertex is     incident to a
matching edge.  A graph $G$ is \emph{$k$-regular} if all vertices have
the same degree $k$, i.e.\ if all vertices are incident to exactly $k$
edges.
\begin{corollary}[Frobenius' Theorem] 
  A bipartite graph $G=(V,E)$  has a perfect  matching if and only  if
  each vertex cover has size at least $\frac12|V|$.
\end{corollary}
\ifproof%
\begin{proof}%
\ifreleasedproof%
  $G$  has a   perfect  matching  if   and  only  if  $\matching(G)\ge
  \frac12|V|$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{corollary}[K\"onig]
  Each $k$-regular bipartite graph  $G=(V,E)$, $k\ge 1$, has a perfect
  matching.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  $|E|=\frac12k|V|$.  Any vertex covers  exactly $k$ edges, hence, any
  vertex cover contains at least $\frac12|V|$ vertices.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

More     generally,     we     can     look     for     \emph{weighted
  matchings}\mdef{weighted  matching\\ weight  function\\weight}  in a
graph $G$. Given a  \emph{weight function} $\vw\in\Z_+^m$ on the edges
of $G$, the \emph{weight} of a matching $M\subset E$ is
\begin{align*}
  \mweight(M):=\sum_{e\in M}w_e\,.
\end{align*}
The  \emph{\textsc{Maximum    Weighted Matching Problem}}\mdef{Maximum
  Weighted Matching Problem} is the task to find a matching of maximum
weight.  Expressed as an integer linear program, we want to solve
\begin{align*}
  \max(\vw^t\vx\mid D\vx\le \1,\, \vx\ge \0,\, \vx\in\Z^m)\,.
\end{align*}

If $G$ is bipartite, then $D$ is totally unimodular, and we know that
any optimal solution of the relaxed problem
\begin{align*}
  \max(\vw^t\vx\mid D\vx\le \1,\, \vx\ge \0)
\end{align*}
is integral.  Note however, that  a maximum weighted matching need not
have maximum cardinality. We obtain a min-max relation as before.
\begin{proposition}\label{prop:minmax-matching}
  Let  $G=(V,E)$ be  a  bipartite graph  and  $\vw\in\Z_+^m$ a  weight
  function on the edges.

  The maximum  weight of  a matching  in $G$ is  equal to  the minimum
  value of $\1^t \vf$, where $\vf$ ranges over all $\vf\in\Z_+^n$ that
  satisfy
  \begin{align*}
    f_u+f_v&\ge w_e&&\text{for all edges $e=(u,v)\in E$}\,.
  \end{align*}
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Using total unimodularity of the incidence matrix $D$ of $G$ and the
  duality theorem this statement can be written as
  \begin{align*}
    \max(\vw^t\vx\mid  D\vx\le\1,\,  \vx\ge\0)\  &=\  \min(\vf^t\1\mid
    \vf^tD\ge \vw^t,\, \vf\ge \0)\,.
  \end{align*}
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

A vector  $\vf$ as in  the proposition is called  a \emph{$\vw$-vertex
  cover}\mdef{$\vw$-vertex cover\\size}.   The value $\1^t\vf$  is the
\emph{size} of $\vf$.   So the proposition just says  that the maximum
weight of a matching in a bipartite graph equals the minimum size of a
$\vw$-vertex cover.

Another  closely related problem  is the  \emph{\textsc{Minimum Weight
    Perfect  Matching Problem}}\mdef{Minimum  Weight  Perfect Matching
  Problem}  (alternatively  you can  of  course  also  search for  the
maximum).  Given a graph $G$ and a weight function $\vw\in\Z^m$ on the
edges, we want to find a perfect matching $M$ in $G$ of minimum weight
\begin{align*}
  \mweight(M):=\sum_{e\in M}w_e\,.
\end{align*}
So we want to solve
\begin{align*}
  \min(\vw^t\vx\mid D\vx=\1,\, \vx\ge \0,\, \vx\in\Z^m)\,.
\end{align*}
If $G$ is bipartite, then this coincides with
\begin{align*}
  \min(\vw^t\vx\mid D\vx=\1,\, \vx\ge \0)\,.
\end{align*}
The   corresponding  polyhedron $P_{pm}(G)$  is the   perfect matching
polytope of   Theorem~\ref{thm:bipperfmatch}.  Unimodularity and   the
duality theorem again immediately imply the following statement.
\begin{proposition}
  Let $G=(V,E)$ be a bipartite graph having a perfect matching and let
  $\vw\in\Q^m$ be a weight function.

  The minimum weight  of a perfect  matching  is equal to  the maximum
  value of $\1^t\vf$ taken over all $\vf\in\Z^n$ such that
  \begin{align*}
    y_u+y_v&\ge w_e&&\text{for each edge $e=(u,v)\in E$}\tag*{\mqed}
  \end{align*}
\end{proposition}

In a   similar way  we  can also  derive  another  classical result of
K\"onig about bipartite graphs.  An \emph{edge cover}\mdef{edge cover}
in $G$ is  a subset $F\subseteq E$  of the edges of  $G$ such that for
any vertex $v\in V$ there is an  incident edge in $F$.  The \emph{edge
  cover number}\mdef{edge cover number} of $G$ is
\begin{align*}
  \edgecov(G):=\min(|F|\mid F\text{ is an edge cover of }G)\,.
\end{align*}
Determining $\edgecov(G)$ is the \textsc{Minimum Edge Cover Problem}.

A \emph{stable set}\mdef{stable set} in $G$ is a subset $S\subseteq V$
of the vertices such that  no two vertices  in $G$ are connected by an
edge.    The    \emph{stable   set number}\mdef{stable    set  number}
$\stable(G)$ is
\begin{align*}
  \stable(G):=\max(|S|\mid S\text{ is a stable set in }G)\,.
\end{align*}
Finding $\stable(G)$ is the \textsc{Maximum Stable Set Problem}.
\begin{theorem}[K\"onig 1933]
  Let $G$ be a bipartite graph. Then the  maximum size of a stable set
  equals the minimum size of an edge cover in $G$.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Let $D$ be the incidence matrix of $G$, and let $S\subseteq V$. Then
  $\vy\in\R^n$ is the characteristic vector  of a stable set in $G$ if
  and only if
  \begin{align*}
    \vy^t D &\le \1 & \vy&\ge \0& &\text{$\vy$ integer}\,.
  \end{align*}

  A characteristic vector $\vx\in\R^m$ defines an edge cover in $G$ if
  and  only  if $\vx\in\{0,1\}^m$  and  $D\vx\ge  \1$.  Again, we  can
  reformulate this to
  \begin{align*}
    D\vx&\ge \1,& \vx&\ge \0, &&\text{$\vx$ integer}\,.
  \end{align*}

  The   incidence   matrix   of   $G$   is   totally   unimodular   by
  Theorem~\ref{thm:totunimod_bipartite}.  So integer and linear optima
  coincide. Hence, using the duality theorem, the optimal value of the
  integer linear program
  \begin{align}
    \max(\1^t\vy\mid    \vy^tD\le    \1,\,    \vy\ge   \0,\,    \vy\in
    \Z^n)\tag{\textsc{Max Stable Set}}\label{eq:maxstabset}
  \end{align}
  equals the optimal value of the linear program
  \begin{align}
    \min(\1^t\vx\mid     D\vx\ge    \1,\,    \vx\ge     \0\,    \vx\in
    \Z^m),\,\tag{\textsc{Min Edge Cover}}\label{eq:minedgecover}
  \end{align}
  where the  first computes the  size of a  maximum stable set and the
  second the size of a minimum edge cover.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
This  theorem is  \textit{dual} to Theorem~\ref{thm:matchingvertcover}
in that it interchanges the r\^ole of vertices  and edges, and minimum
and maximum.

\begin{proposition}
  Let $G=(V,E)$  be  a  bipartite  graph  and  $\vw\in Z_+^m$  a  weight
  function on the edges.

  The minimum weight  of an edge cover in $G$ is  equal to the maximum
  of $\1^t  \vf$, where the  maximum is taken over  all $\vf\in\Z_+^n$
  that satisfy $f_u+f_v\le w_e$ for all edges $e=(u,v)\in E$.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  This   is    completely    analogous      to    the      proof    of
  Proposition~\ref{prop:minmax-matching}.  Total unimodularity  of $D$
  implies that the theorem is equivalent to
  \begin{align*}
    \max(\vw^t\vx\mid  D\vx\ge\1,\,  \vx\ge\0)\  &=\  \min(\vf^t\1\mid
    \vf^tD\le \vw^t,\, \vf\ge \0)\,.
  \end{align*}  
  This equality is now just the duality theorem.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\bigskip

Now we consider  a directed graph $G=(V,A)$  with $n$ vertices and $m$
arcs.  For a set $U\subseteq V$ let
\begin{align*}
  \delta^+(U):=\{(u,v)\mid u\in U,\, v\not\in U\}
\end{align*}
be  the set of  outgoing edges of $U$.  For  a vertex $v\in  V$ we set
$\delta^+(v):=\delta^+(\{v\})$.     Similarly,  we define    the  sets
$\delta^-(U)$   and   $\delta^-(v)$  of  incoming    edges.   A subset
$C\subseteq A$  is a \emph{cut}\mdef{cut\\proper cut\\capacity} in $G$
if $C=\delta^+(U)$ for  some $U\subseteq V$.   $C$ is \emph{proper} if
$\varnothing \ne U\ne    V$.  Let  $c\in(\R_+\cup\{\infty\})^m$ be   a
\emph{capacity} on the arcs of $G$.   The \emph{capacity} of a cut $C$
is
\begin{align*}
  \capacity(C)\ :=\ \sum_{a\in C}c_a\,.
\end{align*}

Let $\vf\in\R^A_+$ be a non-negative vector on the arcs of $G$.  $\vf$
satisfies    the    \emph{flow    conservation    condition}\mdef{flow
  conservation} at a vertex $v\in V$ if
\begin{align*}
  \sum_{a\in\delta^-(v)}f_a\ =\ \sum_{a\in\delta^+(v)}f_a\,.
\end{align*}
Let   $s,t\in   V$   be   two   vertices   of   $G$.    $\vf$   is   a
\emph{$s$-$t$-flow}\mdef{$s$-$t$-flow} in $G$ if
\begin{compactenum}
\item  $\vf$  satisfies  flow   conservation  at  all  vertices  $v\in
  V-\{s,t\}$ and
\item         $\val(\vf):=            \sum_{a\in\delta^+(s)}f_a        -
  \sum_{a\in\delta^-(s)}f_a\       =\  \sum_{a\in\delta^-(t)}f_a     -
  \sum_{a\in\delta^+(t)}f_a \ \ge \ 0$\,.
\end{compactenum}
$s$    is   the    \emph{source}\mdef{source,    sink\\value   of    a
  flow\\circulation} of the flow and $t$ the \emph{sink}.  $\val(\vf)$
is the \emph{value} of the flow  $\vf$.  It is the net out-flow of the
source  (which equals  the net  in-flow  of the  sink).  A  flow is  a
\emph{circulation} in $G$  if flow conservation also holds  at $s$ and
$t$.  Let $\vc\in(\R_+\cup\{\infty\})^m$ be  a capacity on the arcs of
$G$.   A flow $\vf$  is \emph{subject  to $\vc$}\mdef{flow  subject to
  $c$\\maximum $s$-$t$-flow\\\textsc{Maximum Flow Problem}} if $\vf\le
\vc$.   A \emph{maximum  $s$-$t$-flow} is  an $s$-$t$-flow  subject to
$\vc$ of  maximum value.  Finding  such a flow is  the \textsc{Maximum
  Flow Problem}.

We want to transform  this  into a linear   program.  Let $D$ be   the
incidence  matrix of $G$ and $D'$  the matrix obtained by deleting the
rows corresponding to the vertices $s$ and $t$.   Then $D'$ is totally
unimodular by Corollary~\ref{thm:unimodfordirected} and
\begin{align}
  \text{$\vf$       is      an      $s$-$t$-flow       subject      to
    $\vc$}\qquad\Longleftrightarrow\qquad D\vf\ =\ \0,\ \ \0\le \vf\le
  \vc.\label{eq:corr-circulation-DG}
\end{align}

Let $\vw$ be the row of $D$ corresponding to $s$. Then $\vw$ has a $1$
at the position of an outgoing edge of $s$, and a $-1$ at the position
of an incoming  edge.  Hence, if $\vf$ is a  $s$-$t$-flow on $G$, then
$\val(\vf)=\vw^t\vf$.   

The \textsc{Maximum Flow  Problem} can  now be written as
\begin{align}
  \max(\vw^t\vf\mid  D'\vf\  =  \  \0,\  \ \0\,\le  \,  \vf\,  \le  \,
  \vc)\,.\label{eq:maxflow}
\end{align}
Although  the matrix $D'$  is totally  unimodular, we  cannot conclude
integrality  of an  optimal solution,  as  the capacities  may not  be
integer. However, the cost function  $\vw$ is integral, as it is given
by a  row of  the integral  matrix $D$.  So  we can  pass to  the dual
program
\begin{align}
  \min(\vy^t\vc\mid  \vy\ge \0\text{ and  there is  $\vz$ such  that }
  \vy^t+\vz^tD'\ge \vw^t)\,.\label{eq:dualtoflow}
\end{align}
Here, the constraint matrix and right hand side are 
\begin{align*}
  M\ &:=\ \left(
    \begin{array}{rr}
      -\id & -\id\\ 
      -D'\phantom{_m}& 0\phantom{_m}
    \end{array}\right)&
  \vb\ &:=\ \left(
    \begin{array}{r}
      -\vw^t\\\0\phantom{^t}
    \end{array}\right)\,.
\end{align*}
By Theorem~\ref{thm:poly-integral} the polyhedron $\{\vx\mid \vx^tM\le
\vb^t\}$ is integral, so~(\ref{eq:dualtoflow}) has an integral optimal
solution $(\ov \vy, \ov \vz)$. We  want to interpret this in the graph
$G$.

Extend $\ov \vz$ by $\ov z_s=-1$ and $\ov z_t=0$. Then
\begin{align}
  \ov \vy^t +\ov \vz^t D \ge 0\,.\label{eq:cutineqs}
\end{align}
Define
\begin{align*}
  U&:=\{v\in V\mid \ov z_v\le -1\} &&\text{and}& C&:=\delta^+(U)\,.
\end{align*}
%%%%%%%%%%%%% Margin figure
\marginnote{
  \footnotesize
  \psfrag{s}{$s$}
  \psfrag{t}{$t$}
  \psfrag{f0}{$1\mid2$}
  \psfrag{f1}{$2\mid3$}
  \psfrag{f2}{$1\mid1$}
  \psfrag{f3}{$2\mid4$}
  \psfrag{f4}{$1\mid1$}
  \psfrag{f5}{$0\mid2$}
  \psfrag{f6}{$2\mid2$}
  \psfrag{f7}[cl][cl]{$0\mid1$}
  \psfrag{f8}{$2\mid2$}
  \psfrag{f9}[br][br]{$1\mid1$}
  \psfrag{fa}{$1\mid2$}
  \centering\includegraphics[width=.28\textwidth]{flo_cut_simple_flipped}
  \mbox{ }\bigskip
  \begin{minipage}{.4\linewidth}
    \captionof{figure}{\label{fig:flow-cut}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
then $s\in U$  and $t\not\in U$, so $C$  is an $s$-$t$-cut  in $G$. We
want to    compute the  capacity of   $C$.   Let  $a=(u,v)\in   C$. By
construction,
\begin{align*}
  \ov z_u&\le -1&&\text{and }&\ov z_v&\ge 0\,.
\end{align*}
But~(\ref{eq:cutineqs})  implies that  $y_a+z_u-z_v\ge  0$, so $y_a\ge
z_v-z_u\ge 1$. Hence
\begin{align}
  \capacity(C) &\le \vy^t\vc = \val(\vf)\label{eq:cutlowerbound}
\end{align}
by duality.  However, the capacity of any cut $C$ in $G$ is clearly an
upper bound on the size of an $s$-$t$-flow in $G$, so we have equality
in~(\ref{eq:cutlowerbound}).      This    is     the     well    known
\textsc{MaxFlow-MinCut} Theorem.  See Figure~\ref{fig:flow-cut} for an
example. The dashed edges in the graph are a cut. The arc labels $i|J$
denote the flow and the capacity of that arc.
\begin{theorem}[Ford \& Fulkerson 1956]
  Let $G=(V,A)$ be  a directed graph, $\vc\ge \0$  a capacity function
  on the edges of $G$ and $s, t$ two vertices of $G$.

  The maximum value  of  an $s$-$t$-flow  in  $G$ equals  the  minimum
  capacity  of an $s$-$t$-cut in  $G$.  If all capacities are integer,
  then the optimal flow can be chosen to be integer.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  The  only missing  piece is  the integrality  of $\vf$  if  $\vc$ is
  integer.  But in this case the value of
  \begin{align*}
    \max(\vw^t\vf\mid D'\vf\ = \ \0,\,  \0\,\le \, \vf\, \le \, \vc,\,
    \vf\in\Z^m)
  \end{align*}
  equals~(\ref{eq:maxflow}) by total unimodularity of $D$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

Let  $s,   t$  be  vertices   of  a  directed  graph   $G=(V,A)$.   An
\emph{$s$-$t$-path}\mdef{$s$-$t$-path} $P$ is an alternating sequence
\begin{align*}
  P=v_0=s,  a_0, v_1,  a_1,  v_2,  \ldots, a_{k-2},  v_{k-1}, a_{k-1},
  v_k=t\,,
\end{align*}
It is \emph{arc disjoint}\mdef{arc disjoint path}  if $a_i\ne a_j$ for
all  $0\le i<j\le k-1$.   Let  $P_1, \ldots, P_k$  be $s$-$t$-paths in
$G$, and $\chi_{P_1},  \ldots, \chi_{P_k}\in\{0,1\}^m$ their incidence
vectors. Then
\begin{align*}
  \vf:=\chi_{P_1}+\cdots+\chi_{P_k}
\end{align*}
is an integral flow of value $k$ in $G$. 
\begin{theorem}[Menger 1927]
  Let $G=(V,A)$ be  a directed graph and $s,t$  vertices of $G$.  Then
  the maximum number of pairwise arc disjoint $s$-$t$-paths equals the
  minimum size of an $s$-$t$-cut.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Define a  capacity $\vc$ on $G$  by $c_a=1$ for all  $a\in A$.  Then
  the size of a cut equals its capacity.  Let $C_0$ be a cut in $G$ of
  minimum size $s$.   Let $P_1, \ldots, P_k$ be a  set of arc disjoint
  $s$-$t$-paths. Then $s\ge  k$ as $C_0$ contains at  least one arc of
  each path.

  By  the \textsc{MaxFlow-MinCut}-Theorem, and  as $\vc$  is integral,
  there  is  an  integral  flow  $\vf$  of  value  $s$.  The  capacity
  constraints imply that $f_a\in\{0,1\}$ for each $a\in A$.

  We show by induction that  $\vf$ can be decomposed into $s$ disjoint
  $s$-$t$-paths.  Let $B:=\{a\mid  f_a=1\}$.  Clearly, if $\vf\ne \0$,
  then  flow conservation  implies that  $B$ contains  an $s$-$t$-path
  $P$.  Hence, we can construct a new $s$t-$s$-flow $\vf'$ by
  \begin{align*}
    f'&:=f-\chi_P\,.
  \end{align*}
  $\vf'$ has  value $s-1$. Further,  $f'_a=0$ if $a\in  P$.  Repeating
  this, we obtain $s$ arc disjoint $s$-$t$-paths in $G$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\smallskip

We finish this chapter with an example  for the primal-dual-method for
solving a linear  program   that we have    seen towards the   end  of
Chapter~\ref{cha:simplex-method}.    There  we  started  from the dual
linear programs in the form
\begin{align*}
  \begin{split}
    \max(\vc^t\vx&\mid A\vx=\vb,\, \vx\ge \0)\\
    \min(\vy^t\vb&\mid \vy^tA\ge \vc^t)\,,
  \end{split}
\end{align*}
where $A\in\R^{m\times  n}$, $\vb\in\R^m$, and  $\vc\in\R^n$.  We want
to have a slightly different form here: We multiply $A, \vb$ and $\vc$
by $-1$ to obtain the dual pair of programs
\begin{align}\label{eq:gen-pdpair}
  \begin{split}
    \min(\vc^t\vx&\mid A\vx=\vb,\, \vx\ge \0)\\[.2cm]
    \max(\vy^t\vb&\mid \vy^tA\le \vc^t)\,.
  \end{split}
\end{align}
The primal-dual  method applies  with almost no  change also  to these
programs.   We just  have to  reverse all  inequality signs.   Given a
feasible  dual  solution $\ov  \vy\in\R^m$,  and  the set  $J:=\{j\mid
\vy^tA_{*j}=c_j\}\subset[m]$  we  looked   at  the  restricted  primal
program
\begin{align*}
  \min(\1^t\vz\mid \vz+A_{*J}\vu=\vb,\, \vu,\vz\ge \0)\,.
\end{align*}
and the corresponding dual program
\begin{align*}
  \max(\vw^t\vb\mid \vw\le \1,\, \vw^tA_{*J}\le \0)\,.
\end{align*}
Using the  optimal solution  $\ov \vw$ of  the dual program  we showed
that there is some $\eps>0$ such  that $\ov \vy+\eps\ov \vw$ is a dual
feasible solution with better objective value.

Let us consider again the \textsc{MaxFlow-MinCut} problem that we have
introduced above. We give   a  different representation as a    linear
program.  Let $G:=(V,A)$ be a directed graph with $n$ vertices and $m$
arcs.  Let $\vd\in\Z^V$ be defined by
\begin{align*}
  d_v:=
  \begin{cases}
    \phantom{-}1\qquad&\text{if }v=t\\
    -1&\text{if }v=s\\
    \phantom{-}0&\text{otherwise}\,.
  \end{cases}
\end{align*}
Let $\vf\in\Z^A$ denote  a flow in $G$, $\vc\in\R^A$,  $\vc\ge \0$ the
capacities on the  arcs, and $w\in\R$ the flow  value.  Then a maximum
flow can be computed by solving the linear program
\begin{align}\label{eq:maxflow-pd-D}
  \begin{split}
    &\max(w\mid D\vf-\vd w\le \0,\, \0\le \vf\le \vc)\\[.1cm]
    &\qquad=\max(w\mid  D\vf-\vd  w\le   \0,\,  -\vf\le  \0,\,  \vf\le
    \vc)\,.
  \end{split}
\end{align}

The  inequality system  $D\vf-w\vd\le  \0$  may seem  to  be a  weaker
condition that flow conservation, as it only requires that the in-flow
of  a  vertex  $v\ne  s,t$  is larger  than  the  out-flow.   However,
$\vb:=\1^t(D\vf+w\vd)=\0$, which implies that  every entry of $\vb$ is
$\0$.  So  $D\vf+w\vd\le \0$  implies $D\vf+w\vd=\0$ for  any feasible
solution  $(\vf,\vd)$.  The  additional  row $\vd$  of the  constraint
matrix can be seen as an additional arc from $t$ to $s$ that sends all
flow back  to the source.  This  way we have flow  conservation at any
vertex  (i.e.\ we have  a \emph{circulation}\mdef{circulation}  in the
graph), and we want to maximize the flow on the artificial arc.

We  can  use   this  linear  program  as  the   dual  program  in  the
pair~(\ref{eq:gen-pdpair}).   As  $\vc\ge\0$  we know  that  $\vf=\0$,
$w=0$ is a  feasible solution of this linear program,  so we can start
the primal-dual  algorithm.  Let now  $(\vf, w)$ be any  dual feasible
solution. In order to write down the restricted linear program we have
to find all  inequalities of the dual program  that are satisfied with
equality by our solution.

By our above consideration,  the inequalities in $D\vf+w\vd\le \0$ are
always satisfies with equality. Let
\begin{align*}
  U&:=\{a\in A\mid f_a=c_a\}& L&:=\{a\in A\mid f_a=0\}&
\end{align*}
%%%%%%%%%%%%% Margin figure
\marginnote{\centering
  \footnotesize
  \psfrag{s}{$s$}
  \psfrag{t}{$t$}
  \psfrag{f0}{$0|3$}
  \psfrag{f1}{$1|1$}
  \psfrag{f2}{$0|1$}
  \psfrag{f3}{$2|4$}
  \psfrag{f4}[bl][bl]{$2|4$}
  \psfrag{f5}[cr][cr]{$1|1$}
  \psfrag{f6}[tl][tl]{$1|1$}
  \psfrag{f7}[cr][cr]{$1|4$}
  \psfrag{f8}{$2|3$}
  \psfrag{f9}[br][br]{$1|1$}
  \psfrag{fa}{$1|1$}
  \includegraphics[width=.28\textwidth]{flow_flipped}
\mbox{ }\\[\baselineskip]
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:maxflow:a}}
\end{minipage}
\\[\baselineskip]
  \psfrag{s}{$s$}
  \psfrag{t}{$t$}
    \psfrag{a0}{\footnotesize$2$}
    \psfrag{b0}{\footnotesize$2$}
    \psfrag{a1}{\footnotesize$2$}
    \psfrag{b1}{\footnotesize$1$}
    \psfrag{a2}{\footnotesize$1$}
    \psfrag{a3}{\footnotesize$1$}
    \psfrag{a4}{\footnotesize$1$}
    \psfrag{a5}{\footnotesize$1$}
    \psfrag{a6}{\footnotesize$2$}
    \psfrag{b6}{\footnotesize$2$}
    \psfrag{a7}{\footnotesize$1$}
    \psfrag{a8}{\footnotesize$3$}
    \psfrag{b8}{\footnotesize$1$}
    \psfrag{a9}{\footnotesize$1$}
    \psfrag{b9}{\footnotesize$2$}
    \psfrag{aa}{\footnotesize$1$}
  \includegraphics[width=.28\textwidth]{flow_flipped_aug}
\mbox{ }\\[\baselineskip]
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:maxflow:b}}
\end{minipage}
\\[\baselineskip]
  \psfrag{s}{$s$}
  \psfrag{t}{$t$}
  \psfrag{f0}{$0|3$}
  \psfrag{f1}{$2|1$}
  \psfrag{f2}{$1|1$}
  \psfrag{f3}{$2|4$}
  \psfrag{f4}[bl][bl]{$1|4$}
  \psfrag{f5}[cr][cr]{$0|1$}
  \psfrag{f6}[tl][tl]{$1|1$}
  \psfrag{f7}[cr][cr]{$0|4$}
  \psfrag{f8}{$1|3$}
  \psfrag{f9}[br][br]{$1|1$}
  \psfrag{fa}{$1|1$}
  \includegraphics[width=.28\textwidth]{flow_flipped}
\mbox{ }\\[\baselineskip]
  \begin{minipage}{.4\linewidth}
    \captionof{figure}{\label{fig:maxflow:c}}
\end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
The dual of the restricted linear program for the solution $(\vf, w)$
is
\begin{align*}
  \max(z\mid D\vx-z\vd\le  \0,\, x_U\le  0,\, -x_L\le 0,\,  x\le \1,\,
  z\le 1)\,.\label{eq:residualgraph}
\end{align*}

The constraint matrix of this linear program is totally unimodular, so
we have  an integral optimal  solution $(\vx,z)$.  If $z=0$,  then our
flow  was optimal.  Otherwise, $z=1$,  and we  may interpret  the dual
program in the following way:

Let $G^r$ be directed graph on the vertex  set $V$ obtained from $G$
in the following way.
\begin{compactenum}
\item for all arcs $a\in L$ we insert $a$ into $G^r$,
\item for all arcs $a=(u,v)\in U$ we insert $(v,u)$ into $G^r$,
\item for all arcs $a=(u,v)\in A-(U\cup L)$ we insert both $(u,v)$ and
  $(v,u)$ into $G^r$.
\end{compactenum}
$G^r$ is  the \emph{residual graph}  of $G$ corresponding to  the flow
$\vf$. The linear program~\ref{eq:residualgraph} finds a path $P$ from
$s$ to $t$ in $G^r$:
\begin{compactenum}
\item We have an integral flow $\vx$ that is $\le 1$ on each arc.
\item Its value is $1$, so $-1\le x_a\le 1$ on all arcs.
\item $0\le x_a\le 1$ on arcs $a\in L$
\item $-1\le x\le 0$ on  arcs in $a\in U$  (so $0\le x_a\le 1$ on  the
  reversed arcs in $G^r$).
\item Flow conservation  implies that at  each vertex we  have at most
  one incoming and one outgoing arc.
\end{compactenum}
(we may create additional disconnected  loops in the graph. They don't
improve the flow.) The path $P$ is an \emph{augmenting path} in $G^r$.
Figures~\ref{fig:maxflow:a}-\ref{fig:maxflow:c}  show an  example. The
first figure shows a graph with  a feasible flow and capacities on the
arcs. The second figure shows an augmenting path, and the last the new
flow after adding the augmenting path.

The new dual feasible solution
\begin{align}
  \tilde \vf &:= \vf+\eps \vx&\tilde w&:=w+\eps z\label{eq:flowup}
\end{align}
increases the flow along this path by  some $\eps>0$. We can determine
the  maximum possible  $\eps$ by assigning  capacities  to the arcs in
$G^r$:
\begin{compactenum}
\item $c_a$ on arcs originating from $L$ or $U$.
\item $c_a-f_a$ on the  forward and $f_a$ on  the backward arc for all
  other arcs.
\end{compactenum}
The minimum of the capacities along the augmenting path is the maximum
$\eps$.   This  is strictly positive  by  construction.  Viewed in the
graph, the  flow update in~(\ref{eq:flowup}) does  the following.  For
each  forward  arc in   the path $P$  (i.e.\  one  that  has the  same
orientation as in $G$), we add $\eps$ to the flow on the arc. For each
backward arc in  the  path we subtract $\eps$.   All other  arcs  stay
unchanged. This is the \textsc{Ford-Fulkerson} algorithm for finding a
maximum flow in a directed graph. The  arguments above show that it is
in fact an algorithm based on linear programming.

If $\vc$ is integral, then $\eps\ge  1$.  In this case, the flow value
increases by  at least  one in  each step.  As  $\1^t\vc$ is  an upper
bound for the maximum flow, this algorithm reaches the optimum after a
finite  number  of steps.   The  same  argument  applies if  $\vc$  is
rational, as we  can scale the capacities to be  integer.  If $\vc$ is
not rational, then this algorithm in this form may fail to finish in a
finite number  of steps.  However,  choosing the augmenting  path more
carefully also guarantees  finiteness for non-rational capacities (see
any book on graph algorithms).

If $\vc$ is integral, then we can also choose the flow to be integral,
as we  may start with the  flow $\vf=\0$ and then  always increase the
flow on the arcs  by an integer. By an argument similar  to the one in
the proof of Menger's theorem, one can show that such an integral flow
can always be decomposed into a set of paths.

There are more graph  algorithms that can  be seen to be a primal-dual
linear programming  algorithm.   Among them is  e.g.\  the  Hungarian
algorithm for finding a minimum weight perfect matching.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 
