\chapter{The Matching Polytope}
\label{ch:matching}

Let $G=(V,E)$ be an undirected graph.   $G$ is \emph{bipartite}, if we
can write $V$ as the disjoint union of two  non-empty sets $A$ and $B$
(the \emph{color classes} of $G$) such that no edge has both end points
in the same set.
\begin{definition}
  A \emph{matching} in $G$  is a subset $M\subset  E$ of the  edges so
  that no two edges are incident. The \emph{size} of a matching $M$ is
  $|M|$.

  A matching is \emph{maximal} if it is not contained in a matching of
  larger size, and  it is  \emph{maximum} if  it has  the largest size
  among all matchings. 

  A matching is \emph{perfect}, if any vertex  is incident to one edge
  of $M$.
\end{definition}
It is easy to see that a maximal matching covers  at least half of the
vertices, but $P_4$ shows that this may be  tight. Clearly, $|V|$ must
be even if $G$ contains a perfect matching.
%%%%%%%%%%%%% Margin figure
\marginnote{ 
  \centering
  \includegraphics[angle=90,width=.18\textwidth]{graph1}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:exmatching}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{example}\label{ex:matching}
  Let $V:=\{a,b,c,d,e,f\}$ and
  \begin{align*}
    E:=\{(a,b),(a,c),(b,c),(b,d),(b,e),(c,d),(c,e),(d,f),(e,f)\}\,.
  \end{align*}
  Then     $M:=\{(b,d),   (c,e)\}$   is   a   maximal   matching,  and
  $M:=\{(a,b),(c,d), (e,f)\}$ is  a maximum and perfect matching.  See
  also Figure~\ref{fig:exmatching}.
\end{example}
% \begin{figure}[b]
%   \centering
%   \includegraphics[width=.3\textwidth]{graph1}
%   \caption{The graph of Example \ref{ex:matching}}
%   \label{fig:exmatching}
% \end{figure}
The \emph{characteristic  vector}  $\chi^M\in\{0,1\}^{|E|}$  of      a
matching $M$ is the vector defined by
\begin{align*}
  \chi^M_e:=
  \begin{cases}
    1\qquad&\text{if $e\in M$}\\
    0&\text{otherwise}\,.
  \end{cases}
\end{align*}

\begin{definition}
  Let $G=(V,E)$ be    a graph.  The \emph{perfect matching   polytope}
  $P_{pm}(G)$ of $G$ is
  \begin{align*}
    P_{pm}(G):=\conv(\chi^M\mid M\text{ is a perfect matching in } G)\,.
  \end{align*}
  and the \emph{matching polytope} $P_{pm}(G)$ of $G$ is
  \begin{align*}
    P_{m}(G):=\conv(\chi^M\mid M\text{ is a matching in } G)\,.
  \end{align*}
\end{definition}
All  characteristic  vectors are  $0/1$-vectors,  hence,  they form  a
subset of the vertex set of the cube $[0,1]^{|E|}$.  This implies that
any characteristic vector  of a (perfect) matching is  a vertex of the
(perfect) matching polytope.  The origin and the standard unit vectors
of   $\R^{|E|}$   are   characteristic   vectors  of   matchings,   so
$\dim(P_m(G)=|E|$. The  dimension of $P_{pm}(G)$  is certainly smaller
than $|E|$, as  all points are contained in  the hyperplane $\{\vx\mid
\1^t\vx=\nicefrac{|V|}{2}\}$.

Let  $\weight:E\longrightarrow \R$ be  some \emph{weight  function} on
the edges of  $G$. The \emph{weight} of a  matching $M$ is $\sum_{e\in
  M}w(e)$.  The problem of finding a maximum weight (perfect) matching
is the linear program
\begin{align*}
  \text{maximize}\quad&   \sum_{e\in     E}\weight(e)x_e&\text{subject
    to}\quad &\vx\in P_{m}(G)\quad (\vx\in P_{pm}(G))\,.
\end{align*}
In particular, choosing unit  weights would return a maximum matching.
This could  be solved via linear  programming if we  had an inequality
description of  the polytope.   We want to  derive such  a description
now.  It is  due to Jack Edmonds (1965). It  is sometimes claimed that
this   discovery  initiated   the   use  of   polyhedral  methods   in
combinatorial optimization.   We start with bipartite  graphs, as they
are much easier to handle.  For $U\subseteq V$, let
\begin{align*}
  \setadjedges(U):=\{e\in E\mid |e\cap U|=1\}
\end{align*}
be the set  of all edges that  have exactly one end point  in $U$.  If
$U=\{v\}$,    then    we    write   $\setadjedges(v)$    instead    of
$\setadjedges(\{v\})$.  Further,  for $\vx\in\R^{|E|}$ and $S\subseteq
E$ let $\vx(S):=\sum_{e\in S}x_e$.

%For   $U,U'\subset   V$,    $U\cap     U'=\varnothing$,  we      write
%$x(U,U'):=\sum_{u\in U, u'\in U'}x_{(u,u')}$.

Clearly,  any convex  combination  $x$  of characteristic  vectors  of
perfect matchings satisfies the following sets of constraints.
\begin{subequations}\label{eq:pm}
  \begin{align}
    x_e&\ge 0&&\text{for all $e\in E$}\label{eq:pm:e}\\
    \vx(\setadjedges(v))&=1&&\text{for all $v\in V$}\label{eq:pm:v}
  \end{align}
\end{subequations}
The next  theorem says that these  inequalities even suffice if $G$ is
bipartite.
\begin{theorem}\label{thm:bipperfmatch}
  If $G$ is bipartite  then \eqref{eq:pm} give a complete  description
  of the perfect matching polytope $P_{pm}(G)$.
\end{theorem}
\ifproof
\begin{proof}%
  \ifreleasedproof%
  Let      $Q:=\{\vx\in\R^{|E|}\mid      \vx     \text{      satisfies
    \eqref{eq:pm}}\}$. $Q$ is clearly  contained in the $0/1$-cube, so
  it is  bounded and a polytope.  Further,  $P_{pm}(G)\subseteq Q$, so
  we need to prove $Q\subseteq P_{pm}(G)$.

  Let  $\vx$  be  a  vertex  of  $Q$,  $E_\vx:=\{e\mid  x_e>0\}$,  and
  $F:=(V,E_\vx)$  the  sub-graph defined  by  $F$.   Suppose that  $F$
  contains  a cycle  $C$.  As  $G$ is  bipartite, $C$  must  have even
  length, and $C$ can be  split into the disjoint union $C=M\dotcup N$
  of two matchings in $G$.

  For   small  $\eps>0$, both
  \begin{align*}
    \vy_+&:=\vx+\eps(\chi^M-\chi^N)&&\text{and}&
    \vy_-&:=\vx-\eps(\chi^M-\chi^N)
  \end{align*}
  satisfy  \eqref{eq:pm}.    Hence,  $\vx=\frac12(\vy_++\vy_-)$  is  a
  convex combination  of two different vectors  representing $\vx$, so
  $\vx$  is not  a vertex  (by Theorem~\ref{thm:verticesofcanonical}).
  This contradicts  the choice  of $\vx$, so  $F$ contains  no cycles.

  Hence $F$ is a forest, so any connected component of $F$ contains at
  least one node  of degree $1$. It follows  from \eqref{eq:pm:v} that
  all nodes  have degree  $1$.  Thus, $E_\vx$  is a  perfect matching.
  \else \texttt{missing} \fi
\end{proof}%
\fi%

%%%%%%%%%%%%% Margin figure
\marginnote{ 
  \centering
  \includegraphics[width=.25\textwidth]{graph2}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:two-triangles}}
  \end{minipage}
  \\[2\baselineskip]
  \includegraphics[width=.25\textwidth]{graph3}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:triangle-leaf}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\ifreleasedproof%
Here  is  an   alternative  proof  of  the  theorem,   that  uses  the
characterization of the Birkhoff polytope.
\begin{proof}%
  As  before,   let    $Q:=\{\vx\in\R^{|E|}\mid \vx     \text{   satisfies
    \eqref{eq:pm:e} and \eqref{eq:pm:v}}\}$.   Then $Q$ is  a polytope
  that contains $P_{pm}(G)$.

  Let $X, Y$ be the two colour classes of $G$,  and let $\vx$ be a point
  of $Q$. By (\ref{eq:pm:v}) we obtain
  \begin{align*}
    |X|    =    \sum_{v\in    X}\vx(\setadjedges(v))   =    \sum_{v\in
      Y}\vx(\setadjedges(v)) = |Y|\,.
  \end{align*}
  so $|X|=|Y|=:n$.   Define a matrix $A=(a_{ij})_{ij}$ by $a_{ij}=x_e$
  if  there is the edge  $e$ between the $i$-th vertex  of $X$ and the
  $j$-th vertex of $Y$, and $a_{ij}=0$ otherwise.

  By (\ref{eq:pm}),  $A$ is a  doubly stochastic matrix.   By Exercise
  12.4,  it  is a  convex  combination  of  permutation matrices.   By
  non-negativity,  these   permutation  matrices  have   zero  entries
  whenever $A$  has a zero entry.   So any permutation  matrix in this
  combination correspond to a perfect matching in $G$.  Hence $\vx$ is
  a convex combination of  characteristic vectors of perfect matchings
  in $G$.
\end{proof}%
\fi%
\begin{example}\label{ex:bipnotenough}
  \begin{compactenum}
  \item For a general graph the first two sets of inequalities are not
    enough: Let $G$ be the  union of two two disjoint triangles, i.e.\
    the graph
    \begin{align*}
      G &=   (V, E)& V&=  \{A, B,  C, a, b,   c\}& E&= \{(A,B), (B,C),
      (A,C), (a,b), (b,c), (a,c)\}\,.
    \end{align*}
    Then  $\vx=\frac12(1,1,1,1,1,1)$ is  a solution  of the  first two
    sets  of inequalities,  but clearly  $G$ does  not have  a perfect
    matching, so $P=\varnothing$. See Figure~\ref{fig:two-triangles}.
  \item The converse of the theorem is not true. Let
    \begin{align*}
      G&:=(V,E)&V&:=\{a,b,c,d\}&E&:=\{(a,b),(b,c),(c,d),(d,b)\}\,.
    \end{align*}
    See Figure~\ref{fig:triangle-leaf}. Then $P_{pm}(G)=\{(1,0,1,0)\}$
    is characterized by (\ref{eq:pm}).
  \end{compactenum}
\end{example}
Using  this theorem  one  can  derive a  description  of the  matching
polytope  $P_m(G)$, which  is defined  to be  the convex  hull  of all
characteristic vectors  $\chi^M$ of all matching $M\subset  E$ in $G$.
Consider the set of inequalities
\begin{subequations}\label{eq:bm}
  \begin{align}
    x_e&\ge 0&&\text{for all $e\in E$}\label{eq:bm:e}\\
    \vx(\setadjedges(v))&\le        1&&\text{for       all       $v\in
      V$}\label{eq:bm:v}\,.
  \end{align}
\end{subequations}
%%%%%%%%%%%%% Margin figure
\marginnote{ 
  \centering
\small
  \psfrag{0}{$0$}
  \psfrag{2}[cr][cr]{$\nicefrac12$}
  \psfrag{3}[ct][ct]{$\nicefrac13$}
  \psfrag{4}[cb][cb]{$\nicefrac14$}
  \psfrag{4a}[ct][ct]{$\nicefrac14$}
  \psfrag{6}[cb][cb]{$\nicefrac16$}
  \psfrag{34}[ct][ct]{$\nicefrac34$}
  \psfrag{23}[bc][bc]{$\nicefrac23$}
  \includegraphics[width=.3\textwidth]{matchbippoly_side}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:matchbippoly}}
  \end{minipage}
  \\[\baselineskip]
  \includegraphics[width=.3\textwidth]{matchbippoly_side_2}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:matchbippoly2}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% 

Clearly  any  convex combination  of  incidence  vectors of  matchings
satisfies these inequalities.
\begin{proposition}\label{prop:matchbippoly}
  The matching   polytope  $P_m(G)$ of a  graph   $G$ is determined by
  (\ref{eq:bm:e}) and (\ref{eq:bm:v}) if and only if $G$ is bipartite.
\end{proposition}
\ifproof
\begin{proof}%
  \ifreleasedproof%
  ``$\Rightarrow$'':\  If $G$  is not  bipartite then  $G$ has  an odd
  cycle $C$.   Let $x_e:=\frac12$ if $e\in C$,  and $x_e=0$ otherwise.
  Then  $x$  satisfies  (\ref{eq:bm})  but  is  not  in  the  matching
  polytope.

  ``$\Leftarrow$'':\  Define  a  new  graph $H=(W,F)$  by  taking  two
  disjoint copies $G_1$ and  $G_2$ of $G$ and connecting corresponding
  vertices in the two copies $G_1$  and $G_2$ by an edge.  Then $H$ is
  bipartite   if    $G$   is   bipartite.    
  
  The    first   graph   in  Figure~\ref{fig:matchbippoly} shows $H$ if 
  $G$  is  a square.  For  any  $\vx\in \R^{|E|}$  construct a point in 
  $\vy\in \R^{|F|}$ by
  \begin{compactenum}
  \item $y_e=x_e$ if $e$ is an edge in $G_1$ or $G_2$, and
  \item  $y_e=1-\vx(\setadjedges(u_1))$ if  $e=(u_1,u_2)$  is an  edge
    between corresponding nodes in the two copies $G_1$ and $G_2$.
  \end{compactenum}
  By this construction we have
  \begin{align*}
    &\text{$\vx$      satisfies      (\ref{eq:bm})}&&\Longrightarrow&&
    \text{$\vy$ satisfies (\ref{eq:pm})}\,.
  \end{align*}
  Hence, $\vy$  is a  convex combination of  perfect matchings  in the
  graph  $H$, which  implies that  $\vx$  is a  convex combination  of
  matchings   in   $G$.    Figure~\ref{fig:matchbippoly2}  shows   the
  decomposition  into   perfect  matchings   for  the  graph   $H$  of
  Figure~\ref{fig:matchbippoly}.%
  \else \texttt{missing} \fi
\end{proof}
\fi%

Now we  head for general graphs.   These are much  more complicated to
handle. Again we first deal  with perfect matchings.  The general case
follows  from this  with  almost  the same  proof  as for  Proposition
\ref{prop:matchbippoly}.   

Consider  the  following system  of  linear inequalities.
\begin{align}
  \vx(\setadjedges(U))&\ge 1&&\text{for  all $U\subseteq V$  such that
    $|U|$ is odd}.\label{eq:pm:odd}
\end{align}

It  is easy  to  see that  these  inequalities are  satisfied for  any
perfect matching in  a graph $G$. The following  theorem tells us that
together  with  the  previous  inequalities  (\ref{eq:pm})  they  also
suffice.   We  will later  see  in  Chapter~\ref{ch:chvatal} that  the
additional  inequalities arise  naturally from  a construction  of the
integer hull of a general polyhedron.
\begin{theorem}[Edmonds 1965]\label{thm:edmonds}
  Let  $G=(V,E)$ with $|V|$  even.  Then its perfect matching polytope
  is
  \begin{align*}
    P_{pm}(G)=\{\vx\mid \vx\text{ satisfies } \eqref{eq:pm} \text{ and
    }\eqref{eq:pm:odd}\}\,.
  \end{align*}
\end{theorem}
\ifproof
\begin{aproof}%
  \ifreleasedproof%
  Let  $Q$   be   the   polytope  determined   by   \eqref{eq:pm}  and
  \eqref{eq:pm:odd}.  Then we clearly have $P_{pm}(G)\subseteq Q$.

  Suppose that the  converse is not true.  Among  those graphs $G$ for
  which  $Q\ne  P_{pm}(G)$  choose  one  with $|V|+|E|$  as  small  as
  possible.  We may assume that $|V|$ is even, as otherwise $\vx(V)\ge
  1$ by the  third condition, which implies $Q=\varnothing=P_{pm}(G)$.
  The same  argument also shows  that each connected component  of $G$
  has an even  number of vertices.  Let $\vy\in Q$ be  a vertex of $Q$
  that is not in $P_{pm}(G)$.

%%%%%%%%%%%%% Margin figure
\marginnote{ 
  \centering
\small
  \psfrag{0}{$0$}
  \psfrag{14}[cr][cr]{$\nicefrac14$}
  \psfrag{18}[ct][ct]{$\nicefrac18$}
  \psfrag{12}[cb][cb]{$\nicefrac12$}
  \psfrag{38}[ct][ct]{$\nicefrac38$}
  \psfrag{58}[cb][cb]{$\nicefrac58$}
  \includegraphics[width=.25\textwidth]{matchingpoly1_2a}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:matchingpoly1a}}
  \end{minipage}
  \\[\baselineskip]
  \includegraphics[width=.25\textwidth]{matchingpoly1_2b}\mbox{ }\\[\baselineskip]
  \begin{minipage}{.25\textwidth}
    \captionof{figure}{ \label{fig:matchingpoly1b}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  \begin{claim}\label{claim:pmpoly:vert}
    $0< y_e< 1$ for all $e\in E$.
  \end{claim}
  \begin{claimproof}
    Assume $y_e=0$.  Let
    \begin{align*}
      E'&:=E-\{e\}&  G'&:=(V,E')&  Q&:=\{\vx\in\R^{|E'|}\mid \vx\text{
        satisfies  }  (\ref{eq:pm})  \text{  and  }  (\ref{eq:pm:odd})
      \text{ for } G'\}\,.
    \end{align*}
    Then the projection of $\vy$  onto $\R^{|E'|}$ is in $Q'$, but not
    in $P_{pm}(G')$, so $G'$ would be a smaller counter-example.

    If $y_e=1$,  then either $e$  is an isolated  edge or there  is an
    incident  edge  $f$.   In  the  first case  we  obtain  a  smaller
    counterexample by removing $e$ and  its end points.  In the second
    case $y_f=0$  by (\ref{eq:bm:v}), and  we could remove $f$  by the
    previous argument.
  \end{claimproof}
  By (\ref{eq:pm:v}) each node has  degree at least $2$.  This implies
  $|E|\ge  |V|$ by  double  counting.   If $|E|=|V|$,  then  $G$ is  a
  collection of  cycles, and by (\ref{eq:pm:odd}) each  cycle has even
  length.  So  $G$ would be a  bipartite graph. However,  in this case
  the theorem  follows from Theorem \ref{thm:bipperfmatch},  so $G$ is
  not be a counter-example. Hence we can assume that $|E|> |V|$.

  By  the choice  of $\vy$  there must  be $|E|$  linearly independent
  constraints   among   the    inequalities   in   \eqref{eq:pm}   and
  \eqref{eq:pm:odd} that are satisfied with equality.

  By Claim \ref{claim:pmpoly:vert} no inequality in \eqref{eq:pm:e} is
  satisfied  with  equality.   As  $|E|>|V|$,  at  least  one  of  the
  inequalities  in  \eqref{eq:pm:odd}   is  satisfied  with  equality.
  Hence,   there   is  an   odd   subset   $U$   of  $V$   such   that
  $\vy(\setadjedges(U))=1$.  Let $\ov U=V-U$. Then
  \begin{align*}
    1\ =\ \vy(\setadjedges(U))\ =\ \vy(\setadjedges(\ov U))\,.
  \end{align*}
  We  can restrict  to odd  subsets of  size $3\le  |U|\le  |V|-3$, as
  otherwise   the   equation  is   already   contained   in  the   set
  \eqref{eq:pm:v}).  

  Let $H_1:=(V_1, E_1):=G/\ov U$. So  $H_1$ is a graph with vertex set
  $V_1:=U\cup\{\ov u\}$ and edge set
  \begin{align*}
    E_1:=\{e\in E\mid e\subseteq U\}\cup\{(v,\ov  u)\mid v\in U \text{
      and there is $w\in \ov U$ such that $(v,w)\in E$}\}\,.
  \end{align*}

  Define a projection $\vy^{(1)}$ of $\vx$ onto the edge set of $H_1$ by
  \begin{align*}
    y^{(1)}_e&:=y_e\qquad\text{for  $e\subseteq U$}  & y^{(1)}_{(v,\ov
      u)}       &:=\sum_{{w\in       \ov      U}\,,{(v,w)\in       E}}
    y_{(v,w)}\qquad\text{for $v\in U$}\,.
  \end{align*}
  Figure~\ref{fig:matchingpoly1a} shows an example  of a graph $G$ and
  a subset $U$ of the vertices.  Figure~\ref{fig:matchingpoly1b} shows
  its two projections $H_1$ and $H_2$.
  \begin{claim}
    $\vy^{(1)}$  satisfies  \eqref{eq:pm}  and  \eqref{eq:pm:odd}  for
    $H_1$.
  \end{claim}
  \begin{claimproof}
    By definition, $\vy^{(1)}\ge  0$, so (\ref{eq:pm:e}) is satisfied.
    Now look  at the inequalities (\ref{eq:pm:v}).  We distinguish two
    cases, vertices $v\in U$ and $\ov u$.
    \begin{align*}
      &v\in U:&\vy^{(1)}(\setadjedges(v))&
      % 
      = \sum_{w\in U,\, (v,w)\in E}y^{(1)}_{(v,w)}%
      + \sum_{w\in \ov U} y^{(1)}_{(v,w)} = \vy(\setadjedges(v))%
      = 1\\
      &&\vy^{(1)}(\setadjedges(\ov    u))&  = \sum_{{v\in   U\,  w\in\ov
          U}\atop {(v,w)\in E}} y_{(v,w)} = \vy(\setadjedges(U)) = 1\,.
    \end{align*}
    by the particular choice of $U$.  

    Let $W\subseteq U\cup\{\ov u\}$, $|W|$ odd.  Again, we distinguish
    two cases, $\ov u\not\in W$ and $\ov u\in W$.
      
    If  $\ov w\not\in  W$, then  by the  definition of  the  values of
    $\vy^{(1)}$    on    the    edges    connected   to    $\ov    u$.
    $\vy^{(1)}(\setadjedges(W)) = \vy(\setadjedges(W)) \ge 1$.
      
    If $\ov u\in  W$, then let $W':=W-\{\ov u\}$  and $W'':=U-W'$.  As
    $|W|$  is  odd,  $|W'|$  is  even,  and  $|W''|$  is  odd.   Thus,
    $\setadjedges(W)$ is  the set of all edges  between $W'$and $W''$,
    and all edges between $\ov u$ and $W''$.  Hence, by our definition
    of the values of $\vy^{(1)}$ on the edges,
    \begin{align*}
      \vy^{(1)}(\setadjedges(W))=\vy(\setadjedges(W'')) \ge 1\,.
    \end{align*}
    This proves  that $y^{(1)}$ also  satisfies (\ref{eq:pm:odd}).  So
    in total, $\vy^{(1)}$  satisfies all inequalities in (\ref{eq:pm})
    and (\ref{eq:pm:odd}) with respect to the graph $H_1$.
  \end{claimproof}
  Now  $H_1$ has less  vertices and  edges than  $G$, so  it is  not a
  counterexample, so  $\vy^{(1)}$ is in the  perfect matching polytope
  $P_{pm}(H_1)$ of $H_1$.

  Similarly define the graph  $H_2:=(V_2, E_2):=G/ U$.  The projection
  $\vy^{(2)}$ of  $\vy$ onto $\R^{|E_2|}$  satisfies (\ref{eq:pm}) and
  (\ref{eq:pm:odd}), so $\vy^{(2)}\in P_{pm}(H_2)$.


  \begin{figure}[tb]
    \centering
    \psfrag{a}[cr][cr]{$\frac14$}
    \psfrag{b}[cr][cr]{$\frac18$}
    \psfrag{c}[cr][cr]{$\frac12$}
    \psfrag{d}[cr][cr]{$\frac18$}
    \psfrag{e}[cl][cl]{$\frac12$}
    \psfrag{f}[cl][cl]{$\frac14$}
    \psfrag{g}[cl][cl]{$\frac14$}
    \includegraphics[width=\textwidth]{matchingpoly1b}
    \caption{The decomposition of $y^{(1)}$ and $y^{(2)}$ into perfect
      matchings.}
    \label{fig:matchpoly1b}
  \end{figure}
  So  there are  perfect  matchings $M_1^{(1)},  \ldots, M_k^{(1)}$ of
  $H_1$ and $N_1^{(2)},   \ldots,  N_l^{(2)}$ of  $H_2$  together with
  coefficients      $\lambda_1^{(1)},   \ldots,\lambda_k^{(1)}$    and
  $\lambda_1^{(2)}, \ldots,\lambda_l^{(2)}$ such that
  \begin{align*}
    \vy^{(1)}&=\sum_{i=1}^k\lambda_i^{(1)}\chi^{M_i^{(1)}}&
    \vy^{(2)}&=\sum_{j=1}^l\lambda_j^{(2)}\chi^{N_j^{(2)}}\,.
  \end{align*}
  See Figure~\ref{fig:matchpoly1b} for  the perfect matchings obtained
  for  the  example   shown  in  Figures~\ref{fig:matchingpoly1a}  and
  \ref{fig:matchingpoly1b}.  $y$  is rational, so  by multiplying both
  equations  with  a  suitable  factor,  and  repeating  matchings  if
  necessary, we can assume that
  \begin{align*}
    \vy^{(1)}&=\frac1p\sum_{i=1}^p                    \chi^{M_i^{(1)}}&
    \vy^{(2)}&=\frac1p\sum_{i=1}^p \chi^{N_i^{(2)}}\,.
  \end{align*}
  for  some $p\in\N$.  We  can even  assume that  $p$ is  chosen large
  enough  that $py_e$  is integer  for all  $e\in E$.

  We can lift  a perfect matching $M_i^{(1)}$ to a  matching $ M_i$ in
  $G$ that covers  all edges in $\ov U$ and exactly  one vertex in $U$
  (so it contains exactly one edge from $\setadjedges(U)$).
  \begin{figure}[btp]
    \centering
    \includegraphics[height=.105\textheight]{matchingpoly2}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly3}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly4}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly5}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly6}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly7}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly8}

    \vspace{1em}

    \includegraphics[height=.105\textheight]{matchingpoly9}
    \caption{The lifted and paired matchings of the projected graphs}
    \label{fig:matchingpoly}
  \end{figure}
  Let $v\in \ov U$ and $f:=(v,\ov u)$.  Then by construction
  \begin{align*}
    py^{(1)}_f=\sum_{w\in\ov U}py_{(v,w)},.
  \end{align*}
  Hence, we can choose the liftings  $M_i$ in such a way that any edge
  $e\in\setadjedges(U)$  is  contained  in  exactly  $py_e$  of  them.
  Similarly, we can lift  the matchings $N_j^{(2)}$ to matchings $N_j$
  in such  a way that  any edge $e\in\setadjedges(U)$ is  contained in
  exactly  $py_e$ of them.  See Figure~\ref{fig:matchingpoly}  for the
  liftings in our example..

  Thus, we  can pair  the  lifted matchings according  to  the edge in
  $\setadjedges(U)$  they contain.  Relabel  the matchings from $1$ to
  $p$    so that $M_k$   and    $N_j$  contain  the   same  edge  from
  $\setadjedges(U)$.  Then $L_k:=M_k\cup N_k$ is a perfect matching in
  $G$ and
  \begin{align*}
    \vy=\frac1p\sum_{k=1}^p\chi^{L_k}\,.
  \end{align*}
  This  implies  that  $\vy\in  P_{pm}(G)$, in  contradiction  to  our
  assumption. So $Q=p_{pm}(G)$\mqed %
  \else \texttt{missing} \fi
\end{aproof}%
\fi%

Recall that $E(U)$ for some $U\subseteq V$ is the set of all edges $e$
with both end points in $U$,
\begin{align*}
  E(U):=\{e\in e\mid e\subseteq U\}\,.
\end{align*}
The characteristic vector of a matching in $G$ clearly satisfies
\begin{subequations} \label{eq:m}
  \begin{align}
    x_e&\ge 0&&\text{for all $e\in E$}\label{eq:m:e}\\
    \vx(\setadjedges(v))&\le 1&&\text{for all $v\in V$}\label{eq:m:v}\\
    \vx(E(U))&\le \frac12(|U|-1)&&\text{for all $U\subseteq V$ such that
      $|U|$ is odd}.\label{eq:m:odd}
  \end{align}
\end{subequations}
The next theorem also shows that these inequalities suffice. Its proof
is      completely      analogous       to      the      proof      of
Proposition~\ref{prop:matchbippoly}.
\begin{theorem}\label{thm:general-matching-poly}
  The  matching polytope $P_m(G)$  of an undirected graph $G=(V,E)$ is
  given by
  \begin{align*}
    P_{pm}(G)=\{\vx\mid \vx\text{ satisfies } \eqref{eq:m}\}\,.
  \end{align*}
\end{theorem}
\ifproof
\begin{proof}%
\ifreleasedproof%
  Each vector   in  the matching polytope  satisfies  the inequalities
  (\ref{eq:m}).

  We need  to prove  that they suffice.   Define a  graph $H=(W,F)$ by
  taking  two disjoint copies  $G_1=(V_1,E_1)$ and $G_2=(V_2, E_2)$ of
  $G$   and connect corresponding vertices by   an  edge.  For subsets
  $X_1\subseteq V_1$  let $X_2\subseteq V_2$  be the corresponding set
  in $G_2$ and vice versa.

  For any $\vx\in \R^{|E|}$ we construct a point $\vy\in \R^{|F|}$ by
  \begin{compactenum}
  \item $y_e=x_e$ if $e$ is an edge in $G$ or $G'$, and
  \item   $y_e=1-\vx(\setadjedges(u_1))$   if $e=(u_1,u_2)$  is  an edge
    between corresponding vertices in the two copies $G_1$ and $G_2$.
  \end{compactenum}
  
  Assume that $x$ satisfies (\ref{eq:m}). Then
  \begin{compactenum}
  \item $\vy\ge 0$.
  \item               For         each      $v_1\in              V_1$,
    $\vy(\setadjedges_H(v_1))=\vy(\setadjedges_H(v_2))=1$.
  \item  Let $U=X_1\cup Y_2$ be an  odd subset of $W=V_1\cup V_2$ with
    $X_1, Y_1\subseteq V_1$. Then clearly
    \begin{align*}
      \vy(\setadjedges_H(U))\ge
      \vy(\setadjedges_H(X_1-Y_1))+\vy(\setadjedges_H(Y_2-X_2))\,.
    \end{align*}
    So we may assume that $X_1, Y_1$ are disjoint.
    
    One  of the sets  $X_1, Y_1$ must be  odd.   So we may assume that
    this is $X_1$.  Then we can as well assume that $Y_1=\varnothing$.

    So  it suffices to show that   $\vy(\setadjedges_H(U))\ge 1$ for all
    odd $U\subseteq V_1$.  Now
    \begin{align*}
      |U| &= \sum_{v\in U} \vy(\setadjedges_H(v)) = \vy(\setadjedges_H(U))
      + 2\vy(E_H(U))%
      \intertext{so  that}  \vy(\setadjedges(U))& = |U|  - 2\vy(E_H(U))\ge
      |U|-2(\frac12(|U|-1)) = 1\,.
    \end{align*}
  \end{compactenum}
  Hence, $\vy$ satisfies (\ref{eq:bm}), so it is in the perfect matching
  polytope of $H$, so $\vx$ is in the matching polytope of $G$.
\else
\texttt{missing}
\fi
\end{proof}%
\fi%


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "geometry_and_optimization"
%%% End: 
