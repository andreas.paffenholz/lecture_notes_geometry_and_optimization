\chapter{Linear Programming and Duality}
\label{cha:duality}

This section introduces linear  programming as an optimization problem
of  a  linear  functional  over  a polyhedron.   We  explain  standard
terminology  and conversions  between different  representations  of a
linear program,  before we define  dual linear programs and  prove the
Duality Theorem. This theorem will  be an important tool for the study
of faces of polyhedra in the next section.

\emph{Linear programming}\mdef{linear programming}  is a technique for
minimizing     or      maximizing     a     linear     \emph{objective
  function}\mdef{objective function} over a  set $P$ defined by linear
inequalities  and equalities.   More explicitly,  let $A\in\R^{p\times
  n}$,   $E\in\R^{q\times  n}$,   $\vb\in  \R^p$,   $\vf\in\R^q$,  and
$\vc\in(\R^n)^*$.   Then we  want to  find the  maximum  of $\vc^t\vx$
subject to the constraints
\begin{align*}
  A\vx&\le \vb\\
  E\vx&= \vf\,.
\end{align*}
This  system  defines a  polyhedron  $P\subseteq\R^n$.  Techniques  to
explicitly and  efficiently compute  the maximal value  $\vc^t\vx$ use
algebraic manipulations on the representation of the polyhedron $P$ to
obtain a  representation in  which the maximal  value can  be directly
read of  from the  system.  We introduce  two representations  that we
will use quite often and explain  how to convert between them. We will
first do the algebraic manipulations  and later see what this means in
geometrical terms.
\begin{definition}\label{def:standard-canonical-form}
  A  linear  program in  \emph{standard  form}\mdef{standard form}  is
  given by a matrix $A\in\R^{m\times  n}$, a vector $\vb\in \R^m$, and
  a cost vector $\vc^t\in\R^n$ by
  \begin{align*}
    \text{maximize}&& \vc^t\vx\\
    \text{subject to}&& A\vx&=\vb\\
    &&\vx&\ge \0\,.&&\tag{non-negativity constraints}
  \end{align*}
  A program in \emph{canonical  form}\mdef{canonical form} is given as
  \begin{align*}
    \maximize \vc^t\vx\\
    \subjectto A\vx&\le \vb\\
    && \vx&\ge \0\,.&&\tag{non-negativity constraints}
  \end{align*}
\end{definition}
Note that the definition of a  standard form is far from unique in the
literature.   To  justify this  definition  we  show  that any  linear
program  can be  transformed into  standard form  (or into  any other)
without changing  the solution  set.  Here is  an example  that should
explain all necessary techniques.  Consider the linear program
\begin{alignat*}{5}
  \text{maximize}\quad&2x_1+3x_2&\quad
  \text{subject to}\quad 3x_1-x_2&\ge 2\\
  && 4x_1+x_2&\le5\\
  &&x_1&\le 0%
  \intertext{we can reverse the inequality signs and pass to equations
    using additional auxiliary variables $x_3, x_4$}%
  \text{maximize}&\quad2x_1+3x_2&\quad
  \text{subject to}\quad-3x_1+x_2+x_3\phantom{+x_4}&= -2\\
  && 4x_1+x_2\phantom{+x_4}+x_4&= \phantom{-}5\\
  &&x_1&\le \phantom{-}0\\
  &&x_3, x_4&\ge \phantom{-}0 %
  \intertext{correct   the   variable   constraints  by   substituting
    $y_1:=-x_1$}%
  \text{maximize}\quad&-2y_1+3x_2&\quad
  \text{subject to}\quad3y_1+x_2+x_3\phantom{+x_4}&= -2\\
  && -4y_1+x_2\phantom{+x_4}+x_4&= \phantom{-}5\\
  &&x_3, x_4,y_1&\ge \phantom{-}0%
  \intertext{    add   constraints    for   $x_2$    by   substituting
    $x_2=y_2-y_3$}%
  \text{maximize}\quad&-2y_1+3y_2-3y_3&\quad
  \text{subject to}\quad3y_1+y_2-y_3+x_3\phantom{+x_4}&= -2\\
  && -4y_1+y_2-y_3\phantom{+x_4}+x_4&= \phantom{-}5\\
  &&x_3, x_4,y_1,y_2,y_3&\ge \phantom{-}0%
  \intertext{    normalize   by    renaming    $y_1\rightarrow   x_1$,
    $y_2\rightarrow x_2$,  $y_3\rightarrow x_3$, $x_3\rightarrow x_4$,
    $x_4\rightarrow x_5$,}%
  \text{maximize}\quad&-2x_1+3x_2-3x_3&\quad
  \text{subject to}\quad3x_1+x_2-x_3+x_4\phantom{+x_4}&= -2\\
  && -4x_1+x_2-x_3\phantom{+x_4}+x_5&= \phantom{-}5\\
  &&x_1, x_1,x_3, x_4, x_5&\ge \phantom{-}0
\end{alignat*}
If we want to write this in matrix form, $A$, $b$ and $c$ are given by
\begin{align*}
  A=\left(
    \begin{array}{rrrrr}
      3&1&-1&1&0\\-4&1&-1&0&1
    \end{array}\right)\qquad \vb=\left(
    \begin{array}{r}
      -2\\5
    \end{array}\right)\qquad \vc^t=\left(
    \begin{array}{rrrrr}
      -2&3&-3&0&0
    \end{array}\right).
\end{align*}
An  optimal   solution  of  this   transformed  system  is   given  by
$(-1,1,0,0,0)$,  which we can  transform back  to an  optimal solution
$(1,1)$ of the original system.  Here is the general recipe:

\begin{center}
  \begin{tabular}{rcl}
    minimize $\vc^t\vx$&$\; \longleftrightarrow$ &maximize $-\vc^t\vx$.\\
    $\va_i^t\vx\ge b_i$&$\;\longleftrightarrow \; $&$-\va_i^t\vx\le -b_i$\\
    $\va_ix=b_i$&$\;\longleftrightarrow\; $&$\va_i^t\vx\le b_i$ and $\va_i\vx\ge b_i$.\\
    $\va_i\vx\le b_i$&$\;\longleftrightarrow\; $&$\va^t_i\vx+s_i=b_i$ and $s_i\ge 0$.\\
    $x_i\in \R$&$\;\longleftrightarrow \; $&$x_i=x^+_i-x^-_i$, $x_i^+,
    x_i^-\ge 0$.
  \end{tabular}
\end{center}
\begin{definition}
  The variables $s_i$ introduced in  rule $(4)$ are called \emph{slack
    variables}\mdef{slack variables}.
\end{definition}

\begin{definition}
  A linear maximization program is
  \begin{compactenum}
  \item \emph{feasible}\mdef{feasible\\feasible solution}, if there is
    $\vx\in\R^n$ satisfying  all constraints.  $\vx$ is  then called a
    \emph{feasible solution}.
  \item \emph{infeasible}\mdef{infeasible} if it is not feasible.
  \item \emph{unbounded}\mdef{unbounded}, if there is no $M\in\R$ such
    that $\vc^t\vx\le M$ for all feasible $\vx\in\R^n$.
  \end{compactenum}
  An \emph{optimal  solution}\mdef{optimal solution\\optimal value} is
  a feasible  solution $\ov\vx$  such that $\vc^t\vx\le  \vc^t\ov \vx$
  for all  feasible $\vx$.  Its value $\vc^t\vx$  is the \emph{optimal
    value} of the program.
\end{definition}
Note that an optimal solution need not be unique. We will learn how to
compute    the    maximal   value    of    a    linear   program    in
Section~\ref{cha:simplex-method}.  We  can completely characterize the
set of linear functionals that lead to a bounded linear program.
\begin{proposition}\label{prop:bounded-via-reccone}
  Let  $A\in\R^{m\times  n}$,  $\vb\in\R^m$, and  $P:=\{\vx\in\R^n\mid
  A\vx\le \vb\}$.
  \begin{compactenum}
  \item The linear program  $\max(\vc^t\vx\mid \vx\in P)$ is unbounded
    if  and   only  if  there  is  a   $\vy\in\reccone(P)$  such  that
    $\vc^t\vy>0$.
  \item Assume $P\ne  \varnothing$. Then $\max(\vc^t\vx\mid \vx\in P)$
    is feasible bounded if and only if $\vc\in\reccone(P)^*$.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
\ifreleasedproof%
\begin{compactenum}
\item   If  $\max(\vc^t\vx\mid  A\vx\le   \vb)$  is   unbounded,  then
  $\min(\vz^t\vb\mid \vz^tA=\vc^t,\, \vz\ge  0)$ is infeasible. Hence,
  there   is   no   $\vz\ge   0$  such   that   $\vz^tA=\vc^t$.    The
  \textsc{Farkas}       Lemma      (in       the       version      of
  Proposition~\ref{prop:farkas-variants}) gives us a vector $\vy\le 0$
  such that $A\vy\le \0$ and $\vy^t\vb>0$.

  Now assume that there is such  a vector $\vy$.  Let $\vx\in P$, then
  $\vx+\lambda   \vy\in   P$   for   all   $\lambda\ge   0$.    Hence,
  $\vc^t(\vx+\lambda \vy)=\vc^t\vx+\lambda \vc^t\vy$ is unbounded.
\item As  $P\ne \varnothing$,  the program is  feasible.  There  is no
  $\vy\in\reccone(P)$    with   $\vc^t\vy>0$    if    and   only    if
  $\vc\in\reccone(P)^*$ by the definition of the dual cone.
\end{compactenum}
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

Now  we want  to show  that  we can  associate to  any linear  program
another linear  program, called  the \emph{dual linear  program} whose
feasible solutions provide some  information about the original linear
program. We start with some general observations and an example.

Let $A\in\R^{m\times n}$, $\vb\in\R^m$, and $\vc\in\R^n$. Consider the
two linear programs in canonical form:
\begin{align*}
  \max(\vc^t\vx&\mid A\vx\le \vb,\, \vx\ge \0)\tag{P}\\
  \min(\vy^t\vb&\mid \vy^tA\ge \vc^t,\, \vy\ge \0)\tag{D}
\end{align*}
Assume that  the first linear  program has a feasible  solution $\vx$,
and the second linear program a feasible solution $\vy$. Then
\begin{align*}
  \vc^t\vx\le \vy^tA\vx\le \vy^t\vb\,,
\end{align*}
where the  first inequality  holds, as $\vx\ge  0$ and the  second, as
$\vy\ge 0$.   Thus, any feasible  solution of $(D)$ provides  an upper
bound for the  value of $(P)$.  The best possible  upper bound that we
can  construct this  way  is assumed  by $\min(\vy^t\vb\mid  \vy^tA\ge
\vc^t, \vy\ge \0)$.
\begin{example}
  Let
  \begin{align*}
    A:=\left(
      \begin{array}{rr}
        8&6\\
        2&6\\
        3&5
      \end{array}\right)\qquad
    \vb:=\left(
      \begin{array}{r}
        22\\10\\12
      \end{array}\right)\qquad
    \vc^t:=\left(
      \begin{array}{rr}
        2&3
      \end{array}\right)
  \end{align*}
  and  consider the  linear program  $(P)$ and  $(D)$ as  above.  Then
  $\vy=(1,0,0)$ is feasible for $(D)$ and we obtain
  \begin{alignat*}{3}
    2x_1+3x_2\,&\le\, 8x_1+6x_2\,\le\, 22
%
    \intertext{ so  the optimum  of $(P)$ is  at most $22$.   From the
      computation you  can see that we  overestimated the coefficients
      by a factor of at least $2$, so a much better choice would be to
      take $\vy=(\frac12,0,0)$, which leads to}%
    2x_1+3x_2&\le\, 4x_1+3x_2\,\le\, 11\,.%
    \intertext{So far, we  have only used one of  the inequalities. As
      long  as  we take  non-negative  scalars,  we  can also  combine
      them. Choosing $\vy=(\frac16,\frac13,0)$ gives}%
    2x_1+3x_2&=\frac16(8x_1+6x_2+4x_1+12x_2)\le\frac16(22+20)&=\frac{42}{6}=7\,.
  \end{alignat*}
  Hence, the optimum is  at most $7$.   It is exactly $7$, as  $x_1=2,
  x_2=1$ is a feasible solution of $(P)$.
\end{example} 
The program  $(D)$ is called the  \emph{dual linear program}\mdef{dual
  linear program} for  the linear program $(P)$, which  is then called
the \emph{primal linear program}\mdef{primal linear program}.  We have
already proven the following proposition.
\begin{proposition}[Weak Duality Theorem]\mdef{weak duality theorem}
  For  each feasible  solution  $\vy$ of  $(D)$  the value  $\vy^t\vb$
  provides  an upper  bound  on the  value  of $(P)$,  i.e.\ for  each
  feasible solution $\vx$ of $(P)$ we have
  \begin{align*}
    \vc^t\vx\le \vy^t\vb.
  \end{align*}
  In   particular, if either  of  the programs  is unbounded, then the
  other is infeasible.\mqed
\end{proposition}

However, we  can prove a  much stronger relation between  solutions of
the primal and dual program.
\begin{theorem}[Duality Theorem]\mdef{duality theorem}
  For the linear programs $(P)$ and $(D)$ exactly one of the following
  possibilities is true:
  \begin{compactenum}
  \item Both are feasible and their optimal values coincide.
  \item One is unbounded and the other is infeasible.
  \item Neither $(P)$ nor $(D)$ has a feasible solution.
  \end{compactenum}
\end{theorem}

A linear program  can either be
\begin{compactenum}
\item feasible and bounded   (fb),
\item infeasible (i), or
\item feasible and unbounded (fu).
\end{compactenum}
Hence,  for the  relation of  $(P)$ and  $(D)$   we a priori  have $9$
possibilities.  Three of them are excluded by the weak duality theorem
(wd), and another two are excluded by the duality theorem (d).
\begin{center}
  \begin{tabular}{l|rrr}
    \backslashbox{(P)}{(D)}&\quad(fb)&(i)&(fu)\\[5mm]
    \midrule
    (fb)&yes&(d)&(wd)\\
    (i)&(d)&yes&yes\\
    (fu)&(wd)&yes&(wd)
  \end{tabular}
\end{center}
The four remaining cases can indeed occur.
\begin{compactenum}
\item $\max(x\mid x\le 1,\,  x\ge0)$ and $\min(y\mid y\ge1,\, y\ge 0)$
  are both bounded and feasible.
\item  $\max(x_1+x_2\mid -x_1-2x_2\le  1,\,   x_1, x_2\ge 0)$ has  the
  solution $x=\R\1$, 
  and the dual program $\min(y\mid -y\ge 1,\,  -2y\ge 1,\, y\ge 0)$ is
  infeasible.  These are the programs corresponding to
  \begin{align*}
    A&=(
    \begin{smallmatrix}
      -1& 2
    \end{smallmatrix}
    )& b&=1 & \vc^t&=\left(
      \begin{smallmatrix}
        1&1
      \end{smallmatrix}\right)
  \end{align*}
\item Consider the programs $(P)$  and $(D)$ for the input data
  \begin{align*}
    &&A&:=\left(
      \begin{smallmatrix}
        \phantom{-}1&-1\\-1&\phantom{-}1
      \end{smallmatrix}\right)&\vb&:=\left(
      \begin{smallmatrix}
        \phantom{-}0\\-1
      \end{smallmatrix}\right)&\vc^t&:=\left(
      \begin{smallmatrix}
        1&1
      \end{smallmatrix}\right)\;.
  \end{align*}
  We can use the \textsc{Farkas}  Lemma to show that both programs are
  infeasible.      Choose     the     following    two     functionals
  $\vu=\vv=\left(
    \begin{smallmatrix}
      1\\1
    \end{smallmatrix}\right)$. Then 
  \begin{align*}
    \vu^tA&=0&\vu^t\vb&<0&\vu&\ge 0\\
    \vv^tA&\le 0&\vv^t\vc&>0&\vv&\ge 0
  \end{align*}
\end{compactenum}

Dual programs also exist  for linear programs  not in canonical  form,
and  it is easy to generate  it using the transformation rules between
linear   programs. 
\begin{proposition}
  Let $A,B,C$,  and $D$  be compatible matrices  and $\va,\vb,\vc,\vd$
  corresponding vectors. Let a linear program
  \begin{align*}
    \text{maximize}&&\vc^t\vx+\vd^t\vy\\
  \text{subject to}&& A\vx+B\vy&\le \va\\
  &&C\vx+D\vy&=\vb\\
  &&\vx&\ge \0
  \end{align*}
  be given. Then its dual program is
  \begin{align*}
    \text{minimize}&&\vu^t\vy+\vv^t\vb\\
  \text{subject to}&& \vu^tA+\vv^tC&\ge \vc^t\\
  &&\vu^tB+\vv^tD&=\vd^t\\
  &&\vu&\ge \0    
  \end{align*}
\end{proposition}
\ifproof%
\begin{proof}%
\ifreleasedproof%
  By our transformation rules we can write the primal as
  \begin{align*}
        \text{maximize}&&\vc^t\vx+\vd^t\vy_1-\vd^t\vv_2\\
  \text{subject to}&& A\vx+B\vy_1-B\vy_2&\le \phantom{-}\va\\
  &&C\vx+D\vy_1-D\vy_2&\le\phantom{-}\vb\\
  &&-C\vx-D\vy_1+D\vy_2&\le -\vb\\
  &&\vx, \vy_1, \vy_2&\ge \phantom{-}\0
  \end{align*}
which translates to
\begin{align*}
  \text{minimize}&&\vu^t\va+\vv_1^t\vb-\vv_2^t\vb\\
  \text{subject to}&& \vu^tA+\vv_1^tC-\vv_2^tC&\ge \phantom{-}\vc^t\\
  &&\vu^tB+\vv_1^tD-\vv_2^tD&\ge \phantom{-}\vd^t\\
  &&-\vu^tB-\vv_1^tD+\vv_2^tD&\ge \phantom{-}\vd^t\\
  &&\vu,\vv_1,\vv_2&\ge \phantom{-}\0
\end{align*}
Set $\vv:=\vv_1-\vv_2$.  and combine the  second and third inequality  to an
equality.
 \else \texttt{missing} \fi%
\end{proof}%
\fi

From this proposition we can derive a rule set that is quite
convenient for quickly writing down the dual program.  Let
$A\in\R^{m\times n}$, $\vb\in\R^m$, $\vc\in\R^n$.
\begin{center}
  \begin{tabular}{r<{\;\;}|>{\;\;}l<{\;\;}|>{\;\;}l}
    \toprule
    &primal&dual\\
    \midrule
    variables&$\vx=(x_1, \ldots, x_n)$&$\vy=(y_1, \ldots, y_m)$\\[4mm]
    matrix&$A$&$A^t$\\[4mm]
    right hand side&$\vb$&$\vc$\\[4mm]
    objective function& max $\vc^t\vx$&min $\vy^t\vb$\\[4mm]
    constraints&$i$-th constraint has\quad $\le$&$y_i\ \ge\ 0$\\
    &\phantom{$i$-th constraint has}\quad $\ge$&$y_i\ \le\ 0$\\
    &\phantom{$i$-th constraint has}\quad $=$&$y_i\ \in\ \R$\\[3mm]
    &$x_j\ \ge\ 0$&$j$-th constraint has\quad  $\ge$\\
    &$x_j\ \le\ 0$&\phantom{$j$-th constraint has}\quad $\le$\\
    &$x_j\ \in\ \R$&\phantom{$j$-th constraint has}\quad $=$\\
    \bottomrule
\end{tabular}
\end{center}

\medskip

Observe that we have one-to-one correspondences
\begin{align*}
  \text{primal variables}&\quad\Longleftrightarrow\quad\text{dual
    constraints}\\
  \text{dual      variables}&\quad\Longleftrightarrow\quad\text{primal
    constraints}
\end{align*}
This fact will  be used in the complementary  slackness theorem at the
end of this section. Now we finally prove the duality theorem.%
\ifproof
\begin{proof}[Duality Theorem]
  \ifreleasedproof %
  By the considerations that we did after the statement of the theorem
  and the fact that \emph{primal} and \emph{dual} are interchangeable,
  it suffices to prove the following:
  \begin{quote}
    If the linear program $(P)$ is feasible and bounded, then also the
    linear program $(D)$ is feasible and bounded with the same optimal
    value.
  \end{quote}

  Assume   that  $(P)$   has  an   optimal  solution   $\ov\vx$.   Let
  $\alpha:=\vc^t\ov\vx$. Then the system
  \begin{align}
    A\vx&\le \vb&\vc^t\vx&\ge\alpha& \vx&\ge \0\tag{*}\label{eq:noeps}\\
    \intertext{has a solution,  but for   any $\eps>0$, the    system}
    A\vx&\le \vb&\vc^t\vx&\ge \alpha+\eps& \vx&\ge \0 \tag{**}\label{eq:eps}
  \end{align}
  has none. Consider the extended matrices
  \begin{align*}
    \ov A&:=\left(
      \begin{array}{r}
        -\vc^t\\A
      \end{array}\right)&\ov\vb_\eps&:=\left(
      \begin{array}{r}
        -\alpha-\eps\\\vb
      \end{array}\right)\;.
  \end{align*}
  Then  \eqref{eq:eps} is equivalent  to  $\ov A\vx\le \ov\vb_\eps$, and
  \eqref{eq:noeps} is the special case $\eps=0$.

  Fix $\eps>0$.  We apply the \textsc{Farkas} Lemma (in the variant of
  Proposition~\ref{prop:farkas-variants}(\ref{item:prop:canonical}))
  to obtain a non-negative vector $\ov \vz=(z_0,\vz)\ge 0$ such that
  \begin{align*}
    \ov \vz^t\ov A&\ge 0&\text{but} \quad \ov \vz^t\ov\vb_\eps&<0\,.
  \end{align*}
  This implies 
  \begin{align*}
    \vz^tA&\ge z_0 \vc^t&\text{and}\quad
    \vz^t\vb&<z_0(\alpha+\eps)&\vz&\ge0,\, z_0\ge0\;.
  \end{align*}
  Further,  applying the  \textsc{Farkas} Lemma  for $\eps=0$,  we see
  that there  is no  such $\ov \vz$,  hence our chosen  $\ov \vz=(z_0,
  \vz)$  must satisfy $\vz^t\vb\ge  \vz_0\alpha$ (otherwise  $\ov \vz$
  would be a certificate that (\ref{eq:noeps}) has no solution!). So
  \begin{align*}
    z_0\alpha\le \vz^t\vb&< z_0(\alpha+\eps)\,.
  \end{align*}
  As  $z_0\ge 0$  this can  only  be  true  for $z_0>0$.   Hence,  for
  $\vy:=\frac1{z_0}\vz$ we obtain
  \begin{align*}
    \vy^tA&\ge \vc^t&\vy^t\vb<\alpha+\eps\;.
  \end{align*}
  So  $\vy$  is  a feasible  solution  of  $(D)$  of value  less  than
  $\alpha+\eps$ for  any chosen $\eps$.  By the  weak duality theorem,
  however, the value  is at least $\alpha$.  Hence,  $(D)$ is bounded,
  feasible  and  therefore has  an  optimal  solution.   Its value  is
  between $\alpha$ and $\alpha+\eps$ for any $\eps>0$, so
  \begin{align*}
    \vb^t\vy&=\alpha\;.
  \end{align*}
\else
\texttt{missing}
\fi
\end{proof}
\fi%
We can  further characterize the  connections between primal  and dual
solutions. Let $\vs$ and $\vr$ be the slack vectors for the primal and
dual program:
\begin{align*}
  \vs&:=\vb-A\vx&(\text{i.e.}\quad A\vx\le \vb&\Leftrightarrow \vs\ge 0)\\
  \vr^t&:=\vy^tA-\vc^t&(\text{i.e.}\quad \vy^tA\ge \vc^t&\Leftrightarrow
  \vr\ge 0)
\end{align*}
Then
\begin{align*}
  \vy^t\vs+\vr^t\vx=\vy^t(\vb-A\vx)+(\vy^tA-\vc^t)\vx=\vy^t\vb-\vc^t\vx\,,
\end{align*}
so
\begin{align*}
  \vy^t\vs+\vr^t\vx=0\qquad\Longleftrightarrow\qquad
  \vy^t\vb=\vc^t\vx\tag{$\Delta$}\label{eq:slack}\,.
\end{align*}
\begin{theorem}[complementary slackness]\mdef{complementary  slackness
    theorem}\label{thm:compl-slack}
  Let  both  programs $(P)$  and  $(D)$  be  feasible.  Then  feasible
  solutions $\ov \vx$ and $\ov\vy$ of $(P)$ and $(D)$ are both optimal
  if and only if
  \begin{compactenum}
    \item for each $i\in[m]$ one of $s_i$ and $\ov y_i$ is zero, and
    \item for each $j\in[n]$ one of $r_j$ and $\ov x_j$ is zero,
  \end{compactenum}
  or, in a more compact way
  \begin{align*}
    \ov\vy^t\vs&=0&&\text{and}&\vr^t\ov \vx&=0\;.
  \end{align*}
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  The Duality  Theorem states that  $\ov\vx$ and $\ov\vy$  are optimal
  for   their   programs   if   and   only   if   $\vc^t\vx=\vy^t\vb$.
  (\ref{eq:slack}) then implies  that this is the case  if and only if
  $\ov\vy^t\vs+\vr^t\ov\vx=0$.  By  non-negativity of $\ov\vy, \ov\vx,
  \vs, \vr$ this happens if and  only if the conditions in the theorem
  are satisfied.
%
\else
\texttt{missing}
\fi%
\end{proof}%
\fi%
So  if for  some  optimal  solution $\ov\vx$  some  constraint is  not
satisfied with equality, then the corresponding dual variable is zero,
and vice versa.  We can rephrase this in the following useful way. Let
$\ov\vx$ be a feasible solution of
\begin{align*}
  \max(\vc^t\vx\mid A\vx\le \vb,\, \vx\ge \0)\,.
\end{align*}
Then  $\ov\vx$ is optimal if  and only if there is  $\ov\vy$ with
\begin{align*}
  \ov\vy^tA&\ge\vc^t&\ov\vy&\ge \0
\end{align*}
such that
\begin{align}\label{eq:slacksystem}
  \begin{split}
%    \ov x_j> 0&&\Rightarrow&(\ov y^tA){j}=c_j\\
%    A_{i*}\ov x&<b_i&&\Rightarrow&\ov y_i&=0\;.
    \begin{aligned}
      \ov x_j&> 0\\
      (A\ov\vx)_i&<b_i
    \end{aligned}
    \qquad
    \begin{aligned}
      \Longrightarrow\\
      \Longrightarrow
    \end{aligned}
    \qquad
    \begin{aligned}
      \ov\vy^t \va^j&=c_j\\
      \ov y_i&=0\;,
    \end{aligned}
  \end{split}
\end{align}
where $\va^j$  is the  $j$-th column of  $A$.  So given  some feasible
solution $\ov\vx$  we can set up the  system (\ref{eq:slacksystem}) of
linear equations and  solve for $\ov\vy$.  If the  solution exists and
is unique,  then $\ov\vx$  and $\ov\vy$ are  optimal solutions  of the
primal  and  dual program.   We  will see  later  that  a solution  to
(\ref{eq:slacksystem}) is always unique  if $\ov\vx$ is a \emph{basic}
feasible solution.
\begin{example}
  \begin{compactenum}
  \item  We consider  the polytope  $P:=\{\vx\mid A\vx\le  \vb, \vx\ge
    \0\}=\conv(V)$ with
%%%%%%%%%%%%% Margin figure
\marginnote{
  \includegraphics[width=.3\textwidth]{slack-example}
      \begin{minipage}{\marginparwidth}
\captionof{figure}{\label{fig:slack-example}}
 \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{align*}
    A&:=\left({\scriptsize
    \begin{array}{rrr}
      1&1&1\\-2&1&2
    \end{array}}\right)&
  \vb&:=\left({\scriptsize
    \begin{array}{r}
      5\\2
    \end{array}}\right)&
    V&:=\left({\scriptsize
      \begin{array}{rrrrrr}
        0&0&0&1&2&5\\0&0&2&4&0&0\\0&1&0&0&3&0
      \end{array}}\right)\,.
  \end{align*}
  The  polytope  is shown  in  Figure~\ref{fig:slack-example}.  If  we
  choose  $\vc^t:=(1,-1,-2)$  as  objective  function, then  the  last
  column of  $V$ is the optimal  solution $\ov\vx$.  This  is the blue
  vertex  in the figure.  The corresponding  dual optimal  solution is
  $\ov\vy=(1,0)$. We compute the slack vectors
  \begin{align*}
    \vs&:=\vb-A\ov\vx = \left({\scriptsize
      \begin{array}{r}
        0\\12
      \end{array}}\right)&
    \vr^t:=\ov\vy^tA-\vc^t=\left({\scriptsize
      \begin{array}{rrr}
        0&2&3
      \end{array}}\right)\,.
  \end{align*}
  Then
  \begin{align*}
    \vy^t\vs+\vr^t\vx =
    \left({\scriptsize\begin{array}{rr}
      1&0
    \end{array}}\right)
    \left({\scriptsize\begin{array}{r}
      0\\12
    \end{array}}\right)+
    \left({\scriptsize\begin{array}{rrr}
      0&2&3
    \end{array}}\right)
    \left({\scriptsize\begin{array}{r}
      5\\0\\0
    \end{array}}\right) = 0\,.
  \end{align*}
\item We  consider the linear program  $\max(\vc^t\vx\mid A\vx\le \vb,
  \vx\ge \0)$ with
  \begin{align*}
    A&:=\left({\scriptsize
    \begin{array}{rrrrr}
      1&1&1&1&1\\-2&1&1&-2&-3\\-1&2&1&0&-1
    \end{array}}\right)&
  \vb&:=\left({\scriptsize
    \begin{array}{r}
      5\\2\\4
    \end{array}}\right)\\
  \vc^t&:=\left({\scriptsize
      \begin{array}{rrrrr}
        -1&3&1&1&-1
      \end{array}}\right)\,.
  \end{align*}
  We are  given the following potential optimal  solution $\ov\vx$ and
  compute  the   primal  slack  vector   $\vs:=\vb-A\ov\vx$  for  this
  solution:
  \begin{align*}
    \ov\vx&:=\left({\scriptsize
      \begin{array}{r}
        0\\2\\0\\3\\0
      \end{array}}\right)&
    \vs&:=\left({\scriptsize
      \begin{array}{r}
       0\\6\\0
      \end{array}}\right)\,.
  \end{align*}
  We want  to use  (\ref{eq:slacksystem}) to check  that $\ov  \vx$ is
  indeed optimal. The system consists of three linear equations
  \begin{align*}
    \left({\scriptsize
      \begin{array}{rrr}
        \ov y_1& \ov y_2&\ov y_3
      \end{array}}\right)
    \left({\scriptsize
      \begin{array}{r}
        1\\1\\2
      \end{array}}\right) &= 3\,,&
    \left({\scriptsize
      \begin{array}{rrr}
        \ov y_1& \ov y_2&\ov y_3
      \end{array}}\right)
    \left({\scriptsize
      \begin{array}{r}
        1\\-2\\0
      \end{array}}\right) &= 1\,,&
  \ov y_2 = 0\,.
  \end{align*}
  This system  has the unique  solution $\ov\vy^t=(1,0,1)$. This  is a
  feasible  solution  of  $\min(\vy^t\vb\mid \vy^tA\ge  \vc^t,  \vy\ge
  \0)$, and $\vc^t\ov\vx=9=\ov\vy^t\vb$, so both $\ov\vx$ and $\ov\vy$
  are optimal for their programs.
  \end{compactenum}
\end{example}

We want to discuss a geometrical interpretation of the duality theorem
and complementary slackness. Let
\begin{align*}
  \max(\vc^t\vx&\mid A\vx\le \vb)\tag{P}\\
  \min(\vy^t\vb&\mid \vy^tA= \vc^t,\, \vy\ge 0)\tag{D}
\end{align*}
be  a  pair  of   dual  programs  for  $A\in\R^{m\times  n}$,  $\vc\in
(\R^n)^*$,  $\vb\in\R^m$.   The  inequalities  of the  primal  program
define a  polyhedron $P:=\{\vx\mid  A\vx\le \vb\}$.  Let  $\ov\vx$ and
$\ov\vy$  be  optimal  solutions  of  the  primal  and  dual  program.
Complementary slackness tells us that
\begin{align}
  \ov\vy^t(\vb-A\ov\vx)=\0\,.\tag{$\star$}\label{eq:slackgeom}
\end{align}
Hence,  $\ov\vy$   is  non-zero  only  at  those   entries,  at  which
$A\ov\vx\le \vb$ is  tight.  Let $B$ be the set of  row indices of $A$
at which  $\va_j\ov\vx=b_j$. Let $A_B$  the $(|B|\times n)$-sub-matrix
of  $A$ spanned  by these  rows, and  $\ov\vy_B$ be  the corresponding
selection of  entries of  $\ov\vy$. Note that  by (\ref{eq:slackgeom})
this  contains all  non-zero  entries of  $\ov\vy$.  The dual  program
states that  $\vc^t$ is contained in  the cone spanned by  the rows of
$A_B$, and the  dual solution $\ov\vy$ gives a  set of coefficients to
represent $\vc^t$ in this set of generators.
\begin{example}
%%%%%%%%%%%%% Margin figure
\marginnote{
  \psfrag{c}{$\vc^t$}
  \psfrag{cx}{$\vc^t\vx=\text{const}$}
  \psfrag{a1}{$\va_1^t$}
  \psfrag{a2}{$\va_2^t$}
  \includegraphics[width=.3\textwidth]{slackgeom}
      \begin{minipage}{\marginparwidth}
\captionof{figure}{\label{fig:slackgeom}}
 \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Let $A$, $\vb$, and $\vc$ be given by
  \begin{align*}
    A&:=\left(
      \begin{array}{rr}
        -1&1\\ 1&2\\ -2&-1\\ 1&-2\\ 1&0
      \end{array}\right)&
    \vb&:=\left(
      \begin{array}{r}
        3\\3\\3\\1\\1
      \end{array}\right)&
    \vc^t&:=\left(
      \begin{array}{rr}
        0&1
      \end{array}\right)
  \end{align*}
  Then
  \begin{align*}
    \ov\vx&:=\left(
      \begin{array}{r}
        -1\\2
      \end{array}\right)&
    \ov\vy&:=\left(
      \begin{array}{rrrrr}
        \nicefrac13& \nicefrac13& 0&0&0
      \end{array}\right)
  \end{align*}
  are primal  and dual solution. The  primal solution is  tight on the
  first  two inequalities  $-x_1+x_2\le  2$ and  $x_1+2x_2\le 4$.  The
  corresponding functionals satisfy
  \begin{align*}
    \nicefrac13\left(
      \begin{array}{rr}
        -1&1
      \end{array}\right)\;+\;
    \nicefrac13\left(
      \begin{array}{rr}
        1&2
      \end{array}\right)\;=\;
    \left(\begin{array}{rr}
        0&1
      \end{array}\right)
  \end{align*}
  See  Figure~\ref{fig:slackgeom}  for  an illustration.   
\end{example}
%%%%%%%%%%%%% Margin figure
\marginnote{
  \psfrag{c}{$\vc^t$}
  \psfrag{cx}{$\vc^t\vx=\text{const}$}
  \psfrag{a1}{$\va_1^t$}
  \psfrag{a2}{$\va_2^t$}
  \includegraphics[width=.3\textwidth]{slackgeomvar}
      \begin{minipage}{\marginparwidth}
\captionof{figure}{\label{fig:slackgeomvar}}
 \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Using this  geometric interpretation we  can discuss the  influence of
small changes of $\vb$ to the  optimal value of the linear program. We
assume  for this  that $m\ge  n$, $|B|=n$  and  $\rank(A_B)=n$. Hence,
$A_B$  is  an   invertible  matrix  and  $\ov\vx=A_B^{-1}\vb_B$.   Let
$\Delta\in\R^m$ be the change in  $\vb$.  If $\Delta$ is small enough,
then the optimal solution of  $A\vx\le \vb+\Delta$ will still be tight
at  the  inequalities  in $B$.   So  $\ov\vx'=A_B^{-1}(\vb+\Delta)_B$.
However, using the duality  theorem and complementary slackness we can
compute  the  new optimal  value  without  computing  the new  optimal
solution $\ov\vx'$. We have  seen above that the non-zero coefficients
of  the dual  solution are  the  coefficients of  a representation  of
$\vc^t$ in the rows of $A_B$. By our assumption, $A_B$ stays the same,
so  the  program with  modified  right hand  side  has  the same  dual
solution $\ov\vy^t$.  By the duality  theorem the new optimal value is
$\ov\vy^t(\vb+\Delta)$. Further, changing  right hand sides $b_i$ that
correspond to inequalities that are not tight for our optimal solution
do not  affect the  optimal value (again,  as long  as the set  $B$ of
tight inequalities stays  the same). See Figure~\ref{fig:slackgeomvar}
for an example.

The crucial  problem in these considerations  is that we  don't have a
good criterion  to decide whether  a change $\Delta$ to  $\vb$ changes
the set  $B$ or not.  In Section~\ref{cha:simplex-method}  we will see
that we can nevertheless efficiently exploit part this idea to compute
optimal values  of linear programs  with variations in the  right hand
side.






% We want to  show that we can  assume that $B$ contains a  basis of the
% dual space  $(\R^n)^*$.  If  this is  not true, then  we can  choose a
% linear functional $\vn\in\R^n$ on $(\R^n)^*$ that vanishes on the span
% of  $B$.   We can  assume  that $\vc^t\vn\ge  0$.   Then  we can  find
% $\lambda>0$ such  that $A(\ov\vx+\lambda\vn)\le \vb$, and  there is at
% least    one   other   functional    $\va_l\not\in   B$    such   that
% $\va_l^t(\ov\vx+\lambda\vn)=b_l$.   As  $\rank(A)=n$  we can  continue
% until $B$ contains a basis.

% Choose a basis  in $B$ and let  $A_B$ be the $n\times n$  of full rank
% whose  rows are  in the  chosen basis,  and $\vb_B$  the corresponding
% irght hand sides. Then
% \begin{align*}
%   \ov\vx=A_B^{-1}\vb_B.
% \end{align*}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "geometry_and_optimization"
%%% End: 
