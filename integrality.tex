
\chapter{Integer Polyhedra}

For   the  rest   of  the   course  we   restrict   to  \emph{rational
  polyhedra}\mdef{rational  polyhedra},  that  is,  we  only  look  at
polyhedra $P=\{\vx\mid  A\vx\le \vb\}$ such  that $A\in\Q^{m\times n}$
and $\vb\in\Q^m$.  By scaling inequalities with an appropriate factor,
we may even assume that $A\in\Z^{m\times n}$ and $\vb\in\Z^m$.

All vertices of $P$ and all  generators of $\reccone\, P$ are given as
the solution  $\vz$ of a  subsystem $A_{I*}\vz=\vb_I$ of  $A\vx\le b$.
From  Cramer's rule  we may  deduce that  any solution  of  a rational
system     of     equations     is     rational.      Hence,     using
Theorem~\ref{thm:cpleteinnerrep}, we  see that  $P$ can be  written as
$P=\conv(X)+\cone(Y)$   for  rational  matrices   $X$  and   $Y$,  and
$\cone(Y)=\reccone(P)$.
\begin{remark}\label{rem:cones_are_integral}
  We can scale a  generator  of a cone  $C$  by an arbitrary  positive
  factor without  changing $C$.  Hence,  we  can even  assume that all
  generators of $\reccone(P)$ are integral,  i.e.\ $Y$ is an  integral
  matrix.

  Using       methods     similar    to       those     presented   in
  Chapter~\ref{cha:compl-sizes-rati}, one  can prove that the absolute
  value of all  entries  of $Y$ is bounded   by the maximum size of  a
  sub-determinant of $A$.
\end{remark}
\begin{definition}
  Let   $A\in\Q^{m\times   n}$,   $\vb\in\Q^m$,   $\vc\in\Q^n$.    The
  \emph{integer   linear   programming  problem}\mdef{integer   linear
    programming  problem (standard  form)} (in  standard form)  is the
  problem to determine
  \begin{align}
    \delta:=\max(\vc^t\vx\mid       A\vx=\vb,\,      \vx\ge      \0,\,
    \vx\in\Z^n)\,.\tag{IP}\label{eq:ILP}
  \end{align}
\end{definition}
The weak duality estimate
\begin{align*}
  \max(\vc^t\vx\mid  A\vx =  \vb,\,  \vx\ge \0,\,  \vx\in\Z^n)\ \le  \
  \min(\vy^t\vb\mid \vy^tA\ge \vc^t,\, \vy\in\Z^m)\,.
\end{align*}
remains valid for integer linear  programs. However, we usually do not
have  equality in this  relation, i.e.\  the (strong)  duality theorem
fails.  The \emph{LP-relaxation}\mdef{LP-relaxation} of (\ref{eq:ILP})
is the linear program
\begin{align*}
  \delta_{lp}:=\max(\vc^t\vx\mid           A\vx=\vb,\,          \vx\ge
  \0)\,.\tag{LP}\label{eq:lprelax}
\end{align*}
The     optimal    solution     of    \eqref{eq:lprelax}     is    the
\emph{LP-optimum}\mdef{LP-optimum}    of   \eqref{eq:ILP}.    Clearly,
$\delta_{lp}$ is  an upper bound for  $\delta$, but this  bound may be
quite far away from the true solution.
%%%%%%%%%%%%% Margin figure
\marginnote{\mbox{ }\\
  \centering\includegraphics[width=.25\textwidth]{ip-vs-lp-flipped}\\[.7\baselineskip]
  \begin{minipage}{.27\textwidth}
    \captionof{figure}{\label{fig:ip-vs-lp}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{example}
  \begin{compactenum}
  \item Consider  the linear program  $\max(x\mid 2x\le 1,\;  x\ge 0\;
    x\in \Z)$ with dual $\min(y\mid  2y\ge 1\; y\ge0\; y\in \Z)$.  The
    primal optimum is $0$, while the dual optimum is $1$.  Without the
    integrality constraint,  both programs are  feasible with solution
    $x=y=\nicefrac 12$ and optimal value $\nicefrac12$.
  \item Let
    \begin{align*}
      A&:=\left(
        \begin{array}{rr}
          1&1\\
          -n+2&1\\
          n&1
        \end{array}\right)&
      \vb&:=\left(
      \begin{array}{r}
        1\\1\\n
      \end{array}\right)&
    \vc^t&:=\left(
      \begin{array}{rrr}
        0&-1
      \end{array}\right)
    \end{align*}
    The   integer  optimum   is   $(0,1)$,  but   the  LP-optimum   is
    $(\nicefrac12,-\nicefrac n2)$. See Figure~\ref{fig:ip-vs-lp}.
  \end{compactenum}
\end{example}
The second  example suggests that,  if the two solutions  differ, then
the matrix $A$ must have large entries.  This is in fact true, one can
give  bounds on  the difference  between an  integer solution  and the
solution of the  corresponding relaxation in terms of  the size of $A$
and $\vb$, see e.g.\ Schrijver's book~\cite{schrijver_tlip}.
\begin{remark}
  Note that  for integer  linear programs we  also have to  be careful
  with  transformations  between   different  forms  of  the  program.
  Consider  $\max(x\mid  -x\le  -  \nicefrac12,\;  \le  \nicefrac32,\;
  x\ge0,\; x\in  \Z)$. This  linear program in  canonical form  as the
  optimal solution $x=1$. However, if we introduce slack variables $y$
  and $z$ to write
  \begin{align*}
    -x+y&=-\nicefrac12 & x+z&=\nicefrac32 & x,y,z&\ge0
  \end{align*}
  then we cannot  require $y,z$ to be integral  (as the solution $x=1$
  corresponds  to $y,z=\nicefrac12$),  so the  new program  is  not an
  integral  linear program. We  discuss transformations  that preserve
  integer points later.
\end{remark}
\begin{definition}
  The \emph{integer hull}\mdef{integer hull} of $P$ is
  \begin{align*}
    \inth P:=\conv(P\cap \Z^n)\,.
  \end{align*}
  A     rational     polyhedron     $P$    is     an     \emph{integer
    polyhedron}\mdef{integer  polyhedron}  if  $P=\inth  P$.   If  all
  vertices of  an integer  polytope are in  $\{0,1\}^n$, then it  is a
  \emph{$0/1$-polytope}\mdef{$0/1$-polytope}.                       See
  Figure~\ref{fig:inthull} for an example.
\end{definition}
We need some notation for the next theorem.  Let $\alpha\in \R$.  Then
the     \emph{integer    part}\mdef{integer    part\\floor\\fractional
  part\\ceiling} (or  \emph{floor}) $\lfloor\alpha\rfloor$ of $\alpha$
is   the   largest   integer   $a$   smaller   than   $\alpha$.    The
\emph{fractional         part}          of         $\alpha$         is
$\{\alpha\}:=\alpha-\lfloor\alpha\rfloor$.      The     \emph{ceiling}
$\lceil\alpha\rceil$ of  $\alpha$ is the smallest  integer larger than
$\alpha$.
\begin{theorem}\label{thm:integer_hull}
  Let $P$ be  a rational polyhedron.  Then $\inth  P$ is a polyhedron.
  If $\inth P\ne \varnothing$, then $\reccone(P)=\reccone(\inth P)$.
\end{theorem}
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{int_hull}\hfill
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:inthull}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  If $P$ is a polytope, then $P\cap \Z^n$ is finite, so $\inth P$ is a
  polytope.    If    $P$   is   a   cone,   then    $P=\inth   P$   by
  Remark~\ref{rem:cones_are_integral}.

  So assume  $P=Q+C$ for a polytope  $Q$ and a cone  $C$.  Let $\vy_1,
  \ldots, \vy_s\in \Z^m$ be generators of $C$ and define
    \begin{align*}
      \Pi:=\left\{\sum_{i=1}^s\lambda_i  \vy_i\bigm| 0\le \lambda_i\le
        1,\, 1\le i\le s\right\}.
    \end{align*}
    $\Pi$   is  a   polytope  (a   parallelepiped),  so   $Q+\Pi$  and
    $\inth{(Q+\Pi)}$ are polytopes.  We will show that
    \begin{align}
      \inth P = \inth{(Q+\Pi)}+C\,.\label{eq:thm:proof:inthull}
    \end{align}
    ``$\subseteq$'':\;  Let  $\vx\in  P\cap  \Z^n$.   Then  there  are
    $\vq\in Q$ and $\vy\in C$ such that
    \begin{align*}
      \vx&=\vq+\vy\,,&&\text{and}                                     &
      \vy&=\textstyle\sum_{i=1}^s\lambda_i\vy_i
    \end{align*}
    for some $0\le  \lambda_i\le 1$, $1\le i\le s$.   We can split the
    coefficients  $\lambda_i\{\lambda_i\}+\lfloor \lambda\rfloor$ into
    their fractional and integral part. We define
    \begin{align*}
      \vz&:=\textstyle\sum_{i=1}^s\{\lambda_i\}\vy_i\in
      \Pi&&\text{and}&
      \vy'&:=\textstyle\sum_{i=1}^s\lfloor\lambda_i\rfloor     \vy_i\in
      C\cap \Z^n\,,
    \end{align*}
    so $\vy= \vy'+\vz$.  Hence, we can write $\vx$ in the form
    \begin{align*}
      \vx&=\vq+\vy=\vq+\vy'+\vz&&\qquad\Longrightarrow\qquad& \vx-\vy'
      &= \vq+\vz\,.
    \end{align*}
    The  right hand  side  $\vx-\vy'$  of the  second  equation is  in
    $\Z^n$. So  also the vector  on the left  side of the  equation is
    integral.  This implies $\vq+\vz\in\inth{(Q+\Pi)}$.  As $\vy'\in C$,
    we obtain
    \begin{align*}
      \vx=(\vq+\vz)+\vy'\in \inth{(Q+\Pi)} + C\,.
    \end{align*}

    ``$\supseteq$'':\; This follows directly from the following chain:
    \begin{align*}
      \inth{(Q+\Pi)}+C\subseteq  \inth P +C  = \inth  P+\inth C\subseteq
      \inth{(P+C)}=\inth P\,.
    \end{align*}

    Finally, if  $\inth P\ne \varnothing$, then the  recession cone of
    $\inth   P$  is   uniquely   defined  by   the  decomposition   in
    (\ref{eq:thm:proof:inthull}).          Hence,        $\reccone(P)=
    C=\reccone(\inth P)$.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
So in  principle, an integer linear  program is just  a linear program
over the integer hull of  a polyhedron.  However, to use this approach
we  need an exterior  description of  $\inth P$.   This is  in general
difficult  (non-polynomial) to  compute.  It  is slightly  easier (but
still  not polynomial)  to decide  whether $\inth  P=\varnothing$.  We
will see  a method to compute  the integer hull  of arbitrary rational
polytopes in Chapter~\ref{ch:chvatal}.

A  central problem for  integer polyhedra  is the  characterization of
those  rational   polyhedra  that  satisfy  $P=\inth   P$.   The  next
proposition  gives  several  important  criteria  for  this.   In  the
Chapters~\ref{ch:unimodularity}  and  \ref{ch:tdi}   we  will  give  a
characterization of two important  families of polyhedra with integral
vertices.
\begin{proposition}\label{prop:charact-of-unimod}
  Let $P$ be a rational polyhedron. Then the following are equivalent.
  \begin{compactenum}
    \item\label{item:lem:intpoly1} $P=\inth P$,
    \item\label{item:lem:intpoly2}  each nonempty face of $P$ contains
      an integral point,
    \item\label{item:lem:intpoly3}  each minimal face  of $P$ contains
      an integral point,
    \item\label{item:lem:intpoly4}       the       linear      program
      $\max(\vc^t\vx\mid \vx\in  P)$ has an  integral optimal solution
      for each $\vc\in\R^n$ for which the maximum is finite.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
\ifreleasedproof%
  \mbox{ }
  \begin{compactitem}
  \item[$(\ref{item:lem:intpoly1})\Rightarrow(\ref{item:lem:intpoly2})$]
    Let $\vc\in \R^n$ and $\delta\in\R$  define a nonempty face $F$ of
    $P$, i.e.
    \begin{align*}
      F=\{\vx\mid \vc^t\vx=\delta\}\cap P\ne \varnothing\,.
    \end{align*}
    Let $\vx\in F$.  As $P=\inth P$,  $\vx$ can be written as a convex
    combination of integer vectors
    \begin{align*}
      \vx&=\textstyle\sum_{i=1}^k\lambda_i\vx_i&&\text{ for  $\vx_i \in
        P\cap\Z^n$,     $\lambda_i\ge    0$,     $1\le     i\le    k$,
        $\textstyle\sum_{i=1}^s\lambda_i=1$.}
    \end{align*}
    \begin{flalign*}
      &\text{Then}&\delta &=
      \vc^t\vx=\vc^t\textstyle\sum_{i=1}^k\lambda_i \vx_i =
      \textstyle\sum_{i=1}^k\lambda_i \vc^t\vx_i\,.&&
    \end{flalign*}
    Hence,  $\vc^t\vx_i\le  \delta$  for  all $1\le  i\le  k$  implies
    $\vc^t\vx_i=\delta$, so $\vx_i\in F$ for $1\le i\le k$.
  \item[$(\ref{item:lem:intpoly2})\Rightarrow(\ref{item:lem:intpoly3})$]
    This is just a specialization of the assumption.
  \item[$(\ref{item:lem:intpoly3})\Rightarrow(\ref{item:lem:intpoly4})$]
    If  $\max(\vc^t\vx\mid  \vx\in P)$  is  finite,  then  the set  of
    optimal  solutions defines a  face $F$  of $P$.   Any face  of $P$
    contains a  minimal face  of $P$, so  also $F$ contains  a minimal
    face $G$ of $P$. $G$ contains an integral point by assumption.
  \item[$(\ref{item:lem:intpoly4})\Rightarrow(\ref{item:lem:intpoly3})$]
    Let  $H:=\{\vx\mid \vc^t\vx=\delta\}$ be  a valid  hyperplane that
    defines a minimal face  $F$ of $P$. Then $\delta=\max(\vc^t\vx\mid
    \vx\in P)$, so there is some integral vector in $F$ by assumption.
  \item[$(\ref{item:lem:intpoly3})\Rightarrow(\ref{item:lem:intpoly1})$]
    Let $F_1, \ldots, F_k$ be the minimal faces of $P$.  We can choose
    an integral  point $\vx_i$ in each.   Let $Q:=\conv(\vx_1, \ldots,
    \vx_k)$  and $C:=\reccone(P)$  the  recession cone  of $P$.   Then
    $Q=\inth Q$  and $C=\inth C$.   By our representation  theorem for
    polyhedra,  Theorem  \ref{thm:cpleteinnerrep},  we  have  $P=Q+C$.
    This implies
    \begin{align*}
      P=Q+C=\inth Q+\inth C\subseteq  \inth{(Q+C)} = \inth  P\subseteq
      P\,.
    \end{align*}
    so that $P=\inth P$.
  \end{compactitem}
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
In particular,  if $P$  is pointed,  then $P=\inth P$  if and  only if
every vertex of $P$ is integral.

\begin{corollary}
  If $P$ is an integer polyhedron, then the integer linear programming
  problem over $P$ can be solved with the simplex algorithm.\mqed
\end{corollary}

We  have seen  in Proposition~\ref{prop:affine_minkowski}  that affine
images  of  polyhedra  are  polyhedra,  and  any  function  that  maps
polyhedra to polyhedra  must be affine.  Clearly the  same is true for
rational  polyhedra  and  rational  affine  maps  (i.e.\  affine  maps
$\vx\mapsto M\vx+\vb$ for a rational matrix $M\in\Q^{m\times n}$ and a
rational  vector  $\vb\in\Q^n$).  In  the  previous  chapters we  have
sometimes used affine transformations  to obtain a nice representation
of a polyhedron. For example, up to an affine map we can assume that a
polyhedron is  full-dimensional.  Implicitly,  we also perform  such a
transformation   when  we   solve   systems  using   \textsc{Gauss}ian
elimination.   All row operations  preserve the  space spanned  by the
rows of the matrix, which ensures  us that the solution we find in the
affine space spanned by the matrix in row echelon form is also a point
in the original space.
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.2\textwidth]{unimod-transform}
  \begin{minipage}{.25\linewidth}
  \captionof{figure}{\label{fig:unimod-transform}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Further,  if  we are  given  rational  polyhedron $P\subseteq\R^n$,  a
linear   functional  $\vc^t\in\R^n$,   and  a   rational   affine  map
$\phi:\R^n\rightarrow\R^n$,    then   the    optimal    solutions   of
$\max((\phi^*\vc)^t\vx\mid \vx\in  \phi(P))$ are the  affine images of
the  optimal   solutions  of  $\max(\vc^t\vx\mid   \vx\in  P)$.   Both
properties do not hold anymore if we restrict to integer polyhedra and
integral optimal solutions to integer linear programs. In general, the
affine image of an integer polyhedron is not integral anymore (shift a
segment  with integral end  points by  some non-integral  amount). The
number of  points in $|P\cap \Z^n|$  for a rational  polyhedron $P$ is
not invariant under affine maps  (Think e.g.\ of scaling a polyhedron.
For any  polytope $P$  there is some  $\eps>0$ such that  $|\eps P\cap
\Z^n|\le  1$.).  Hence,  finding  an integral  solution  in an  affine
transform  of  a  polyhedron  doesn't  tell  us  much  about  integral
solutions in the original polyhedron.

In the following we want  to determine the set of transformations that
preserve  integrality and the  information about  integer points  in a
polyhedron, i.e.\  transformations that preserve the  set $\Z^d$. Such
transformations     are    called    \emph{unimodular}\mdef{unimodular
  transformation}.   Clearly,   this  is   a  subset  of   the  affine
transformations, so we  can write such a map  as $\vx\mapsto U\vx+\vb$
for some $U\in\Q^{m\times  n}$ and $\vb\in \Q^n$.  A  translation by a
vector $\vb$ preserves  $\Z^n$ if and only if  $\vb\in\Z^n$. Hence, we
have   to   characterize   linear   transformations  $U$   such   that
$U\Z^n=\Z^n$. This is given by the following proposition.
\begin{proposition}\label{prop:integral-sols}
  Let $A\in\Z^{m\times  m}$. Then $\det(A)=\pm  1$ if and only  if the
  solution of $A\vx=\vb$ is integer for any $\vb\in\Z^m$.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  ``$\Rightarrow$'':  By  Cramer's  rule,  the entries  of  $\vx$  are
  $x_i=\pm\det(A_i)$, where  $A_i$ is the matrix obtained  from $A$ by
  replacing the $i$-th column with $\vb$.

  ``$\Leftarrow$'':  If  $|\det A|>1$,  then  $0<|\det A^{-1}|<1$,  so
  $A^{-1}$ contains  a non-integer entry  $a_{ij}$.  If $\ve_j\in\Z^m$
  is  the  $j$-th  unit  vector,  then  $A\vx=\ve_j$  has  no  integer
  solution.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{definition}
  A    transformation      $U\in\Z^{m\times             m}$         is
  \emph{unimodular}\mdef{unimodular matrix} if $|\det U|=1$.

  Two    polyhedra     $P$    and    $Q$     are    \emph{unimodularly
    equivalent}\mdef{unimodularly equivalent} if there is a unimodular
  transformation $\phi$ that maps $P$ to $Q$.
\end{definition}
Obviously  the inverse  $U^{-1}$ and  the product  $UV$  of unimodular
transformation     $U$    and     $V$     are    again     unimodular.
Figure~\ref{fig:unimod-transform}   shows   two   $4$-gons  that   are
unimodularly    equivalent   via    the    unimodular   transformation
$\left(\begin{smallmatrix} 1&0\\2&1 \end{smallmatrix}\right)$.
\begin{remark}
  Unimodular  transformations are  more general  those transformations
  that are  \emph{lattice preserving}\mdef{lattice\\lattice preserving
    transformations}.   A  \emph{lattice}   $\Lambda$  is  a  discrete
  subgroup of $\R^n$. It can always be written in the form
  \begin{align*}
    \Lambda=\Z \vv_1+\Z \vv_2+\cdots+\Z \vv_k
  \end{align*}
  for a finite number  of linearly independent vectors $\vv_1, \ldots,
  \vv_k\in\R^n$.   A   non-singular  transformation  $T$   is  lattice
  preserving if  $T(\Lambda)=\Lambda$.  $\Z^n$ is  the special lattice
  spanned by the standard unit vectors.  In the same way as for $\Z^n$
  one  could also  consider  integer polyhedra  w.r.t.\  to any  other
  lattice.
\end{remark}

The equivalent of the row echelon  form of linear algebra will then be
the \textsc{Hermite} normal form, and  we will see in the next theorem
that any  rational matrix  can be transformed  to such a  matrix using
certain unimodular column operations.
\begin{definition}
  A matrix $A=(a_{ij})_{ij}\in\R^{m\times  n}$ with $\rank(A)=m$ is in
  \emph{\textsc{Hermite}   normal  form}\mdef{\textsc{Hermite}  normal
    form}, if the following conditions are satisfied:
  \begin{compactenum}
  \item $A_{*J}=0$ for $J=[n]\setminus[m]$.
  \item   $A_{*[m]}$     is  non-singular,     lower-triangular,   and
    non-negative.
  \item $a_{ii} > a_{ij}$ for $j<i$.
  \end{compactenum}
\end{definition}
\begin{example}
  The first matrix  is in \textsc{Hermite} normal form,  the other two
  are not.
  
  \begin{align*} \left[\begin{array}{rrrr}
        5 &0&0&0\\
        3 &4&0&0\\
        2 &1&4&0\\
    \end{array}\right]&&
    \left[\begin{array}{rrrr}
      5 &0&0\\
      3 &4&0\\
      2 &7&4\\
    \end{array}\right]&&
    \left[\begin{array}{rrrr}
      5 &0&0&0\\
      3 &4&0&0\\
      6 &1&7&4\\
    \end{array}\right]\\[-\baselineskip]\mbox{ }
  \end{align*}
\end{example}

\smallskip

Similarly to the column operations used for \textsc{Gauss} elimination
we  consider the  following three  \emph{elementary  unimodular column
  operations}\mdef{elementary   unimodular   column   operations}   to
transform a matrix into \textsc{Hermite} normal form:
\begin{compactenum}
\item\label{item:unimod1} exchange two columns  of $A$,
\item\label{item:unimod2} multiply a column by $-1$,
\item\label{item:unimod3} add an  integral   multiple of a  column  to
  another column.
\end{compactenum}

Similarly    we   can    define   \emph{elementary    unimodular   row
  operations}\mdef{elementary unimodular  row operations}.  Elementary
unimodular  column operations  on  a  matrix $A$  are  given by  right
multiplication  of $A$ with  a unimodular  transformation.  Similarly,
row operations are given by left multiplication.

\begin{theorem}\label{thm:hermite-form}
  Let  $A\in\Q^{m\times  n}$.   Then  there  is  a  unimodular  matrix
  $U\in\Z^{m\times m}$  such that  $AU$ is in  \textsc{Hermite} normal
  form.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  W.l.o.g.\  we may assume  that $A$  is integral  (scale back  in the
  end).   We use elementary  column operations  to transform  $A$ into
  \textsc{Hermite} normal form.   The unimodular transformation matrix
  $U$   is  then   given  by   multiplication  of   the  corresponding
  transformation matrices.

  Suppose that we  have, using elementary column  operations, achieved
  the following situation:
  \begin{align*}
    A':=\left[\begin{array}{rr} H&0\\B&C
      \end{array}\right]
  \end{align*}
  for an  integral upper   triangular matrix  $H$  and some   integral
  matrices $B$ and $C$ (we may start this  process by choosing $C=A$).

  Let $\gamma_1, \ldots, \gamma_k$ be the  entries of the first row of
  $C$. Using (\ref{item:unimod2}) we  may assume that  $\gamma_i\ge 0$
  for all $i$. As $C$ has full row rank,  not all $\gamma_i$ are zero.
  We apply the following two steps to the first row of $C$:
  \begin{compactenum}
  \item Using (\ref{item:unimod1}), we  can reorder the columns of $C$
    so that $\gamma_1\ge \gamma_2\ge \ldots \ne \gamma_k$.
  \item If  $\gamma_2\ne 0$, then  subtract the second column from the
    first and repeat.
  \end{compactenum}
  This process terminates as in each step the total sum of all entries
  in   the    first   row    of   $C$   strictly    decreases.    Then
  $\gamma_2=\gamma_3=\ldots=\gamma_k=0$  and  the  number of  rows  in
  upper triangular form in $A'$ has increased by one.  Repeating this,
  we obtain
  \begin{align*}
    A'':=\left[\begin{array}{rr} H&0
      \end{array}\right]
  \end{align*}
  with   an upper triangular  integral   matrix $H$, and all  diagonal
  entries of $H$ are positive.  If still
  \begin{align*}
    h_{ij}&<0&&\text{or}& h_{ij}&>h_{ii}&&\text{for some $i>j$}
  \end{align*}
  then we can  add an integral  multiple of the  $i$-th column to  the
  $j$-column   to ensure
  \begin{align*}
    0\le h_{ij}<h_{ii}\,.
  \end{align*}
  If we apply this procedure from top to bottom, then a once corrected
  entry of $H$ is not affected by corrections  of later entries.  This
  finally transforms $A$ into \textsc{Hermite} normal form.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{remark}
  For  a general  lattice $\Lambda$  spanned by  some  vectors $\vv_1,
  \ldots, \vv_k\in\R^n$ it is not  obvious how we can decide whether a
  given vector $\vv$  is in the lattice or  not. 

  The \textsc{Hermite} normal form solves  this problem. If $A$ is the
  matrix  with  columns $\vv_1,  \ldots,  \vv_k$  and  if $B$  is  the
  non-singular matrix obtained  from the \textsc{Hermite} normal form,
  then $\vv$ is in the lattice if and only if $B^{-1}\vv$ is integral.
  This is immediate from the fact that the columns of $A$ and $B$ span
  the same lattice.
\end{remark}
The \textsc{Hermite} normal form of a matrix $A\in\Q^{m\times n}$ with
$\rank(A)=m$  is in fact  unique,   but we  don't  need  this in   the
following.  This is an easy consequence from  the observation that the
lattice  spanned by    the   columns is invariant   under   elementary
operations.
\begin{remark}
  It  follows that  if a  system $Avx=vb$  has at  least  one integral
  solution $\ov  \vx$ then the set  of all integral  solutions forms a
  lattice  in  that hyperplane,  i.e.\  there  are integral  solutions
  $\vx_1, \ldots,  \vx_k$ such  that any integral  solution is  of the
  form
  \begin{align*}
    \ov \vx+\lambda_1\vx_1+\cdots+\lambda_k\vx_k
  \end{align*}
  for $\lambda_1, \ldots, \lambda_k\in \Z$.
\end{remark}
% If both elementary unimodular  row and column operations are applied
% to $A\in\Q^{m\times n}$, then one obtains a matrix
% \begin{align*}
%   \left(
%     \begin{array}{rr}
%       D& 0\\0&0
%     \end{array}\right)
% \end{align*}
% where    $D$  is a diagonal   matrix   with positive  diagonal entries
% $\delta_1, \delta_2, \ldots, \delta_k$ such that
% \begin{align*}
%   \delta_i\,|\,\delta_{i+1}\qquad\text{ for } 1\le i\le k-1\,.
% \end{align*}
% The product $\delta_1\cdot\delta_2\cdots\delta_i$ for $1\le i\le k$ is
% the  greatest common divisor (g.c.d.) of  the sub-determinants  of $A$ of
% order $i$.  This  form of the matrix  $A$ is called \emph{Smith normal
%   form}\mdef{Smith normal form}. It is unique.   The Smith normal form
% is a quite important tool in  integer linear programming, but we don't
% need it in the following.
\begin{theorem}[Integral Alternative Theorem]
  \label{cor:integral_alternative}
  Let $A\vx=\vb$ be a rational system of linear inequalities.

  Then either this  system has an integer solution  $\ov \vx$ or there
  is  a rational  vector  $\vy$  such that  $\vy^tA$  is integral  but
  $\vy^t\vb$ is not.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  If $\ov\vx$  is an integral  solution of $A\vx=\vb$ and  $\vy^tA$ is
  integral, then  also $\vy^t\vb=\vy^tA\vx$  is integral.  So  at most
  one of the two possibilities can hold.
  
  Now  assume  that  $\vy^t\vb$   is  integral  whenever  $\vy^tA$  is
  integral.  If  $A\vx=\vb$ had no solution,  then the \textsc{Farkas}
  Lemma  implies  that  there  is  a  vector  $\vz\in\Q^m$  such  that
  $\vz^tA=\0$, but  $\vz^t\vb>0$.  By Scaling $\vz$  with any positive
  factor   does  not   change  this   property,  so   we   may  assume
  $\vz^t\vb=\nicefrac12$.  This is  a contradiction to our assumption.
  Hence $A\vx=\vb$ has  at least one solution, and  we can assume that
  the rows of $A$ are linearly independent.

  Both  statements in the   theorem   are invariant  under  elementary
  unimodular column operations.  So by  Theorem~\ref{thm:hermite-form}
  we can assume  that $A$ has the  form $A=\left[H\ \  0\right]$ for a
  lower triangular matrix $H$.  Now $H^{-1}A=\left[\id\ \ 0\right]$ is
  integral, so $H^{-1}b$ is  integral by our  assumption (apply it  to
  all row vectors of $H^{-1}$ separately). But
  \begin{align*}
    \left[H\ \ 0\right]\left[
      \begin{array}{r}
        H^{-1}b\\0
      \end{array}\right]\ =\ b
  \end{align*}
  So $x=\left[
    \begin{array}{r}
      H^{-1}b\\0
    \end{array}\right]$ is an integral solution.  
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{proposition}
  Let  $A\in\Z^{m\times   n}$ with $\rank(A)=m$.    The  following are
  equivalent:
  \begin{compactenum}
  \item The greatest common divisor  of the sub-determinants of $A$ or
    order $m$ is one.
  \item $A\vx=\vb$  has an integral  solution $\vx$ for  each integral
    vector $\vb$.
  \item  For  each $\vy$,  if  $\vy^tA$  is  integral, then  $\vy$  is
    integral.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
 \ifreleasedproof%
  All three  claims are invariant  under unimodular column operations.
  Hence, we  may assume  that $A$ is  in \textsc{Hermite}  normal form
  $A=\left(B\ 0\right)$. But then, all three  claims are equivalent to
  $B$ being a unit matrix.
% 
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{proposition}\label{thm:ratsupphyp}
  Let $P=\{\vx\mid  A\vx\le \vb\}$ for  rational $A$ and  $\vb$.  Then
  $P$ is integral  if and only if each  rational supporting hyperplane
  of $P$ contains an integral point.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Assume  first  that  $P$   is  integral.   Any  rational  supporting
  hyperplane  intersects $P$  in a  face $F$  that contains  a minimal
  face.         So         the        claim        follows        from
  Proposition~\ref{prop:charact-of-unimod}.

  Now suppose that every   rational supporting hyperplane  contains an
  integral point. We may assume that $A$ and $b$ are integral. Let $F$
  be a minimal face of $P$ and $I:=\eq(F)$. So
  \begin{align*}
    F&=\{\vx\mid A_{I*}\vx=\vb_I\}\cap P\\
    &= \{\vx\mid  A_{I*}\vx=\vb_I\}\qquad\qquad\text{(by minimality of
      $F$)}\,.
  \end{align*}
  If $F$ does  not contain an integer point,  then there is a rational
  $\vy$ such that
  \begin{align*}
    \vc^t:=\vy^tA_{I*}&\in\Z^n&&\text{but}&
    \delta:=\vy^t\vb_I&\not\in\Z\,,
  \end{align*}
  by         the         integral         alternative         theorem,
  Theorem~\ref{cor:integral_alternative}.      Adding     a     vector
  $\vy'\in\Z^m$ to $\vy$ does not change this property.  Hence, we may
  assume that $\vy>0$. Now let $\vz\in P$.
  \begin{flalign*}
    \text{Then}&&\vc^t\vz\  =\  \vy^tA_{I*}\vz\  \le\  \vy^t\vb_I\  =\
    \delta&&
  \end{flalign*}
  with equality if $\vz\in P$ (here the inequality needs $\vy>0$).  So
  $H:=\{\vz\mid  \vc^t\vz=\delta\}$ is  a supporting  hyperplane.  But
  $\vc\in\Z^n$ and $\delta\not\in\Z$ implies that $H$ does not contain
  an integer point.  This contradicts our assumption.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
A       vector      $\vx=(x_1,      \ldots,       x_n)\in\Z^n$      is
\emph{primitive}\mdef{primitive vector} if $\gcd(x_1, \ldots, x_n)=1$.
Any  integral  vector can  be  transformed  into  a primitive  one  by
dividing each entry with the common g.c.d.\ of all entries.
\begin{corollary}\label{cor:int-opt-val}
  Let $A\in\Q^{m\times  n}$ and  $\vb\in\Q^m$. Then the  following are
  equivalent:
  \begin{compactenum}
  \item There  is an  integral optimal solution  to $\max(\vc^t\vx\mid
    A\vx\le \vb)$  for each $\vc^t\in\Q^n$  for which that  maximum is
    finite.
  \item $\max(\vc^t\vx\mid A\vx\le \vb)$ is integral for each integral
    vector $\vc^t\in\Z^n$ for which the maximum is finite.
  \end{compactenum}
\end{corollary}
\ifproof%
\begin{proof}%
\ifreleasedproof%
\begin{compactitem}
\item[$(1)\Rightarrow(2)$] If the maximum  is attained for an integral
  vector  $\vx$  and $\vc^t$  is  integral,  then  the maximum  is  an
  integer.
\item[$(2)\Rightarrow(1)$]  Let $H:=\{\vx\mid \vc^t\vx=\delta\}$  be a
  rational supporting hyperplane of $P:=\{\vx\mid A\vx\le \vb\}$ for a
  primitive  integral $\vc^t$  (i.e.\ the  g.c.d.\ of  the  entries is
  $1$).  Then  $\delta=\max(\vc^t\vx\mid A\vx\le \vb)$,  so $\delta\in
  \Z$.   By Theorem~\ref{cor:integral_alternative}  we  know that  $H$
  contains an integral point.

  $H$  was arbitrary,  so any  rational supporting  hyperplane  of $P$
  contains   an   integral  point.    Proposition~\ref{thm:ratsupphyp}
  implies that $P$ is  integral, so $\max(\vc^t\vx\mid A\vx\le b)$ has
  an integral optimal solution whenever it is finite.
\end{compactitem}
  % 
\else\texttt{missing}\fi%
\end{proof}%
\fi%
Let $C$  be a  rational cone. Scaling  the generators with  a positive
factor does not change the cone,  so we can assume that all generators
are  integral and  primitive.  Any  integral point  in the  cone  is a
rational  conic combination  of these  generators. However,  it  is in
general not  true that any integral  point in the cone  is an integral
conic combination of the generators. 
\begin{example}
  Let $C\subseteq R^2$ be the cone spanned by $\left(
    \begin{smallmatrix}
      1\\1
    \end{smallmatrix}\right)$ and $\left(
    \begin{smallmatrix}
      -1\\\phantom{-}1
    \end{smallmatrix}\right)$. Then $\left(
    \begin{smallmatrix}
      0\\1
    \end{smallmatrix}\right)$ is in the cone, but cannot be written as
  \begin{align*}
    \left(\begin{smallmatrix}
      0\\1
    \end{smallmatrix}\right)&\ =\ \lambda
    \left(\begin{smallmatrix}
      1\\1
    \end{smallmatrix}\right)\; +\; \mu
    \left(\begin{smallmatrix}
      -1\\\phantom{-}1
    \end{smallmatrix}\right)
  \end{align*}
  for integral $\lambda,\mu\ge 0$.
\end{example}
We  have to  add further  generators if  we want  to obtain  a  set of
vectors that  generate each integer point  in the cone  by an integral
conic combination. 
\begin{definition}
  Let  $\hilb:=\{\vh_1,\ldots, \vh_t\}\subset\Q^n$  be  a finite  set.
  $\hilb$   is   a   \emph{Hilbert   basis}\mdef{Hilbert   basis}   of
  $C:=\cone(\vh_1, \ldots, \vh_t)$ if  every integral vector in $C$ is
  an integral conic combination of $\vh_1, \ldots, \vh_t$.  $\hilb$ is
  an  \emph{integral Hilbert  basis}\mdef{integral Hilbert  basis}, if
  all $\vh_i$ are integral.
\end{definition}
All Hilbert bases that we consider in this course will be integral. So
when we speak  of a Hilbert basis, then we implicitly  mean that it is
integral.
\begin{example}\label{ex:hilbert2d}
  Let $k\in \N$. Consider the cone
  \begin{align*}
    C:=\cone\left(\left(
        \begin{array}{r}
          1\\0
        \end{array}\right), \left(
        \begin{array}{r}
          1\\k
        \end{array}\right)\right)
  \end{align*}
  Then
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.22\textwidth]{hilbert-basis-flipped}
  \begin{minipage}{.28\linewidth}\mbox{  }\\
    \captionof{figure}{\label{fig:hilbert2d}}
\end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{align*}
    \hilb:=\left\{\left(
        \begin{array}{r}
          1\\0
        \end{array}\right), \left(
        \begin{array}{r}
          1\\1
        \end{array}\right), \left(
        \begin{array}{r}
          1\\2
        \end{array}\right), \ldots, \left(
        \begin{array}{r}
          1\\k
        \end{array}\right)\right\}
  \end{align*}
  is a \textsc{Hilbert}  basis of $C$, see Figure~\ref{fig:hilbert2d}.
  This  is  minimal in  the  sense  that no  subset  of  $\hilb$ is  a
  \textsc{Hilbert}  basis and  any other  \textsc{Hilbert}  basis must
  contain  $\hilb$.  Hence,  computation of  a  \textsc{Hilbert} basis
  cannot be polynomial in the size of a generating set of the cone.
\end{example}
Although the previous example  shows that a \textsc{Hilbert} basis can
be  large  compared  to the  set  of  rays  of  a  cone, it  is  still
finite. The following theorem shows that this is always true.
\begin{theorem}\label{thm:Hilbertbasis}
  Let $C\subseteq\R^m$ be a rational cone. Then $C$ is generated by an
  integral \textsc{Hilbert}  basis $\hilb$.   If $C$ is  pointed, then
  there is a unique  minimal \textsc{Hilbert} basis contained in every
  other \textsc{Hilbert} basis of the cone.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Let  $\vy_1,  \ldots,  \vy_k$  be primitive  integral  vectors  that
  generate $C$, and define the parallelepiped 
  \begin{align*}
    \Pi:=\left\{\textstyle\sum_{i=1}^k\lambda_i\vy_i\mid  0\le \lambda_i\le 1,\,
      1\le i\le k\right\}\,.
  \end{align*}
  as   in   the    proof   of   Theorem~\ref{thm:integer_hull}.    Let
  $\hilb:=\Pi\cap  \Z^n$.   Observe   that  $\vy_1,  \ldots,  \vy_k\in
  \hilb$, so  $\hilb$ generates $C$. We  will prove that  $\hilb$ is a
  Hilbert basis of $C$.

  Let $\vx\in C\cap  \Z^n$ be any integral vector  in $C$.  Then there
  are     $\eta_1,     \ldots,      \eta_k\ge     0$     such     that
  $\vx=\textstyle\sum_{i=1}^k\eta_i\vy_i$.  We can rewrite this as
    \begin{align*}
      \vx&=\textstyle\sum_{i=1}^k\left(\lfloor
        \eta_i\rfloor+\{\eta_i\}\right)\vy_i\,,&&
      \text{so that}&
      \vx-\textstyle\sum_{i=1}^k\lfloor        \eta_i\rfloor       \vy_i       &=
      \textstyle\sum_{i=1}^k\{\eta_i\} \vy_i\,.
    \end{align*}
    The left side of this equation is integral.  Hence, also the right
    side is integral. But
    \begin{align*}
      \vh:=\textstyle\sum_{i=1}^k\{\eta_i\} \vy_i\in \Pi\,,
    \end{align*}
    so  $\vh\in  \Pi\cap\Z^n=\hilb$.  This  implies  that  $\vx$ is  a
    integral conic combination of points  in $\hilb$.  So $\hilb$ is a
    Hilbert basis.


    Now assume that $C$ is pointed.  Then there is $\vb^t\in\R^n$ such
    that
  \begin{align*}
    \vb^t\vx&>0&&\text{for all}&\vx&\in C\setminus\{0\}\,.
  \end{align*}
  \begin{flalign*}
    \text{Let}&& K&:=\{\vy\in C\cap \Z^m  \mid \vy\ne 0, \, \vy \text{
      not a sum of two other integral vectors in } C\}\,.&&
  \end{flalign*}
%%%%%%%%%%%%% Margin figure
\marginnote{{\centering\includegraphics[width=.2\textwidth]{hilbert_bases_r2}\bigskip}
  \begin{minipage}{.28\linewidth}
    \captionof{figure}{\label{fig:hb_nonpt}}
\end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Then $K\subseteq \hilb$, so $K$ is finite.  Assume that $K$ is not a
  Hilbert basis.   Then there is  $\vx\in C$ such that  $\vx\not\in \N
  K$.  Choose an  $\vx$ such that $\vb^t\vx$ is  as small as possible.
  Since $\vx\not\in  K$, there must  be $\vx_1, \vx_2\in C$  such that
  $\vx=\vx_1+\vx_2$.  But
  \begin{flalign*}
    &&\vb^t\vx_1&\ge    0\,,&   \vb^t\vx_2&\ge    0,\,&   \vb^t\vx&\ge
    0&&\text{and}&
    \vb^t\vx&=\vb^t\vx_1+\vb^t\vx_2\,,&&\\
    \text{so}&& \vb^t\vx_1&\le \vb^t\vx\,,& \vb^t\vx_2& < \vb^t\vx\,.
  \end{flalign*}
  By  our  choice of  $\vx$  we get  $\vx_1,  \vx_2\in\N  K$, so  that
  $\vx\in\N K$, a contradiction.
  %
  \else\texttt{missing}\fi
%
\end{proof}%
\fi%
\begin{remark}
  \begin{compactenum}
  \item The minimal Hilbert basis of a non-pointed cone is not unique:
    \begin{align*}
      &\{\pm \ve_1, \pm \ve_2\} && &\{\ve_1, \ve_2, -\ve_1-\ve_2\}
    \end{align*}
    are both Hilbert bases of $\R^2$. See Figure~\ref{fig:hb_nonpt}.
  \item Every vector in a minimal Hilbert basis is primitive.
  \end{compactenum}
\end{remark}
\begin{remark}
  Combining Theorems~\ref{thm:integer_hull} and \ref{thm:Hilbertbasis}
  we obtain that for  any rational polyhedron  $P$ there  are integral
  vectors $\vx_1, \ldots, \vx_t$ and $\vy_1,\ldots, \vy_s$ such that
  \begin{align*}
    P\cap   \Z^n   =  \biggl\{\textstyle\sum_{i=1}^t\lambda_i\vx_i   +
    \textstyle\sum_{j=1}^s\mu_j\vy_j             \             \Bigm|\
    \textstyle\sum_{i=1}^t\lambda_i=1,\, \lambda_i, \mu_j\in\Z\;\text{
      for }\, 1\le i\le t,\, 1\le j\le s \biggr\}
  \end{align*}
  This   gives    a parametric    solution   for  linear   Diophantine
  inequalities.
\end{remark}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "geometry_and_optimization"
%%% End: 
