
\chapter{Total Dual Integrality}\label{ch:tdi}

In the last two chapters we found a condition on the constraint matrix
$A$  that ensured the  polyhedron $P:=\{\vx\mid  A\vx\le \vb\}$  to be
integral for any  integral right hand side $\vb$.   Matrices with this
property are  exactly the totally  unimodular matrices.  We  have seen
that   already  this   quite  special   class  has   many  interesting
applications.

In this chapter we want to study a more general relation. This time we
want to consider  which combinations of a constraint  matrix $A$ and a
right  hand  side  vector  $\vb$  lead  to  integer  polyhedra.   This
naturally  leads to  the notion  of totally  dual integral  systems of
linear inequalities.   We will see that  this completely characterizes
integrality of a polyhedron (however, as integer linear programming is
not  known to  be in  $\P$, there  is no  good algorithm  that detects
this). In the next chapter we will  see that this leeds to a method to
construct the integer  hull of a polyhedron by  cutting the polyhedron
with  affine hyperplanes that  separate a  fractional vertex  from all
integral points in the polyhedron.

\begin{definition}[Edmonds and Giles 1977]
  A  rational  system $A\vx\le  \vb$  is  \emph{totally dual  integral
    (\TDI)}\mdef{totally dual integral\\\TDI} if
  \begin{align}
    \min(\vy^t\vb\mid  \vy^tA=\vc^t\,, \vy\ge \0)  = \max(\vc^t\vx\mid
    A\vx\le \vb)
    \label{eq:duality-tdi}
  \end{align}
  has an  integral optimum  dual solution $\ov  \vy$ for  all integral
  $\vc^t\in\Z^n$ for which the dual program is finite.
\end{definition}
Adding any valid inequality to a \TDI\ system preserves \TDI ness:
\begin{proposition}\label{prop:addingineqTDI}
  If $A\vx\le  \vb$ is  a \TDI\ system  and $\vc^t\vx\le \delta$  is a
  valid  inequality  for   $P:=\{\vx\mid  A\vx\le  \vb\}$,  then  also
  $A\vx\le \vb,\, \vc^t\vx\le \delta$ is \TDI.\mqed
\end{proposition}
\begin{proposition}
  Let  $A\vx\le   \vb$  be  a  \TDI-system   and  $\vb\in\Z^m$.   Then
  $\max(\vc^t\vx\mid  A\vx\le \vb)$ has  an integral  optimal solution
  for any rational $\vc^t$ for which the program is bounded.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  If $\vb$  is integral, then  $\min(\vy^t\vb\mid \vy^tA=\vc^t, \vy\ge
  \0)$  has an  integral  optimal  value for  all  integral $\vc^t$,  as
  $A\vx\le   \vb$    is   \TDI.    Now   the    claim   follows   from
  Corollary~\ref{cor:int-opt-val}.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{corollary}\label{cor:rhs-int-tdi}
  Let  $A\vx\le  \vb$  be  a  \TDI\  system  and  $\vb\in\Z^m$.   Then
  $P:=\{\vx\mid A\vx\le \vb\}$ is an integral polyhedron.\mqed
\end{corollary}
Theorem~\ref{thm:poly-integral} (or Corollary~\ref{cor:poly-integral})
now immediately implies  the following characterization, which implies
that \TDI\  is a  generalization of total  unimodularity in  the sense
that it  considers both the matrix  $A$ and the right  hand side $\vb$
instead of just the matrix $A$.
\begin{corollary}
  A rational  system $A\vx\le \vb$ is  \TDI\ for each  vector $\vb$ if
  and only if $A$ is totally unimodular.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Total   unimodularity   of   $A$   implies   that   the   polyhedron
  $P^*:=\{\vy\mid  \vy^tA=\vc^t,\, \vy\ge  \0\}$ is  integral  for any
  integral $\vc^t$. So $A\vx\le \vb$ is \TDI.  If $A\vx\le \vb$ is \TDI\
  for  any  integral  $\vb$,  then  $P:=\{\vx\mid  A\vx\le  \vb\}$  is
  integral for any $\vb$, so $A$ is totally unimodular.\mqed%
  \else\texttt{missing}\fi%
\end{proof}%
\fi%
% \begin{figure}[tb]
%   \centering
%   \includegraphics[height=.12\textheight]{tdi}
%   \caption{The minimal constraint system is not \TDI.}
%   \label{fig:not-tdi}
% \end{figure}
\begin{example}
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{tdi}
  \begin{minipage}{.4\linewidth}
    \captionof{figure}{\label{fig:not-tdi}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \TDI\ is a property  of the  inequality  system $Ax\le b$, not  of the
  polyhedron it defines: 
  \begin{flalign*}
    \text{Consider}&&A_1&:=\left(
      \begin{array}{rr}
        -1&-1\\0&-1\\1&-1
      \end{array}\right)&&\text{and}&
    A_2&:=\left(
      \begin{array}{rr}
        -1&-1\\1&-1
      \end{array}\right)&&
  \end{flalign*}
  Then $A_1\vx\le  \0$ and  $A_2\vx\le \0$ define  the same  cone (see
  Figure~\ref{fig:not-tdi}), but the first  is \TDI\ and the second is
  not.   One may  observe that  the rows  of the  first matrix  form a
  Hilbert basis of the dual cone, while the rows of the second do not.
  We will see later that this indeed characterizes \TDI.
\end{example}

In  the next chapter  we will discuss an  algorithm  that computes the
integer hull of a polyhedron.   This is based on the  fact that we can
describe  any  polyhedron by  a   \TDI\ system and   that  faces of  a
polyhedron described by  such  a system are   again given by   a \TDI\
system.  This will use the following theorem.
\begin{theorem}\label{thm:extend-tdi}
  Let $A\vx\le \vb$, $\va^t\vx\le \beta$  be a \TDI\ system. Then also
  $A\vx\le \vb$, $\va^t\vx=\beta$ is \TDI.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Let $\vc^t\in\Z^n$ such that
  \begin{align*}
    \max(\vc^t\vx\mid         A\vx\le         \vb,\,         \va^t\vx=
    \beta)=\max(\vc^t\vx\mid   A\vx\le  \vb,\,   \va^t\vx\le  \beta,\,
    -\va^t\vx\le -\beta)
  \end{align*}
  is finite with optimal solution  $\ov \vx$. The dual of this program
  is
  \begin{align}\label{eq:tdidual}
    \min(\vy^t\vb+(z_+-z_-)\beta\mid
    \vy^tA+(z_+-z_-)\va^t=\vc^t,\, \vy\ge \0)\,.
  \end{align}
  We have to  find an integral optimal solution  of this program.  Let
  $(\ov \vy, \ov z_+, \ov z_-)$ be a (possibly fractional) optimal
  solution.  Choose $k\in\N$ such that
  \begin{flalign*}
    &&&\ov   z_-\le k&&\text{and}&& k\cdot  \va\in\Z^n&&\\
    \text{and  let}&&&\hat  \vc^t:=\vc^t+k\va^t\,,&&\text{and}& &\ov  u:=\ov
    z_+-\ov z_-+k\,.&&
  \end{flalign*}
  $\ov \vx$ is also a feasible solution of
  \begin{align}
    \max(\hat     \vc^t\vx\mid      A\vx\le     \vb,\,     \va^t\vx\le
    \beta)\label{eq:tdimonprim}
  \end{align}
  and $(\ov \vy, \ov u)$ is a feasible solution of the dual program
  \begin{align}\label{eq:tdimin}
    \min(\vy^t\vb+u\beta\mid \vy^tA+u\va^t=\hat \vc^t, \vy\ge \0, u\ge
    0)\,.
  \end{align}
  The duality theorem implies that both programs~(\ref{eq:tdimonprim})
  and (\ref{eq:tdimin}) are bounded.  
  
  As    $A\vx\le   \vb$,    $\va^t\vx\le   \beta$    is    \TDI,   the
  system~(\ref{eq:tdimin}) has  an integral optimal  solution $(\tilde
  \vy, \tilde u)$.  
  \begin{flalign*}
    &\text{Let}&\tilde z_+&:=\tilde u&&\text{and}& \tilde z_-&:=k\,.&&
  \end{flalign*}
  \begin{flalign*}
    \text{Then}&& \tilde      \vy^tA+(\tilde   z_+-\tilde z_-)a^t=\tilde
    \vy^tA+\tilde u \va^t-k\va^t=\hat \vc^t-k\va^t=\vc^t\,.&&
  \end{flalign*}
  so $(\tilde  \vy, \tilde z_+,  \tilde z_-)$ is an  integral feasible
  solution of~(\ref{eq:tdidual}). Its objective value is
  \begin{align*}
    \tilde    \vy^t\vb+(\tilde   z_+-\tilde    z_-)\beta    =   \tilde
    \vy^t\vb+\tilde    u\beta-    k\beta    \le    \ov    \vy^t\vb+\ov
    u\beta-k\beta=\ov \vy^t\vb+(\ov z_+-\ov z_-)\beta\,,
  \end{align*}
  where  the inequality  follows  as $(\ov  \vy,\ov  u)$ is  feasible.
  $(\ov \vy,\ov z_+,\ov z_-)$ is  optimal, so we must have equality in
  this relation.  Hence, $(\tilde \vy,  \tilde z_+, \tilde z_-)$ is an
  integral optimal solution of the dual program~(\ref{eq:tdidual}).
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{proposition}
  Let $A\in\Z^{m\times n}$ and $\vb\in\Q^m$. The system
  \begin{align*}
    A\vx\ &\le \ \vb,\; \vx\  \ge \ \0\qquad\qquad&  &(\ A\vx\ =\ \vb,\;  \vx\ \ge \ \0\ )\\
    \intertext{is \TDI\  if and only  if} \min(\vy^t\vb&\mid \vy^tA\ge
    \vc^t,\,  \vy\ge  \0)\qquad\qquad&  &(\ \min(\vy^t\vb\mid  \vy^tA\ge
    \vc^t)\ )
  \end{align*}
  has an integral optimal solution for any integral $\vc^t$ for which it
  is finite.
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  The system $A\vx\le \vb,\, \vx\ge \0$ can be written as
  \begin{align*}
    \left(
      \begin{array}{r}
        A\\-\id
      \end{array}\right)\; \vx\ \le \
    \left(\begin{array}{r}
        \vb\\\0
      \end{array}\right)
  \end{align*}
  So it is \TDI\ if and only if for any integral $\vc^t$
  \begin{align}
    \min(\vy^t\vb\mid         \vy^tA-\vz^t=\vc^t,\,         \vy,\vz\ge
    \0)\label{eq:canoncal-tdi-dual}
  \end{align}
  has  an integral  optimal  solution $(\vy,\vz)$  if  the program  is
  bounded.  But this has an integral optimal solution if and only if
  \begin{align*}
    \min(\vy^t\vb\mid \vy^tA\ge \vc^t,\, \vy\ge \0)
  \end{align*}
  has an integral optimal solution.  The second claim is similar.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

Now we  give a geometric interpretation  of total  dual integrality by
relating it  to  Hilbert bases in  the  cones of the  normal  fan of a
polyhedron.
\begin{theorem}\label{thm:tdi-hilbert}
  Let  $A\in\Q^{m\times n}$,  $\vb\in\Q^m$  and $P:=\{\vx\mid  A\vx\le
  \vb\}$.The system  $A\vx\le \vb$  is \TDI\ if  and only if  for each
  face $F$  of $P$ the  rows of $A_{\eq(F)*}$  are a Hilbert  basis of
  $\cone(A_{\eq(F)*})$.
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Suppose that  $A\vx\le  \vb$ is   \TDI.   Let $F$  be   a face  of  $P$,
  $I:=\eq(F)$ and $J:=[m]-I$. Let $\vc^t\in\cone(A_{I*})\cap \Z^n$.  Then
  \begin{align}
    \max(\vc^t\vx\mid  A\vx\le   \vb)\  =\  \min(\vy^t\vb\mid  \vy^tA=
    \vc^t,\, \vy\ge \0)
    \label{eq:lp-tdi-hilbert}
  \end{align}
  is  optimally solved  by any  $\ov  \vx\in F$.   By assumption,  the
  minimum   has  an   integral  optimal   solution  $\ov   \vy$.   The
  complementary   slackness   theorem,  Theorem~\ref{thm:compl-slack},
  implies that  $\ov \vy_J=0$.  Hence  $\vc^t=\ov \vy_I A_{I*}$  is an
  integral conic combination of the generators of $\cone(A_{I*})$.

  Suppose   conversely   that   the   value   of   the   programs   in
  (\ref{eq:lp-tdi-hilbert})  is finite  for some  $\vc^t\in\Z^m$.  Let
  $F$  be  the   minimal  face  of  $P$  determined   by  $\vc^t$  and
  $I:=\eq(F)$. By the same argument  as before there is some $\ov \vy$
  such that
  \begin{align*}
    \vc^t&=\ov       \vy_I      A_{I*}\in\cone(A_{I*})&&\text{and}&\ov
    \vy_J&=\0\,.
  \end{align*}
  However,  $\ov  \vy_I\ge  \0$  may  now be  fractional.   Using  the
  assumption there  is a non-negative integral vector  $\vz$ such that
  $\vc^t=\vz^tA_{I*}$.   Extending $\vz$  with zeros  to $\ov  \vz$ such
  that $\vz=\ov \vz_I$ we obtain for all $\vx\in F$
  \begin{align*}
    \vc^t&=\lambda^tA&&\text{and}&          \ov          \vz^t\vb&=\ov
    \vz^tA\vx=\vc^t\vx\,.
  \end{align*}
  So  $\ov \vz$ is  an optimal  solution of~(\ref{eq:lp-tdi-hilbert}).
  As $\vc^t$ was arbitrary, this implies that $A\vx\le \vb$ is \TDI.
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%
As we may always choose the optimal solutions in the previous proof to
lie  in  a minimal  face  of  $P$ we  have  even  proved the  stronger
statement that it  suffices to consider only minimal  faces $F$ in the
theorem.  Using this and choosing  $\vb=\0$ in the previous theorem we
obtain
\begin{corollary}
  The rows of a rational  matrix form a Hilbert  basis if and only  if
  $A\vx\le \0$ is \TDI.\mqed
\end{corollary}
\begin{definition}
  Let $A\vx\le  \vb$ be \TDI.   Then $A\vx\le \vb$  is \emph{minimally
    \TDI}\mdef{minimally  \TDI} if  any proper  subsystem  of $A\vx\le
  \vb$ that defines the same polyhedron is not \TDI.
\end{definition}
A \TDI\  system $A\vx\le \vb$ is  minimally \TDI\ if and  only if each
constraint  defines a supporting  hyperplane of  $P:=\{\vx\mid A\vx\le
\vb\}$  and  cannot  be  written  as a  non-negative  integral  linear
combination of other inequalities in $A\vx\le \vb$.
\begin{theorem}
  Let $P$ be  a rational  polyhedron.
  \begin{compactenum}
  \item Then  there is  a \TDI\ system  $A\vx\le \vb$ for  an integral
    matrix $A$ that defines $P$.
  \item $\vb$ can be  chosen  to be  integral if   and  only if  $P$  is
    integral.
  \item If $P$ is full-dimensional, then there is a unique minimal
    \TDI\ system defining $P$.
  \end{compactenum}
\end{theorem}
See Figure~\ref{fig:tdi-for-triangle} for an example of a TDI-system
defining a triangle.
\ifproof%
\begin{proof}%
  \ifreleasedproof%
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{tdi-system}
  \begin{minipage}{.4\linewidth}
    \captionof{figure}{\label{fig:tdi-for-triangle}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
$(1)$\ Let  $F$ be a minimal face  of $P$ with normal  cone $C_F$.  By
Theorem~\ref{thm:Hilbertbasis}, the cone $C_F$ has an integral Hilbert
basis $\hilb_F:=\{\va_1, \ldots, \va_r\}$.  For $1\le i\le r$ define
\begin{align}
  \beta_i:=\max(\va_i^t\vx\mid \vx\in P)\,.\label{eq:betas}
\end{align}
\begin{flalign*}
  \text{Then}&& F&\subseteq\{\vx\mid \va_i^t\vx=\beta_i\}&&\text{and}&
  P&\subseteq \{\vx\mid \va_i^t\vx\le \beta_i\}&&
\end{flalign*}
for all  $i$.  Let  $\mathcal A_F$ be  the collection  of inequalities
$\va_i\vx\le \beta_i$.

Let $A\vx\le \vb$ be the inequality system obtained from all $\mathcal
A_F$, where $F$ ranges over all  minimal faces of $P$.  Then $A\vx \le
\vb$ determines $P$ and is \TDI\ by Theorem~\ref{thm:tdi-hilbert}.

$(2)$\ If  $P$ is  integral, then the  $\beta_i$'s in~(\ref{eq:betas})
are  integral.  So  also $\vb$  is integral.   If conversely  $\vb$ is
integral, then $P$ is integral by Corollary~\ref{cor:rhs-int-tdi}.

$(3)$\  If $P$  is full-dimensional,  then each  normal cone  $C_F$ is
pointed.   Hence,  again by  Theorem~\ref{thm:Hilbertbasis}  it has  a
unique  minimal integral  Hilbert  basis.  Let  $A\vx\le  \vb$ be  the
inequality system  obtained in the same  way as above,  but only using
these minimal  sets of generators.   By Theorem~\ref{thm:tdi-hilbert},
the  system $A\vx\le  \vb$  must  be a  subsystem  of any  \TDI-system
defining $P$.
% 
\else\texttt{missing}\fi%
\end{proof}%
\fi%
\begin{corollary}
  A rational  polyhedron $P$  is integral  if and only  if there  is a
  \TDI-system $A\vx\le \vb$ with integral $\vb$ that defines it.\mqed
\end{corollary}

We shortly  discuss the complexity  of the following problem:
\begin{align}
  \begin{minipage}{.8\linewidth}
    {Given a rational system of linear equations $Ax=b$, decide
      whether a rational system of linear equations has an integral
      solution.}
  \end{minipage}
  \label{eq:int-eq}
\end{align}
\textsc{Hermite} normal forms can be used to show that this problem is
well characterized.  It is  even in $\P$,  as a transforming  a matrix
into \textsc{Hermite} normal form can  by done in polynomial time (see
Schrijver's  book~\cite[Ch.\ 5]{schrijver_tlip}).   Using  the methods
introduced   in  Chapter~\ref{cha:compl-sizes-rati}   one   shows  the
following theorem.
\begin{theorem}%[{\cite[Thm.\ 5.2]{schrijver_tlip}}]
  Let $A\in\Q^{m\times n}$ with $\rank(A)=m$.
  \begin{compactenum}
  \item The \textsc{Hermite} normal form of  $A$ has size polynomially
    bounded by the size of $A$.
  \item There  is   a unimodular matrix  $U$   such that $AU$  is   in
    \textsc{Hermite} normal form  and the size  of $U$ is polynomially
    bounded in the size of $A$.  
  \end{compactenum}
\end{theorem}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  We  can assume that $A$  is  integral, as multiplication  of  a by a
  constant  also multiplies   the  Hermit  normal  form  by  the  same
  constant.
  \begin{compactenum}
  \item Let $\left(B\ 0\right)$ be the \textsc{Hermite} normal form of
    $A$   for   some  lower    triangular  non-singular  square matrix
    $B=(b_{ij})_{ij}$.   The  main idea of     the proof is  now   the
    following observation.  Let $j\in[m]$.  The g.c.d.\ of all maximal
    minors of the first $j$ rows  of $A$ is invariant under elementary
    column  operations.  Hence,  this g.c.d.\  is  the same in $A$ and
    $\left(B\ 0\right)$.  But in the latter it  is just the product of
    the first $j$ diagonal entries of $B$, as all other maximal minors
    are $0$.  Now $B$ has its maximal  entries on the diagonal, so the
    size of $B$ is polynomially bounded by that of $A$.
  \item  We may  assume    that  $A=\left(A_1\ A_2\right)$   for  some
    non-singular matrix $A_1$. Now  consider the following  matrix and
    its \textsc{Hermite} normal form:
    \begin{align*}
      &\left(
        \begin{array}{rr}
          A_1&A_2\\0&\id
        \end{array}\right)&&\text{with normal form}&\left(
        \begin{array}{rr}
          B&0\\B_1&B_2
        \end{array}\right)\,.
    \end{align*}
    The sizes of $B, B_1, B_2$ are polynomially bounded in the size of
    $A$. Then also 
    \begin{align*}
      U:=\left(
        \begin{array}{rr}
          A_1&A_2\\0&\id
        \end{array}\right)^{-1}\cdot \left(
        \begin{array}{rr}
          B&0\\B_1&B_2
        \end{array}\right)\,.
    \end{align*}
    has bounded size and $AU=\left(B\ 0\right)$.
  \end{compactenum}
  %
\else\texttt{missing}\fi%
\end{proof}%
\fi%

\begin{corollary}
  If a rational system $A\vx=\vb$  of linear equations has an integral
  solution, then it has one  of size polynomially bounded in the sizes
  of $A$ and $\vb$.
\end{corollary}
\begin{proof}
  We may   assume  that $\rank(A)=m$.  Then   there  is  a  unimodular
  transformation $U$ of  polynomially bounded  size  (in that  of $A$)
  such that $AU=\left(B\ 0\right)$ is in \textsc{Hermite} normal form.
  Then
  \begin{align*}
    \ov \vx:=U\left(
      \begin{array}{r}
        B^{-1}b\\0
      \end{array}\right)
  \end{align*}
  is an integral solution of $A\vx=\vb$ of bounded size.
\end{proof}
Using the integral alternative theorem,
Theorem~\ref{cor:integral_alternative}, this implies that the decision
problem~(\ref{eq:int-eq}) is in $\NP\cap \coNP$: We can assume that
$A$ has full rank and is given in \textsc{Hermite} normal form
$A=\left(B\ 0\right)$.  Then a positive certificate is given by the
previous corollary.  By the integral alternative theorem, for a
negative certificate we have to provide a rational vector $\vy$ such
that $\vy^tA$ is integral, but $\vy^t\vb$ is not.  By the proof of the
corollary, some row of $B^{-1}$ has this property if $A\vx=\vb$ does
not have an integral solution.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 
