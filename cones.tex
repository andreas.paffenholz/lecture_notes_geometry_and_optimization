
\chapter{Cones}

This and the  following two sections will introduce  the basic objects
in discrete  geometry and  optimization: cones, polyhedra,  and linear
programs. Each section also brings  some new methods necessary to work
with these objects.  We will see  that cones and polyhedra can both be
defined in two different  ways. We have an \emph{exterior} description
as   the  intersection   of  some   half  spaces,   and  we   have  an
\emph{interior}  description as  convex  and conic  combinations of  a
finite set  of points. The  main therem in  this and the  next section
will be  two versions of the  \textsc{Minkowski-Weyl} Theorem relating
the two descriptions. This will directly lead to the well known linear
programming duality, which we discuss in the third section.  The basic
tool for  the proof of  these duality theorems is  the \textsc{Farkas}
Lemma. This is sometimes is  also referred to as the \emph{Fundamental
  Theorem of  Linear Inequalities}\mdef{Fundamental Theorem  of Linear
  Inequalities\\alternative  theorem}.    It  is  an   example  of  an
\emph{alternative theorem}, i.e.\ it  states that of two given options
always exactly one  is true.  The \textsc{Farkas} Lemma  comes in many
variants, and we will  encounter several other versions and extensions
in  the next  sections.  Before  we can  actually start  with discrete
geometry we need  to review some material from  linear algebra and fix
some terminology.

\begin{definition}\label{def:linalg}
  Let   $\vx_1,  \ldots,   \vx_k\in\R^n$,   and  $\lambda_1,   \ldots,
  \lambda_k\in\R$.   Then  $\sum_{i=1}^k\lambda_i\vx_i$  is  called  a
  \emph{linear       combination}\mdef{linear       combination\\conic
    combination\\affine   combination\\convex   combination}  of   the
  vectors $\vx_1, \ldots, \vx_k$. It is further a
  \begin{compactenum}
  \item \emph{conic combination}, if $\lambda_i\ge0$,
  \item \emph{affine combination}, if $\sum_{i=1}^k\lambda_i=1$, and a
  \item \emph{convex combination}, if it is conic and affine.
  \end{compactenum}
  The \emph{linear} (\emph{conic}, \emph{affine}, \emph{convex})
  \emph{hull}\mdef{linear hull\\conic hull\\affine hull\\convex
    hull\\linear space\\cone\\affine space\\convex set} of a set
  $X\subseteq\R^n$ is the set of all points that are a linear (conic,
  affine, convex) combination of some finite subset of $X$.  It is
  denoted by $\lin(X)$ (or, $\cone(X)$, $\affhull(X)$, $\conv(X)$,
  respectively).  $X$ is a \emph{linear space} (\emph{cone},
  \emph{affine space}, \emph{convex set}) if $X$ equals its linear
  hull (or conic hull, affine hull, convex hull, respectively).
  Figure~\ref{fig:affcincinvhull} illustrates the affine, conic, and
  convex hull of the set $X:=\{(-1,5), (1,2)\}$.  The linear span of
  $X$ is the whole plane.
\end{definition}
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{aff-hull-flipped}
      \begin{minipage}{\marginparwidth}
\captionof{figure}{}
 \end{minipage}
 \label{fig:affcincinvhull}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
We denote the dual space of  $\R^n$ by $(\R^n)^*$.  This is the vector
space of all \emph{linear functionals} $\va:\R^n\rightarrow \R$. Given
some  basis $\ve_1,  \ldots, \ve_n$  of $\R^n$  and  the corresponding
basis  $\ve_1^*, \ldots,  \ve_n^*$ of  $(\R^n)^*$ (using  the standard
scalar  product)  we can  write  elements  $\vx\in  \R^n$ and  $\va\in
(\R^n)^*$ as (column) vectors
\begin{align*}
  \vx:=\left(
    \begin{smallmatrix}
      x_1\\ \vdots\\ x_n
    \end{smallmatrix}
\right) \qquad \va:=\left(
  \begin{smallmatrix}
    a_1\\ \vdots\\ a_n
  \end{smallmatrix}
\right).
\end{align*}
To  save space  we will  often write  vectors as  row vectors  in this
text. A linear functional $\va:\R^n\rightarrow \R$ then has the form
\begin{align*}
  \va(\vx)=\va^t\vx=a_1x_1+\cdots a_nx_n\,.
\end{align*}
Using  the  standard scalar  product  we  can  (and most  often  will)
identify  $\R^n$ and  its dual  space  $(\R^n)^*$, and  view a  linear
functional as a (row) vector $\va^t\in\R^n$. 

Let $B\in\R^{m\times n}$ be a matrix with column vectors $\vb_1,
\ldots, \vb_n$. Then $\cone(B)$ denotes the conic hull $\cone(\{\vb_1,
\ldots, \vb_n\})$ of these vectors.  Similarly, if $A\in\R^{m\times
  n}$ is a matrix of functionals $\va^t_1, \ldots, \va^t_m$ (i.e.\ the
rows of $A$), then $\cone(A):=\cone(\va^t_1, \ldots,
\va^t_m)\subseteq(\R^n)^*$.  Define $\lin(B)$, $\aff(B)$, $\conv(B),
\lin(A)$, $\aff(A)$, and $\conv(A)$ similarly.


\begin{definition}
  For  any non-zero linear  functional $\va^t\in\R^n$  and $\delta\in\R$
  the set\mdef{\mbox{ }\\linear half-space\\affine half-space}
  \begin{align*}
    \{\vx\mid    \va^t\vx\le    0\}&\qquad\text{is   a    \textbf{linear
        half-space},
      and}\\
    \{\vx\mid  \va^t\vx\le   \delta\}&\qquad\text{is  an  \textbf{affine
        half-space.}}
  \end{align*}
  Their    boundaries   $\{\vx\mid   \va^t\vx=0\}$    and   $\{\vx\mid
  \va^t\vx=\delta\}$       are       a       \emph{linear}\mdef{linear
    hyperplane\\affine   hyperplane}   and  \emph{affine   hyperplane}
  respectively.
\end{definition}

We can now define the basic objects that we will study in this course.
\begin{definition}\label{def:cones}
  \begin{compactenum}
  \item   A   \emph{polyhedral  cone}\mdef{polyhedral   cone\\finitely
      constrained cone}  (or a \emph{finitely constrained  cone}) is a
    subset $C\subseteq\R^n$ of the form
    \begin{align*}
      C:=\{\vx\in\R^n\mid A\vx\le \0\}
    \end{align*}
    for  a   matrix  $A\in\R^{m\times  n}$  of   row  vectors  (linear
    functionals).
  \item For  a finite number of vectors  $\vb_1, \ldots, \vb_r\in\R^n$
    the set
    \begin{align*}
      C=\cone(\vb_1, \ldots,
      \vb_r):=\left\{\textstyle\sum_{i=1}^r\lambda_i\vb_i\mid
        \lambda_i\ge 0\right\}=\{B\lambda\mid \lambda\ge0\}.
    \end{align*}
    is a \emph{finitely  generated cone $C$}\mdef{finitely generated},
    where $B$ is the matrix with columns $\vb_1, \ldots, \vb_r$.
  \end{compactenum}
\end{definition}
Definition~\ref{def:linalg}   immediately  implies  that   a  finitely
generated cone is a cone.  Note  that any $A$ uniquely defines a cone,
but the  same cone  can be defined  by different  constraint matrices.
Let $\lambda, \mu\ge 0$ and $\va_1^t, \va_2^t$ two rows of $A$. Then
\begin{align*}
  C:=\{\vx\mid A\vx\le \0\} = \{\vx\mid A\vx\le \0,\; (\lambda
  \va_1^t+\mu \va_2^t)\vx\le0\}
\end{align*}
Similarly, a  generating set uniquely  defines a cone, but  adding the
sum of all generators as a new generator does not change the cone.
\begin{definition}
  The    \emph{dimension}\mdef{dimension}   of    a   cone    $C$   is
  $\dim(C):=\dim(\lin(C))$.
\end{definition}
%%%%%%%%%%%%% Margin figure
    \marginnote{
      \psfrag{b1}[rb][rb]{$b_1$}
      \psfrag{b2}[lb][lb]{$b_2$}
      \psfrag{a1}[tr][tr]{$a_1$}
      \psfrag{a2}[tl][tl]{$a_2$}
      \includegraphics[width=.25\textwidth]{cone-example}
      \begin{minipage}{\marginparwidth}
      \captionof{figure}{\label{fig:cone-example}} 
      \end{minipage}
    }
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{example}
  Let $A:={\small\left(
      \begin{array}{rr}
        -2&-1\\2&-1
      \end{array}\right)}$, $\vb_1:={\small\left(
      \begin{array}{r}
        -1\\2
      \end{array}\right)}$, and  $\vb_2:={\small\left(
      \begin{array}{r}
        1\\2
      \end{array}\right)}$. 

  \smallskip  Then $C:=\{\vx\mid  A\vx\le \0\}$  and $C':=\cone(\vb_1,
  \vb_2)$ define the  same subset of $\R^2$. It is  the shaded area in
  Figure~\ref{fig:cone-example}.    Note  that   we  have   drawn  the
  functionals $\va_1^t,  \va_2^t$ in  the same picture,  together with
  the lines $\va_i^t\vx=0$, $i=1,2$.
\end{example}
We  want to  show  that the  two  definitions of  a  polyhedral and  a
finitely generated cone actually  describe the same objects, i.e.\ any
finitely  constrained  cone  has  a  finite generating  set,  and  any
finitely generated  cone can equally be described  as the intersection
of finitely many linear half spaces.

The main  tool we need in  the proof is  a method to solve  systems of
linear     inequalities,     known     as     \textsc{Fourier-Motzkin}
elimination\mdef{\textsc{Fourier-Motzkin} elimination}.  The analogous
task for  a system  of linear equations  is efficiently solved  by the
well known \textsc{Gau\ss}ian  elimination.  We will basically exploit
the  same  idea for  linear  inequalities.   However,  in contrast  to
\textsc{Gau\ss}ian elimination, it will not be efficient (nevertheless
it is  still one of the  best algorithms known for  this problem). 
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{fouriermotzkinpoly}    
  \begin{minipage}{\marginparwidth}
    \captionof{figure}{\label{fig:fouriermotzkinpoly}}
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% 
We start with an example to  explain the idea, before we give a formal
proof.  We consider the following system of linear inequalities.
\begin{align}
  \begin{array}{rcrcr}
    -x&+&y&\le&2\\
    x&+&2y&\le& 4\\
    -2x&-&y&\le&1\label{fmunred}\\
    x&-&2y&\le&2\\
    x&&&\le&2
  \end{array}
\end{align}
See  Figure~\ref{fig:fouriermotzkinpoly} for  an  illustration fo  the
solution  set.   For a  given  $x$ we  want  to  find conditions  that
guarantee the existence of a $y$  such that $(x,y)$ is a solution.  We
rewrite the  inequalities, solving for  $y$. The first  two conditions
impose an upper bound on $y$,
\begin{alignat*}{7}
  &&&&y\;&\le\;&2\;&+\;&x\\
  &&&&y&\le&2\;&-\;&\frac12x\\
  \intertext{the third and forth a lower bound,}
 -1&-\;&2x\;&\le&y \\
  -1\;&+\;&\frac12x\;&\le&y
 \intertext{while the last one does not affect $y$:}%
  &&x&\le&2
\end{alignat*}

Hence, for a given $x$, the system~(\ref{fmunred})  has a solution for
$y$ if and only if
\begin{align}
  \max(-1-2x,-1+\frac12x)\le                                       y\le
  \min(2+x,2-\frac12x)\,,\label{eq:solfory}
\end{align}
that is, if and only if the following system of inequalities holds:
\begin{align*}
  \begin{array}{rcrcrcrcrcr}
    -1&-&2x&\le&2&+&x\\
    -1&-&2x&\le&2&-&\frac12x\\
    -1&+&\frac12x&\le&2&+&x\\
    -1&+&\frac12x&\le&2&-&\frac12x
  \end{array}
\end{align*}
Rewriting  this in standard form, and  adding back the constraint that
does not involve $y$, we obtain
\begin{align}
  \begin{array}{rcrcr}
    -x&\le&1\\
    -x&\le&2\\
    -x&\le&6\\
    x&\le &3\label{fmred}\\
    x&\le&2
  \end{array}
\end{align}
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{fouriermotzkinpolyproj}    
  \begin{minipage}{\marginparwidth}
    \captionof{figure}{\label{fig:fouriermotzkinpolyproj}} 
  \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
This  system \eqref{fmred} of inequalities has  a solution if and only
if the  system \eqref{fmunred} has  a  solution.  However, it  has one
variable less. We can now iterate to obtain
\begin{align}
  \max(-6, -2,    -1)\ \le\     x\   \le    \   \min(2, 3)\,.\label{eq:solforx}
\end{align}
Now both the minimum and maximum do not involve a variable anymore, so
we can compute them to obtain that (\ref{fmunred}) has a solution if and only if
\begin{align*}
  -1\le x\le 2
\end{align*}
This  is  satisfiable,  so   (\ref{fmunred})  does  have  a  solution.
(\ref{eq:solforx})  tells us,  that any  $x$ between  $-1$ and  $2$ is
good.    If  we   have  fixed   some  $x$,   we  can   plug   it  into
(\ref{eq:solfory}) to obtain  a range of solutions for  $y$. The range
for  $x$  is  just  the  projection  of  the  original  set  onto  the
$x$-axis. See Figure~\ref{fig:fouriermotzkinpolyproj}.

In general,  by iteratively applying  this elimination procedure  to a
system of linear inequalities we  obtain a method to check whether the
system has  a solution (and we  can even compute  one, by substituting
solutions). 
\begin{theorem}[\textsc{Fourier-Motzkin}-Elimination]
  Let    $A\vx\le \vb$ be a system    of  linear inequalities with $n\ge1$
  variables and $m$ inequalities. 

  Then there is a system  $A'\vx'\le \vb'$ with $n-1$ variables and at
  most  $\max(m,\frac{m^2}4)$  inequalities,  such  that $\vs'$  is  a
  solution of $A'\vx'\le  \vb'$ if and only if  there is $\vs_0\in \R$
  such that $\vs:=(s_0,\vs')$ is a solution of $A\vx\le \vb$.
\end{theorem}
\ifproof
\begin{proof}
\ifreleasedproof%
  We classify the  inequalities depending on the coefficient  $a_{i0}$
  of $x_0$.   Let $U$ be  the indices of inequalities with $a_{i0}>0$,
  $E$ those with    $a_{i0}=0$ and  $L$ the  rest.    We multiply  all
  inequalities in $L$ and $U$ by $\frac{1}{|a_{i0}|}$.

  Now  we eliminate  $x_0$  by adding  any  inequality in  $L$ to  any
  inequality  in  $U$.   That  is,  our new  system  consists  of  the
  inequalities
  \begin{align}
    {\va'_j}^t\vx'+{\va'_k}^t\vx'& \le b_j+b_k&&
    \text{for } j\in L, k\in U \label{combinedfmineq}\\
    {\va'_l}^t\vx'&\le b_l&&\text{for } l\in E.\notag
  \end{align}
  Any solution $\vx$  of the original system yields  a solution $\vx'$
  of the new system by  just forgetting the first coordinate.  We have
  at most $|L|\cdot|U|+|E|$ many inequalities, which proves the bound.

  Now   assume,  our  new   system  has   a  solution   $\vx'$.   Then
  \eqref{combinedfmineq} implies
  \begin{align}
    {\va'_j}^t\vx'-b_j&\le b_k-{\va'_k}^t\vx'&&\text{for  all }j\in L,
    k\in U\notag%
    \intertext{which implies} %
    \max_{j\in       L}({\va'_j}^t\vx'-b_j)&\le\min_{k\in       U}(b_k
    -{\va'_k}^t\vx')\,. \label{minmaxfm}%
    \intertext{I
f we take for $x_0$ any value in between, then}
    x_0+{\va'_k}^t\vx'&\le b_k&&\text{for all }k\in U\notag\\
    -x_0+{\va'_j}^t\vx'&\le b_j&&\text{for all }j\in L\notag
  \end{align}
  The coefficient of $x_0$ is $0$ for all inequalities in $E$. Hence,
  they are also satisfied by $\vx=(x_0,\vx')$ and we have found a
  solution of $A\vx\le \vb$.
%
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{remark}
  \begin{compactenum}
  \item If $A, \vb$ are rational, then so are $A', \vb'$.
  \item Any  inequality in  the new system  is a conic  combination of
    inequalities of the original system.  This implies that there is a
    matrix $U\ge 0$ such that $A'=UA$ and $\vb'=U\vb$.
  \item $U$ or $L$ may be empty. In this case only the inequalities in
    the   set   $E$   survive.    More  specifically,   suppose   that
    $L=\varnothing$.  Then, given any solution $\vx'$ of the projected
    system, we can choose
    \begin{align*}
      x_0:=\min_{i:a_{i0}>0}\frac{b_i-\sum_{k=1}^na_{ik}x_k}{a_{i0}}
    \end{align*}
    and  obtain  a  solution  to  the original  system.   Further,  if
    $a_{i0}>0$ for all $i$, then  the projected system is empty, hence
    any point $\vx'\in\R^{n-1}$ lifts to a solution of $A\vx\le \vb$.
  \end{compactenum}
\end{remark}

Now that we have studied our new tool, let us get back to cones.
\begin{theorem}[\textsc{Weyl}'s Theorem]\mdef{\textsc{Weyl}'s Theorem}\label{thm:Weyl}
  A non-empty finitely generated cone $C$ is polyhedral.
\end{theorem}
\ifproof
\begin{proof}
  \ifreleasedproof%
  Let $C=\{B\vlambda\mid \vlambda\ge 0\}$ be a finitely generated cone
  with a matrix of generators $B\in \R^{n\times r}$. Then
    \begin{alignat*}{4}
      C&=\{\vx\in\R^n&&\mid\exists\,\vlambda\in\R^r\,:\;
      \vx=B\vlambda, \vlambda\ge \0\}\\
      &=\{\vx\in   \R^n&&\mid\exists\,\vlambda\in\R^r\,:\;  \vx-B\vlambda
      \le \0,\; -\vx+B\vlambda\le \0,\; -\vlambda\le \0 \}
    \end{alignat*}
    This set is the projection onto the first $n$ coordinates of the set
    \begin{align}
      C':=\{(\vx,\vlambda)\in\R^{n+r}\mid    \vx-B\vlambda\le    \0,\;
      -\vx+B\vlambda\le \0,\; -\vlambda\le \0 \}\;.\label{weyl}
    \end{align}
    Using   \textsc{Fourier-Motzkin}  elimination  to   eliminate  the
    variables $\lambda_1, \ldots, \lambda_r$ from the system of linear
    inequalities defining $C'$ we can write the cone $C$ as
    \begin{align*}
      C&=\{\vx\in \R^n\;\mid Ax\le \0\}
    \end{align*}
    for some  matrix $A\in\R^{m\times  n}$. Hence, $C$  is polyhedral.
    \else \texttt{missing} \fi%
\end{proof}
\fi Note  that the proof of  this theorem is  constructive.  Given any
finitely generated cone  $C$ we can write it  in the form~(\ref{weyl})
and   apply   \textsc{Fourier-Motzkin}   elimination   to   obtain   a
corresponding  system of  linear inequalities.   Using \textsc{Weyl}'s
theorem we can now prove a first variant of the \textsc{Farkas} Lemma.
\begin{theorem}[\textsc{Farkas} Lemma, Geometric Version]\mdef{\textsc{Farkas} Lemma}
  For any matrix $B\in\R^{m\times  n}$ and vector $\vb\in\R^m$ exactly
  one of the following holds:
  \begin{compactenum}
  \item   there  is   $\vlambda\in\R^m$  such   that  $B\vlambda=\vb$,
    $\vlambda\ge \0$, or
  \item  there   is  $\va\in  \R^n$  such  that   $\va^tB\le  \0$  and
    $\va^t\vb>0$.
  \end{compactenum}
\end{theorem}
Geometrically,  this theorem means  the following.   Given a  cone $C$
generated by the columns of $B$ and some vector $\vb$,
\begin{compactenum}
\item  either  $\vb\in  C$,  in  which  case  there  are  non-negative
  coefficients that  give a representation of $\vb$  using the columns
  of $B$,
\item  or $\vb\not\in  C$,  in which  case  we can  find a  hyperplane
  $H_\va$ given  by its normal $\va$,  such that $\vb$ and  $C$ are on
  different sides of $H_\va$.
\end{compactenum}
\ifproof
\begin{proof}[\textsc{Farkas} Lemma]
  \ifreleasedproof %
  The  two  statements cannot  hold  simultaneously:  Assume there  is
  $\vlambda\ge\0$ such that $B\vlambda=\vb$ and $\va\in\R^m$ such that
  $\va^tB\le \0$ and $\va^t\vb>0$. Then
  \begin{align*}
    0<\va^t\vb=\va^t(B\vlambda)=\va^t(B\vlambda)\le 0\,,
  \end{align*}
  a contradiction.   Let $C:=\{B\vmu\mid \vmu\ge 0\}$.   Then there is
  $\vlambda\ge \0$  such that $B\vlambda=\vb$  if and only  if $\vb\in
  C$.  By \textsc{Weyl}'s Theorem the cone $C$ is polyhedral and there
  is a matrix $A$ such that
  \begin{align}
    C=\{\vx\mid A\vx\le \0\}\,.\label{eq:cone2}
  \end{align}

  Hence, $\vb\not\in C$  if and only if there  is a functional $\va^t$
  among the rows of $A$  such that $\va^t\vb>0$. Clearly, $\vb_j\in C$
  for  each column  of  $B$, hence  $\va^tB\le  \0$.  So  $\va$ is  as
  desired. \else \texttt{missing} \fi
\end{proof}
\fi

\begin{definition}
  The  \emph{polar  (dual)}\mdef{polar  dual   of  a cone} of  a  cone
  $C\subset \R^n$ is the set
  \begin{align*}
    C^*:=\{\va\in\R^n\mid  \va^t\vx\le \0\;\text{  for  all }\;\vx\in
    C\}.
  \end{align*}
\end{definition}
See Figure~\ref{fig:dualcone} for and example of a cone and its dual.
%%%%%%%%%%%%% Margin figure
    \marginnote{\includegraphics[width=.25\textwidth]{dual_cone}    
      \begin{minipage}{.25\textwidth}
      \captionof{figure}{  \label{fig:dualcone}}
      \end{minipage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}\label{prop:coneprops}
  Let $C,D\subseteq \R^n$ be cones. Then the following holds:%
  \begin{compactenum}
  \item $C\subseteq D$ implies $D^*\subseteq C^*$.
  \item \label{item:coneindouble} $C\subseteq C^{**}$.
  \item \label{item:tripledual} $C^*=C^{***}$.
  \end{compactenum}
\end{proposition}
\ifproof
\begin{proof}
\ifreleasedproof
  \begin{compactenum}
  \item  $\va^t\in   D^*$  means  $\va^t\vx\le  0$   for  all  $\vx\in
    D\supseteq C$.
  \item $\vx\in C$ means $\va^t\vx\le 0$ for all $\va\in C^*$. 
  \item $\va\in C^{***}$ means  $\va^t\vx\le 0$ for all $x\in C^{**}$,
    which is equivalent to $\va\in C^*$.
  \end{compactenum}
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{lemma}
  Let $C$ be a cone.
  \begin{compactenum}
  \item\label{prop:item:finite} If $C=\{B\vlambda\mid \vlambda\ge 0\}$
    then $C^*=\{\va^t\mid \va^tB\le \0^t\}$.
  \item\label{prop:item:double}  if $C=\{\vx\mid  A\vx\le  \0\}$, then
    $C=C^{**}$.
  \item If $C$ is finitely generated then $C^{**}=C$.
  \item  If   $C=\{\vx\mid  A\vx\le  \0\}$  is   polyhedral  for  some
    $A\in\R^{m\times  n}$,  then  $C^*=\{\vlambda^t A\mid  \vlambda\ge
    \0\}$ is finitely generated.
  \end{compactenum}
\end{lemma}
\ifproof
\begin{proof}
\ifreleasedproof
  \begin{compactenum}
  \item Clearly,  the inequalities  are necessary.  Let  $\va$ satisfy
    $\va^tB\le\0^t$.      Then     $\vx=B\vlambda\in    C$     implies
    $\va^t\vx=\va^tB\vlambda\le 0$, as $\vlambda\ge \0$.
  \item      Let      $D:=\{\vlambda^tA\mid     \vlambda\ge      0\}$.
    $(\ref{prop:item:finite})$        implies        $C=D^*$       and
    Proposition~\ref{prop:coneprops}(\ref{item:tripledual})      then
    shows $C^{**}=D^{***}=D^*=C$.
  \item    Let     $C=\{B\vlambda\mid    \vlambda\ge    0\}$.     Then
    (\ref{prop:item:finite})  tells us  that  $C^*=\{\va\mid \va^tB\le
    \0\}$.                         

    By   Proposition~\ref{prop:coneprops}(\ref{item:coneindouble})  we
    need  only prove $C^{**}\subseteq  C$. But  this follows  from the
    observation that, if $\vb\not\in  C$, then, by the \textsc{Farkas}
    Lemma,    there   is   $\va^t$    such   that    $\va^tB\le   \0$,
    $\va^t\vb>0$. The first inequality  implies $\va^t\in C^*$ and the
    second $\vb\not \in C^{**}$.
  \item  $C$ is  the dual  of $D:=\{\vlambda^t  A\mid \vlambda\ge0\}$,
    i.e.\  $C=D^*$.   By  dualizing  again  and using  $(1)$  we  have
    $C^*=D^{**}=D$.
  \end{compactenum}
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{theorem}[\textsc{Minkowski}'s Theorem]\mdef{\textsc{Minkowski}'s Theorem}
  A polyhedral cone is non-empty and finitely generated.
\end{theorem}
\ifproof
\begin{proof}
  \ifreleasedproof Let $C=\{\vx\mid A\vx\le  \0\}$. Then $\0\in C$ and
  $C$ is  not empty.  Let $D:=\{\vlambda^tA\mid  \vlambda\ge 0\}$.  By
  \textsc{Weyl}'s Theorem, $D$ is polyhedral.  Hence $D^*$ is finitely
  generated.  But  $D^*=C$, and so  $C$ is finitely  generated.  \else
  \texttt{missing} \fi
\end{proof}
\fi

Combining   this    with   \textsc{Weyl}'s   Theorem    we   get   the
\emph{\textsc{Weyl-Minkowski}-Duality}\mdef{\textsc{Weyl-Minkowski}-Duality}
for cones.
\begin{theorem}
  A cone is polyhedral if and only if it is finitely generated.\mqed
\end{theorem}
This finally proves the claimed  equivalence of the two definitions of
a  cone in  Definition~\ref{def:cones}.   The \textsc{Fourier-Motzkin}
elimination  also  gives  us  a  method to  convert  between  the  two
representations (apply the  method to the dual if  you want to convert
from  a polyhedral  representation to  the generators).   In  the next
chapters we  will see that, although  mathematically equivalent, there
are properties  of cones  that are  trivial to compute  in one  of the
representations, but hard in  the other. This affects also algorithmic
questions. Efficient conversion between the two representations is the
fundamental algorithmic problem in polyhedral theory.


Before  we consider polyhedra  and their  representations in  the next
chapter we  want to list  some useful variants of  the \textsc{Farkas}
lemma that follow directly from the above geometric version.
\begin{proposition}\label{prop:farkas-variants}
  Let $B\in \R^{m\times n}$ and $\vb\in \R^m$. 
  \begin{compactenum}
  \item Either $B\vlambda=\vb$  has a solution or there  is $\va$ such
    that $\va^tB =\0$ and $\va^tb>0$, but not both.
  \item Either $B\vlambda\le \vb$ has  a solution, or there is $\va\le
    \0$ such that $\va^tB=0$ and $\va^t\vb>0$, but not both.
  \item\label{item:prop:canonical}    Either    $B\vlambda\le    \vb$,
    $\vlambda\ge \0$ has a solution  or there is $\va\le \0$ such that
    $\va^tB\le 0$ and $\va^t\vb>0$, but not both.\mqed
  \end{compactenum}
\end{proposition}

We finish this chapter with an application of \textsc{Fourier-Motzkin}
elimination    to   linear    programming.    \textsc{Fourier-Motzkin}
elimination allows us to
\begin{compactenum}
\item decide  whether a linear  program is feasible, and
\item determine an optimal solution.
\end{compactenum}
Let a linear program
\begin{align*}
  \text{maximize}\qquad \vc^t\vx&&\text{subject to}\qquad A\vx\le \vb
\end{align*}
be given,  with $A\in\R^{m\times n}$,  $\vb\in\R^m$, and $\vc\in\R^n$.
If  we  apply \textsc{Fourier-Motzkin}  elimination  $n$-times to  the
system  $A\vx\le  \vb$,   then  no  variable  is  left   and  we  have
inequalities of the form
\begin{align}
  0\le \alpha_j\label{fm-linear1}
\end{align}
for  right  hand    sides   $\alpha_1, \ldots,  \alpha_k$.    By   the
\textsc{Fourier-Motzkin} theorem, the   system has a solution  if  and
only if all inequalities in  (\ref{fm-linear1}) have a solution, i.e.\
if all $\alpha_j$ are non-negative.

To  obtain the optimal value, we  add an additional variable $x_{n+1}$
and extend our system as follows
\begin{align*}
  B:=\left(
    \begin{array}{rr}
      A&0\\-\vc^t&1
    \end{array}\right)\qquad \vd:=\left(
    \begin{array}{r}
      \vb\\0
    \end{array}\right)\;.
\end{align*}
In  the system  $B\left(
  \begin{smallmatrix}
    \vx\\x_{n+1}
  \end{smallmatrix}\right)\le  \vd$ we  eliminate  the first  $n$
variables, and  we obtain upper and  lower bounds on  $x_{n+1}$ in the
form
\begin{align*}
  \alpha_j\le x_{n+1}\le \beta_j
\end{align*}
The minimum  over the $\beta_j$ is  our optimal value, as  this is the
largest possible value such that there is $\vx\in\R^n$ with
\begin{align*}
  -\vc^t\vx+x_{n+1}\le   0\qquad\Longleftrightarrow\qquad   x_{n+1}\le
  \vc^t\vx\;.
\end{align*}
If we  need the minimum over  $\vc^t\vx$ then we add  the row $(\vc^t,
-1)$  instead.   Observe however,  that  this  procedure  is far  from
practical, the  number of inequalities  may grow exponentially  in the
number of eliminated variables (can you find an example for this?).




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 
