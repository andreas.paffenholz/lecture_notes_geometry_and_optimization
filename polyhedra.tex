\chapter{Polyhedra}
\label{cha:polyhedra}

Now we generalize the results of the previous section to polyhedra and
collect basic geometric and  combinatorial properties. A polyhedron is
a quite  natural generalization of a  cone. We relax the  type of half
spaces we  use in  the definition and  allow affine instead  of linear
boundary hyperplanes. We will see that we can use the theory developed
in  the previous section  to state  an affine  version of  our duality
theorem.  We  will continue this  in Section~\ref{cha:faces-polyhedra}
with the  study of faces of  polyhedra after we  have discussed Linear
Programming and Duality in the next section.
\begin{definition}
  \begin{compactenum}
  \item A \emph{polyhedron}\mdef{polyhedron} is a subset
    $P\subseteq\R^n$ of the form
    \begin{align*}
      P=P(A,\vb)=\{\vx\in\R^n\mid Ax\le \vb\}
    \end{align*}
    for a matrix $A\in\R^{m\times n}$ of row vectors and a vector
    $\vb\in\R^m$.
  \item  A  \emph{polytope}\mdef{polytope} is  the  convex  hull of  a
    finite set of points in $\R^n$.
  \end{compactenum}
\end{definition}
\begin{definition}
  The \emph{dimension}\mdef{dimension}  of a non-empty  polyhedron $P$
  is  $\dim(P):=\dim(\aff(P))$.   The  dimension of  $\varnothing$  is
  $-1$.
\end{definition}
\begin{example}
  \begin{compactenum}
  \item Polyhedral cones are polyhedra.
  \item   The matrix $A:={\small\left(
      \begin{array}{rr}
        -1&1\\1&0\\0&1
      \end{array}\right)}$ and the vector $\vb:={\small\left(
      \begin{array}{r}
        1\\0\\ 1
      \end{array}\right)}$ 
%%%%%%%%%%%%% Margin figure
    \marginnote{
      \includegraphics[width=.25\textwidth]{polyhedron-example}
      \begin{minipage}{\marginparwidth}\mbox{}\\
      \captionof{figure}{\label{fig:polyhedron-example}} 
      \end{minipage}
    }
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  define the polyhedron shown in Figure~\ref{fig:polyhedron-example}.
  \item The system of linear inequalities and equations 
    \begin{alignat*}{3}
      B\vx\,&&+\,C\vy&\,\le\, \vc\\
      D\vx\,&&+\,E\vy&\,=\, \vd\tag{*}\label{eq:generalsystem}\\
      \vx\,&&&\,\ge \0
    \end{alignat*}
    for compatible matrices $B, C, D$ and $E$  and vectors $\vc, \vd$ is a
    polyhedron:
    \begin{align*}
      \text{Put}\qquad&&A&:=\left(
        \begin{array}{rr}
          B&C\\
          D&E\\
          -D&-E\\
          -I& 0
        \end{array}\right)&
      b&:=\left(
        \begin{array}{r}
          \vc\\ \vd\\ -\vd\\\0
        \end{array}\right)
    \end{align*}
  \item   $\R^n=\{\vx\mid   \0^t\vx\le  0\}$,   $\varnothing=\{\vx\mid
    \0^t\vx\le -1\}$ and affine spaces are polyhedra.
  \item cubes $\conv(\{0,1\}^n)\subset\R^n$ are polytopes.
  \end{compactenum}
\end{example}
\begin{proposition}
  Arbitrary   intersections of  polyhedra   with  a  affine spaces  or
  polyhedra are polyhedra.
\end{proposition}
\ifproof
\begin{proof}%
  \ifreleasedproof%
  $P(A,\vb)\cap P(A',\vb')=\{\vx\mid  A\vx\le \vb,\, A'\vx\le \vb'\}$.
  \else \texttt{missing} \fi%
\end{proof}%
\fi
A polyhedron  defined by linear  inequalities is a  finitely generated
cone by our considerations in the previous section, and we will see in
the  next theorem  that any  bounded  polyhedron is  a polytope.   The
general case interpolates between these two by taking a certain sum of
a cone and a polytope.
\begin{definition}
  For two subsets $P,Q\subseteq\R^n$, the set
  \begin{align*}
    P+Q:=\{p+q\mid p\in P,\;q\in Q\}
  \end{align*}
  is the \emph{\textsc{Minkowski} sum}\mdef{\textsc{Minkowski} sum} of
  $P$ and $Q$.
\end{definition}
We  will  prove later  that  Minkowski  sums  of polyhedra  are  again
polyhedra.   The next  theorem gives  the analogue  of \textsc{Weyl}'s
Theorem  for  polyhedra.  We  will  prove  it  by reducing  a  general
polyhedron         $P$          to         a         cone,         the
\emph{homogenization}\mdef{homogenization} $\homog(P)$ of $P$.
\begin{theorem}[Affine        \textsc{Weyl}       Theorem]\mdef{Affine
    \textsc{Weyl} Theorem}
  Let $B\in \R^{n\times p}$, $C\in\R^{n\times q}$, and
  \begin{align*}
    P:=\conv(B)+\cone(C)=\{B\vlambda+C\vmu\mid  \vlambda,  \vmu\ge  0,
    \textstyle\sum \lambda_i=1\}\,.
  \end{align*}
  Then  there is  $A\in  \R^{m\times n}$  and  $\vb\in\R^m$ such  that
  $P=\{\vx\in\R^n\mid A\vx\le \vb\}$.
\end{theorem}
\ifproof
\begin{proof}
  \ifreleasedproof%
  For   $P=\emptyset$   we   can   take  $A=0$   and   $\vb=-1$.    If
  $P\ne\emptyset$,   but   $B=0$,   then   the  theorem   reduces   to
  \textsc{Weyl}'s Theorem  for cones.  So  assume that $P\ne\emptyset$
  and $p>0$.  We define the following finitely generated cone:
    \begin{align*}
      Q&:=\left\{%
        \left(
          \begin{array}{rr}%
            \1^t&\0^t \\B &C
          \end{array}
        \right)
        \left(
          \begin{array}{r}
            \vlambda\\\vmu
          \end{array}
        \right)%
        \;\Bigm|\; \vlambda, \vmu\ge 0\right\}\\%
      &=\left\{\,\left(
          \begin{array}{c}
            \textstyle\sum  \lambda_i\\ B\vlambda+C\vmu
        \end{array}
        \right)
        \bigm| \vlambda, \vmu\ge 0\right\}.
  \end{align*}
  $Q$ is  the \emph{homogenization} of  $P$.  We obtain  the following
  correspondence between points in $P$ and in $Q$:
  \begin{align}
    \vx\in    P\quad\Longleftrightarrow\quad    \left(\begin{array}{r}
        1\\\vx
    \end{array}\right)
    \in Q\,.\label{eq:homogweyl}
  \end{align}
  $Q$ is a cone, so by \textsc{Weyl}'s Theorem~\ref{thm:Weyl} there is
  a matrix $A'$ such that
  \begin{align*}
    Q=\{\left(
      \begin{smallmatrix}
        x_0\\\vx
      \end{smallmatrix}
      \right)\mid A'\left(
      \begin{smallmatrix}
        x_0\\\vx
      \end{smallmatrix}
      \right)\le \0\}\,.
  \end{align*}
  Write $A'$ in the form $A'=(-\vb|A)$ by separating the first column.
  Using the correspondence  (\ref{eq:homogweyl}) between points in $P$
  and in $Q$ we obtain $P=\{\vx\in\R^n\mid A\vx\le \vb\}$.
  \else \texttt{missing} \fi
\end{proof}
\fi%
In particular, we obtain from  this Theorem, that the Minkowski sum of
a  polytope and  a  cone is  a  polyhedron. Using  the  same trick  of
homogenizing  a   polyhedron  we  can  also  prove   the  analogue  of
\textsc{Minkowski}'s Theorem.
\begin{theorem}[Affine \textsc{Minkowski} Theorem]\mdef{Affine \textsc{Minkowski} Theorem}\label{thm:affineminkowski}
  Let $P=\{\vx\in\R^n\mid A\vx\le \vb\}$ for some $A\in\R^{m\times n}$
  and  $\vb\in   \R^m$.  Then   there  is  $B\in\R^{n\times   p}$  and
  $C\in\R^{n\times q}$ such that
  \begin{align*}
    P=\conv(B)+\cone(C)\,.   
  \end{align*}
\end{theorem}
\ifproof
\begin{proof}
  \ifreleasedproof If $P=\emptyset$, then we  let $B$ and $C$ be empty
  matrices (i.e.\ we take $p=q=0$).  Otherwise, we define a polyhedral
  cone $Q\subseteq\R^{n+1}$ as
  \begin{align*}
    Q:=\left\{\left(
        \begin{array}{l}
          x_0\\\vx
        \end{array}\right)\Bigm|
      \left(\begin{array}{cc}
          -1&\0^t\\-\vb&A
        \end{array}\right)\left(
        \begin{array}{l}
          x_0\\\vx
        \end{array}\right)\le \0\right\}
  \end{align*}
  Again,  in the  same way  as in  the previous  proof, we  obtain the
  correspondence $\vx\in  P$ if and  only if $(1,\vx)^t\in  Q$ between
  points  of $P$  and $Q$.   $Q$ is  the \emph{homogenization}  of the
  polyhedron $P$  as a polyhedral  cone. The defining  inequalities of
  $Q$   imply   $x_0\ge  0$   for   all   $(x_0,\vx)$   in  $Q$.    By
  \textsc{Minkowski}'s     Theorem      for     cones     there     is
  $M\in\R^{r\times(n+1)}$ such that
  \begin{align*}
    Q=\{M\veta\mid \veta\ge 0\}.
  \end{align*}
  The columns  of $M$ are the  generators of $Q$.  We  can reorder and
  scale these  generators with a positive scalar  without changing the
  cone $Q$. Hence, we can write $M$ in the form
  \begin{align*}
    M=
    \left(\begin{array}{rr}
        \1^t&\0^t\\B&C
      \end{array}\right)
  \end{align*}
  for  some $B\in\R^{n\times p}$,  $C\in\R^{n\times q}$  with $p+q=r$.
  Split $\veta=(\vlambda,\vmu)\in \R^p\times \R^q$ accordingly. Then
  \begin{align*}
    Q=\left\{\,\left(\,
        \begin{array}{c}
          \1^t\vlambda\\
          B\vlambda+C\vmu\,
        \end{array}
        \right)\Bigm|\, \vlambda,\vmu\ge\0\right\}\,.
  \end{align*}
  $P$ is  the subset of all  points with first  coordinate $x_0=1$, so
  $P=\conv(B)+\cone(C)$.  \else \texttt{missing} \fi
\end{proof}
\fi   Combining   the   affine   versions   of   \textsc{Weyl}'s   and
\textsc{Minkowski}'s   Theorem  we  obtain   a  duality   between  the
definition  of  polyhedra  as  solution  sets  of  systems  of  linear
inequalities  and Minkowski sums  of a  convex and  conic hull  of two
finite point sets.
\begin{theorem}[Affine \textsc{Minkowski-Weyl}-Duality]\mdef{Affine \textsc{Minkowski-Weyl}-Duality}
  A subset  $P\subset\R^n$ is a  polyhedron if and  only if it  is the
  \textsc{Minkowski}  sum  of  a  polytope and  a  finitely  generated
  cone.\mqed
\end{theorem}
Using  this  duality  we  can  characterize  all  polyhedra  that  are
polytopes.
\begin{corollary}
  $P\subseteq\R^n$ is  a  polytope if and  only  if $P$  is  a bounded
  polyhedron.
\end{corollary}
\ifproof
\begin{proof}
  \ifreleasedproof%
  Any polytope  $P=\{\lambda^tA\mid \lambda \ge  0, \sum\lambda_i=1\}$
  is  a polyhedron  by the  affine  \textsc{Weyl} Theorem,  and it  is
  contained in the ball with  radius $\sum\|a_i\|$ around any point in
  $P$.

  Conversely,   any   polyhedron   can   be  written   in   the   form
  $P=\{B\vlambda+C\vmu\mid     \vlambda,     \vmu\ge    \0,\;     \sum
  \lambda_i=1\}$.   If   it  is  bounded  then   $C=0$,  as  otherwise
  $rC\vlambda\in P$ for any $r\ge 0$.  \else \texttt{missing} \fi
\end{proof}
\fi

Here are some examples to illustrate this theorem.
\begin{example}\label{ex:polyhedra}
  \begin{compactenum}
  \item  The   $3$-dimensional  \emph{standard  simplex}\mdef{standard
      simplex} is the polytope defined as the convex hull of the three
    unit vectors,
    \begin{align*}
      \Delta_3&:=\conv\left( \left(\scriptsize\begin{array}{r} 1\\0\\0
          \end{array}\right),
        \left(\scriptsize\begin{array}{r}
            0\\1\\0
          \end{array}\right),
        \left(\scriptsize\begin{array}{r}
            0\\0\\1
          \end{array}\right)\right)=\left\{
        \left(\scriptsize\begin{array}{r}
            x\\y\\z
          \end{array}\right)\Bigm|x,y,z\ge 0,\, x+y+z=1\right\}\,.
    \end{align*}
  \item Here  is an example of  an unbounded polyhedron that  is not a
    cone.
    \begin{align*}
      P_2&=\left\{
        \left(\scriptsize\begin{array}{r}
            x\\y
          \end{array}\right)\Bigm|
        \left(\scriptsize\begin{array}{rr}
            -2&-1\\-1&-2\\2&-1\\-1&2
          \end{array}\right)
        \left(\scriptsize\begin{array}{r}
            x\\y
          \end{array}\right)\le
        \left(\scriptsize\begin{array}{r}
            -3\\-3\\-3\\-3
          \end{array}\right)\right\}\\
      &=\conv\left(
        \left(\scriptsize\begin{array}{r}
            3\\0
          \end{array}\right),
        \left(\scriptsize\begin{array}{r}
            0\\3
          \end{array}\right),
        \left(\scriptsize\begin{array}{r}
            1\\1
          \end{array}\right)\right)+\cone\left(
        \left(\scriptsize\begin{array}{r}
            1\\2
          \end{array}\right),
        \left(\scriptsize\begin{array}{r}
            2\\1
          \end{array}\right)\right)\,.
    \end{align*}
  \item We have  already seen that the representation  of a polyhedron
    as the set of solutions of  a linear system of inequalities is not
    unique. The same is true for its representation as a Minkowski sum
    of a polytope and a cone.
    \begin{align*}
      P_3&=\left\{ \left(\scriptsize\begin{array}{r} x\\y\\z
            \end{array}\right)\Bigm|
          \left(\scriptsize\begin{array}{rrr}
              1&1&-1\\
              -1&-1&-1\\
              0&0&-1
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} x\\y\\z
            \end{array}\right)\le
          \left(\scriptsize\begin{array}{r} 1\\1\\-1
            \end{array}\right)\right\}\\
        &=\conv\left(
          \left(\scriptsize\begin{array}{r} 1\\1\\1
            \end{array}\right),
          \left(\scriptsize\begin{array}{r} -1\\-1\\1
            \end{array}\right)\right)+\cone\left(
          \left(\scriptsize\begin{array}{r} 1\\-1\\0
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} -1\\1\\0
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} 1\\1\\2
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} -1\\-1\\2
            \end{array}\right)\right)\\
        &=\conv\left(
          \left(\scriptsize\begin{array}{r} 2\\0\\1
            \end{array}\right),
          \left(\scriptsize\begin{array}{r} -2\\0\\1
            \end{array}\right)\right)+\cone\left(
          \left(\scriptsize\begin{array}{r} 1\\-1\\0
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} -1\\1\\0
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} 1\\1\\2
            \end{array}\right)
          \left(\scriptsize\begin{array}{r} -1\\-1\\2
            \end{array}\right)\right)
      \end{align*}
    \item   \textsc{Fourier-Motzkin}    elimination   allows   us   to
      explicitely compute one representation  of a polyhedron from the
      other.  We  give  an  explicit  example.  Let  $\id[3]$  be  the
      $(3\times 3)$-identity matrix and
    \begin{align*}
      P:=\conv(\id[3])=\{\id[3]\vlambda\mid          \vlambda\ge          0,\;
      \sum\lambda_i=1\}\;.
    \end{align*}

    We want to compute a polyhedral description.  As in the proof of
    the Affine \textsc{Weyl} Theorem, we extend the system to
    \begin{align*}
      \ov P:=\left\{\left(
          \begin{smallmatrix}
            \1^t\\\id[3]
          \end{smallmatrix}
        \right)\vlambda\mid \vlambda\ge 0\right\}\;.
    \end{align*}
    We apply \textsc{Fourier-Motzkin}-elimination to the system
    \begin{align*}
      x_0-\lambda_1-\lambda_2-\lambda_3&\le 0&    -x_0+\lambda_1+\lambda_2+\lambda_3&\le 0& \\
      x_1-\lambda_1&\le 0&      -x_1+\lambda_1&\le 0& -\lambda_1&\le 0\\
      x_2-\lambda_2&\le 0&      -x_2+\lambda_2&\le 0& -\lambda_2&\le 0\\
      x_3-\lambda_3&\le 0& -x_3+\lambda_3&\le 0& -\lambda_2&\le 0
    \end{align*}
    Eliminating all $\lambda_j$ from this we obtain
    \begin{align*}
      x_0-x_1-x_2-x_3&\le 0&-x_1&\le 0\\
      -x_0+x_1+x_2+x_3&\le 0&-x_2&\le 0\\
      -x_0&\le 0&-x_3&\le 0
    \end{align*}
    so that $\ov P=\{\ov \vx=\left(
      \begin{smallmatrix}
        x_0\\\vx
      \end{smallmatrix}
      \right)\mid \ov A\ov \vx\le 0\}$ for
    \begin{align*}
      \ov A&:=\left(\scriptsize
        \begin{array}{rrrr}
          1&-1&-1&-1\\
          -1&1&1&1\\
          -1&0&0&0\\
          0&-1&0&0\\
          0&0&-1&0\\
          0&0&0&-1
        \end{array}\right)
    \end{align*}
    Separating  the  first column  we  obtain  $P=\{\vx\mid x_i\ge  0,
    x_1+x_2+x_3=1\}$.

  \item The $3$-dimensional \emph{unit cube}\mdef{unit cube} is
    \begin{align*}
      C_3&:=\conv\left(
        \left(\scriptsize\begin{array}{rrrrrrrr}
          0&1&0&0&1&1&0&1\\0&0&1&0&1&0&1&1\\0&0&0&1&0&1&1&1
        \end{array}\right)
    \right)\\[.2cm]
    &=\{(x_1, x_2, x_3)^t\mid 0\le x_i\le 1,\; 1\le i\le 3\}\;.
    \end{align*}
  \end{compactenum}
\end{example}
Summarizing  our  results  we   have  two  different  descriptions  of
polyhedra,
\begin{compactenum}
\item either as the solution set of a system of linear inequalities
\item or  as the \textsc{Minkowski} sum  of a polytope  and a finitely
  generated cone.
\end{compactenum}
The  first  description  is called  the  \emph{exterior}\mdef{exterior
  description\\$H$-description\\interior description\\$V$-description}
or  \emph{$H$-description}   of  a  polyhedron,  the   second  is  the
\emph{interior} or \emph{$V$-description}.

Both are  important for various problems in  polyhedral geometry. When
working  with polyhedra  it is  often  important to  choose the  right
representation.  There  are many properties  of a polyhedron  that are
almost trivial to compute in  one representation, but very hard in the
other. Here are two examples.
\begin{compactitem}
\item  Using   the  inequality   description  it  is   immediate  that
  intersections of polyhedra are polyhedra, and
\item using the  interior description, it is straight  forward (and we
  will do it  in the next proposition) to show  that affine images and
  \textsc{Minkowski} sums of polyhedra are polyhedra.
\end{compactitem}
A map $f:\R^n\longrightarrow\R^d$  is an \emph{affine map}\mdef{affine
  map}  if  there  is  a  matrix $M\in\R^{d\times  n}$  and  a  vector
$\vb\in\R^d$ such that $f(\vx)=M\vx+\vb.$ Two polytopes $P\in\R^n$ and
$Q\in\R^d$ are \emph{affinely equivalent}\mdef{affinely equivalent} if
there  is an  affine map  $f:\R^n\rightarrow\R^d$ such  that $f(P)=Q$.
For any  polyhedron $P\in\R^n$ with  $\dim(P)=d$ there is  an affinely
equivalent  polyhedron  $Q\in\R^d$.   The   first  part  of  the  next
proposition supports this definition.
\begin{proposition}\label{prop:affine_minkowski}
  \begin{compactenum}
  \item Affine images of polyhedra are polyhedra.
  \item \textsc{Minkowski} sums of polyhedra are polyhedra.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  (1)\,  Let  $P:=\{B\vlambda+C\vmu\mid   \vlambda,  \vmu\ge  0,  \sum
  \lambda_i=1\}$ for $B\in \R^{n\times  p}$, $C\in\R^{n\times q}$ be a
  polyhedron  and  $f:\vx\longmapsto  M\vx+\vt$ with  $M\in\R^{d\times
    n}$,  $\vt\in\R^d$  an  affine  map.   Let $T$  be  the  $(n\times
  r)$-matrix whose columns are copies of $\vt$. Let
  \begin{align*}
    \ov B&:=MB+T\qquad\text{and}&\ov C:=MC\;.
  \end{align*}
  Then
  \begin{align*}
    f(P)&=\left\{M(B\vlambda+C\vmu)+\vt\mid    \vlambda,    \vmu\ge    0,
    \textstyle\sum \lambda_i=1\right\}\\
  &=\left\{\ov    B\vlambda+\ov    C\vmu\mid    \vlambda,   \vmu\ge    0,
    \textstyle\sum \lambda_i=1\right\}
  \end{align*}
  which is again a polyhedron.

  (2)\,   Let  $P=\conv(\vb_1,  \ldots,   \vb_r)+\cone(\vy_1,  \ldots,
  \vy_s)$, $P'=\conv(\vb'_1,  \ldots, \vb'_{r'})+\cone(\vy'_1, \ldots,
  \vy'_{s'})$ be two polyhedra. We claim that their Minkowski sum is
  \begin{align*}
    P+P'=\conv(\vb_i+\vb'_j\mid    1\le     i\le    r,    1\le    j\le
    r')+\cone(\vy_i, \vy'_j\mid 1\le i\le s, 1\le j\le s')\,.
  \end{align*}
  We prove both inclusions for this equality. Let
  \begin{align*}
    \vp:=\left(\textstyle\sum_{i=1}^r\lambda_i\vb_i
      +\textstyle\sum_{j=1}^s\mu_j\vy_j\right)
    +\left(\textstyle\sum_{i=1}^{r'}\lambda'_i\vb'_i
      +\textstyle\sum_{j=1}^{s'}\mu'_j\vy'_j\right) \in P+P'
  \end{align*}
  with    $\sum_{i=1}^r\lambda_i=\sum_{i=1}^{r'}\lambda'_i=1$.    Then
  \begin{align*}
    \vy:=\textstyle\sum_{j=1}^s\mu_j\vy_j
    +\textstyle\sum_{j=1}^{s'}\mu'_j\vy'_j\in  \cone(\vy_i, \vy'_j\mid
    1\le i\le s, 1\le j\le s')\,,
  \end{align*}
  so we have to show that
  \begin{align}
    \vx:=\textstyle\sum_{i=1}^r\lambda_i\vb_i
    +\textstyle\sum_{j=1}^{s'}\mu'_j\vy'_j\in    \conv(\vb_i+\vb'_j\mid
    1\le i\le r, 1\le j\le r')\label{eq:msumx}
  \end{align}
  We   successively   reduce  this   to   a   convex  combination   of
  $\vb_i+\vb'_j$  in  the  following  way.   We can  assume  that  all
  coefficients  in this sum  are strictly  positive (remove  all other
  summands  from  the sum).   Choose  the  smallest coefficient  among
  $\lambda_1,  \ldots, \lambda_r, \lambda'_1,  \ldots, \lambda'_{r'}$.
  Without  loss  of  generality   let  this  be  $\lambda_1$.   Define
  $\lambda''_1:=\lambda'_1-\lambda_1\ge 0$, $\lambda''_i:=\lambda'_i$,
  $2\le i\le r'$, and $\eta_{11}:=\lambda_1$. Then
  \begin{align*}
    \vx-\eta_{11}(\vb_1+\vb'_1) = \textstyle\sum_{i=2}^r\lambda_i\vb_i
    +\textstyle\sum_{i=1}^{r'}\lambda''_i\vb'_i\,.
  \end{align*}
  and  $\sum_{i=2}^r\lambda_i=\sum_{i=1}^{r'}\lambda''_i$. The  sum on
  the right hand side contains at  least one summand less than the sum
  in (\ref{eq:msumx}), so repeating  this procedure a finite number of
  times gives a representation of $\vx$ as
  \begin{align*}
    \vx=\sum_{1\le i\le r\atop 1\le j\le r'}\eta_{ij}(\vb_i+\vb'_j)
  \end{align*}
  for non-negative coefficients $\eta_{ij}$ summing to $1$.  The other
  inclusion is obvious.  \else \texttt{missing} \fi%
\end{proof}%
\fi
\begin{remark}
  More complicated  examples require the  use of a computer  to assist
  the computations.  One suitable option for this is the software tool
  \texttt{polymake}.   It can (among  other things)  transform between
  $V$-  and   $H$-representations  of  polytopes   (using  some  other
  software,  either  \texttt{cdd}   or  \texttt{lrs}).   It  uses  the
  homogenization of a polyhedron  for representation, so that a matrix
  of generators $B$ should  be given to \texttt{polymake} as $\1|B^t$,
  and   inequalities   $A\vx\le   \vb$   are  written   as   $\vb|-A$.
  \texttt{polymake}  can  also  compute  optimal solutions  of  linear
  programs.  See \url{http://www.polymake.de} for more information.
\end{remark}

We will  see in  the next  section that in  Linear Programming  we are
given  the exterior  description of  a polyhedron,  while  finding the
optimum requires to find (at least some of) the extreme points.  If we
are also given the $V$-description then linear programming is a linear
time algorithm (in  the input size, which can be  very large).  So the
complexity  of linear  programming is  to some  extend related  to the
complexity of finding an interior from the exterior description.

Given an  exterior description,  there is no  polynomial bound  on the
number of points  in an interior description, unless  the dimension is
fixed.    A  simple   example   for  this   behaviour   is  the   cube
\begin{align*}
  \mathrm{C}_n:=\{\vx\in\R^n\mid -1\le x_i\le 1,\; 1\le i\le n\}\,.
\end{align*}
$C_n$ needs $2n$ linear inequalities for its exterior description. For
the interior  description, it is the  convex hull of  the $2^n$ points
with coordinates $+1$ and $-1$. Conversely, the cross polytope 
\begin{align*}
  \mathrm{Cr}_n:=\{\vx\in\R^n\mid            \ve^t\vx\le            1,
  \ve\in\{+1,-1\}^n\}\,.
\end{align*}
needs $2^n$ inequalities, but it is the convex hull of the $2n$ points
$\pm\ve_i$, $1\le  i\le n$.  We study  this problem in  more detail in
Sections~\ref{cha:faces-polyhedra} and \ref{cha:compl-sizes-rati}

\begin{definition}
  The \emph{characteristic}\mdef{characteristic cone\\recession  cone}
  or \emph{recession cone} of   a convex set $P\subseteq\R^n$ is   the
  cone
  \begin{align*}
    \reccone(P):=\{\vy\in \R^n\mid \vx+\lambda \vy\in P \text{ for all
    } \vx\in P, \lambda\ge 0\}.
  \end{align*}
  The \emph{lineality space}\mdef{lineality space}  of a polyhedron is
  the linear subspace
  \begin{align*}
    \lineality(P)&:=\reccone(P)\cap (-\reccone(P))\\
    &\phantom{:}=\{\vy\in \R^n\mid \vx+\lambda \vy\in P  \text{ for all } x\in P,
    \lambda\in\R\}.
  \end{align*}

  A  polyhedron  $P$  is  \emph{pointed}\mdef{pointed  polyhedron}  if
  $\lineality(P)=\{0\}$.
\end{definition}
$\lineality(P)$  is  a  linear  subspace  of $\R^n$.   Let  $W$  be  a
complementary subspace of $\lineality(P)$ in $\R^n$. Then
\begin{align}\label{eq:decomposition-pointed-linear}
  P=\lineality(P)+(P\cap W)\;.
\end{align}
as a \textsc{Minkowski} sum of a  linear space and a convex set $P\cap
W$  whose  lineality space  is  $\lineality(P\cap  W)=\{0\}$.  So  any
polyhedron $P$ is  the \textsc{Minkowski} sum of a  linear space and a
pointed polyhedron. Note that this decomposition is different from the
one used in the \texttt{Minkowski-Weyl} Duality.
\begin{examplecont}[~\ref{ex:polyhedra} continued]
  Consider again the polyhedron $P_3$. We compute the lineality space $L$:
  \begin{align*}
    L&:=\lineality(P_3)=\lin\left(\scriptsize
      \left(\begin{smallmatrix}
          \phantom{-}1\\-1\\\phantom{-}0
      \end{smallmatrix}\right)\right)\\
  \intertext{if  we choose a transversal subspace}
  W&:=\left\{ \left(\scriptsize\begin{smallmatrix} x\\y\\z
        \end{smallmatrix}\right)\bigm| x=y \right\}
    \intertext{then}
    Q:=P\cap W&:=\conv\left(
    \left(\scriptsize\begin{smallmatrix}
        1\\1\\1
      \end{smallmatrix}\right),
        \left(\scriptsize\begin{smallmatrix}
            -1\\-1\\\phantom{-}1
        \end{smallmatrix}\right)\right)+\cone\left(
      \left(\scriptsize\begin{smallmatrix}
          1\\1\\10
        \end{smallmatrix}\right),
      \left(\scriptsize\begin{smallmatrix}
          -1\\\phantom{-}1\\10
        \end{smallmatrix}\right)\right)
  \end{align*}
  and $P$ splits as $P=L+Q$.
\end{examplecont}

\begin{proposition}
  Let $P=\{\vx\mid A\vx\le \vb\}=\conv(V)+\cone(Y)$ be a polyhedron.
  \begin{compactenum}
  \item     \label{item:reccone}     $\reccone(P)=\{\vy\mid    A\vy\le
    \0\}=\cone(Y)$.
  \item $\lineality(P)=\{\vy\mid A\vy=\0\}$.
  \item $P+\reccone(P)=P$.
  \item $P$ polytope if and only if $\reccone(P)=\{\0\}$.
  \end{compactenum}
\end{proposition}
\ifproof
\begin{proof}
\ifreleasedproof%
  \begin{compactenum}
  \item  Let $C:=\{\vx\mid  A\vx\le \0\}$.  If  $\vy\in\R^n$ satisfies
    $A\vy\le \0$ and  $\vx\in P$, then $A(\vx+\lambda\vy)=A\vx+A\vy\le
    \vb$, so $C\subseteq\reccone(P)$.

    Conversely,  assume   $\vy\in\reccone(P)$  and  there   is  a  row
    $\va_k^t$    of   $A$    such    that   $s:=\va_k^t\vy>0$.     Let
    $r:=\inf_{\vx\in             P}(b_k-\va_k^t\vx)$.             Then
    $b_k-\va_k^t(\vx+\vy)=r-s<r$.   This  is  a contradiction  to  the
    choice of $r$.

    Now   consider  the  second   equality.   Let   $Q:=\conv(V)$  and
    $D=\cone(Y)$.      Clearly     $D\subseteq\reccone(P)$.     Assume
    $\vy\in\reccone(P)$,  $\vy\not\in   D$.   By  the  \textsc{Farkas}
    Lemma, there  is a functional  $\vc$ such that $\vc^tY\le  0$, but
    $\vc^t\vy>0$.  Choose any  $\vp\in Q$. By assumption, $\vp+n\vy\in
    P$ for $n\in\N$, so there are $\vp_n\in Q$, $\vq_n\in D$ such that
    $\vp+n\vy=\vp_n+\vq_n$.  $Q$  is bounded,  so there is  a constant
    $M>0$ such that $\vc^t\vx\le M$  for all $\vx\in Q$. Apply $\vc^t$
    to the sequence $\vp+n\vy$ to obtain
    \begin{align*}
      \vc^t\vp+n\vc^t\vy=\vc^t\vp_n+\vc^t\vq_n.
    \end{align*}
    By construction, the right side  of this equation is bounded above
    for  all  $n$,   while  the  left  side  tends   to  infinity  for
    $n\rightarrow\infty$.     This    is    a     contradiction,    so
    $\reccone(P)\subseteq D$.
  \item Follows immediately from (\ref{item:reccone}).
  \item   ``$\subseteq$'':  $\vx\in   P,  \vy\in   \reccone(P)$,  then
    $\vx+\vy\in P$ by definition.

    ``$\supseteq$'': $\0\in\reccone(P)$.
  \item $P$ bounded if and only if $\reccone(P)=\{\0\}$.
  \end{compactenum}
\else
\texttt{missing}
\fi
\end{proof}
\fi


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "geometry_and_optimization"
%%% End: 
