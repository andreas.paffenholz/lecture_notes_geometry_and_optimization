
\chapter{Farkas-Lemma}

In  this section  we want  to prove  the \textsc{Farkas}  Lemma, which
sometimes  is also  referred to  as the  \emph{Fundamental  Theorem of
  Linear    Inequalities}\mdef{Fundamental     Theorem    of    Linear
  Inequalities\\alternative  theorem}.   It  is  of  the  type  of  an
\emph{alternative  theorem},  saying that  exactly  one  of two  given
options  is always  true.   The \textsc{Farkas}  Lemma  comes in  many
variants, and we will encounter several of them in the next chapters.

% \begin{figure}[htb]
%   \centering
%   \includegraphics[height=.13\textheight]{aff-hull}
%   \caption{Affine, conic, and convex hull of $X:=\{(5,-1), (2,1)\}$.
%     The linear span of $X$ is the whole plane.}
%   \label{fig:affcincinvhull}
% \end{figure}. 

We will  see that these  two definitions essentially describe the same
set of    cones.       One     way  to     prove    this   is      via
\textsc{Fourier-Motzkin}-Elimination\mdef{\textsc{Fourier-Motzkin}
  elimination}.  This is a tool to remove variables from inequalities
similar to what \textsc{Gau\ss}ian elimination does for equations.

We explain the idea with an example.
%%%%%%%%%%%%% Margin figure
\marginnote{\includegraphics[width=.25\textwidth]{aff-hull-flipped}
      \begin{minipage}{\marginparwidth}
\captionof{figure}{Affine, conic, and convex hull of $X:=\{(-1,5), (1,2)\}$.
     The linear span of $X$ is the whole plane.}
 \end{minipage}
 \label{fig:affcincinvhull}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{align}
  \begin{array}{rcrcr}
    2x&+&y&\le&4\\
    -5x&+&2y&\le&4\\
    3x&-&2y&\le&2\label{fmunred}\\
    &&-y&\le& 2\\
    2x&&&\le&3
  \end{array}
\end{align}
For a  given $x$   we want  to   find conditions  that  guarantee  the
existence of  a $y$ such  that $(x,y)$ is a  solution.  We rewrite the
inequalities, solving   for $y$. The  first   two conditions impose an
upper bound on $y$,
\begin{alignat*}{7}
  &&&&y\;&\le\;&4\;&-\;&2x\\
  &&&&y&\le&2&+&\frac52x\\
  \intertext{the third and forth a lower bound,}
  -1\;&+\;&\frac32x\;&\le&y\\
  -2&&&\le&y \intertext{while the last one does not affect $y$:}%
  &&&&2x&\le&3
\end{alignat*}

Hence, for a given $x$, the system~(\ref{fmunred})  has a solution for
$y$ if and only if
\begin{align}
  \max(-2,-1+\frac32x)\le                                         y\le
  \min(4-2x,2+\frac52x)\,.\label{eq:solfory}
\end{align}
that is, if and only if the following system of inequalities holds.
\begin{align*}
  \begin{array}{rcrcrcrcrcr}
    -2&&&\le&4&-&2x\\
    -2&&&\le&2&+&\frac52x\\
    -1&+&\frac32x&\le&4&-&2x\\
    -1&+&\frac32x&\le&2&+&\frac52x
  \end{array}
\end{align*}
Rewriting  this in standard form, and  adding back the constraint that
does not involve $y$, we obtain
\begin{align}
  \begin{array}{rcrcr}
    x&\le&3\\
    -\frac52x&\le&4\\
    \frac72x&\le&5\\
    -x&\le &3\label{fmred}\\
    2x&\le&3
  \end{array}
\end{align}
This  system \eqref{fmred} of inequalities has  a solution if and only
if the  system \eqref{fmunred} has  a  solution.  However, it  has one
variable less. We can now iterate to obtain
\begin{align}
  \max(-\frac85,     -3)\ \le\     x\   \le    \   \min(3,    \frac72,
  \frac32)\,,\label{eq:solforx}
\end{align}
which has a solution if and only if
\begin{align*}
  \begin{array}{rcrrcrl}
    -\frac85&\le& 3&\qquad\qquad-3&\le& 3\\
  -\frac85&\le& \frac72&-3&\le& \frac72\\
  -\frac85&\le& \frac32&-3&\le& \frac32&\,.
\end{array}
\end{align*}
This  is easily  seen to  be a satisfiable   set of inequalities.   So
(\ref{fmunred})  does  have a solution.   (\ref{eq:solforx}) tells us,
that any $x$ between $-\nicefrac85$  and $\nicefrac32$ is good. If  we
have fixed some $x$, we can  plug it into (\ref{eq:solfory}) to obtain
a range of solutions for $y$.

Hence, by iteratively applying this elimination  procedure to a system
of linear inequalities we obtain a method  to check whether the system
has a   solution  (and we   can  even  compute one,  by  resubstituting
solutions).  Now let us do this in general.
\begin{theorem}[\textsc{Fourier-Motzkin}-Elimination]
  Let    $Ax\le b$ be a system    of  linear inequalities with $n\ge1$
  variables and $m$ inequalities. 

  Then there is a system $A'x'\le b'$ with $n-1$ variables and at most
  $\max(m,\frac{m^2}4)$ inequalities, such that  $s'$ is a solution of
  $A'x'\le  b'$  if  and only   if  there  is  $s_0\in  \R$  such that
  $s:=(s_0,s')$ is a solution of $Ax\le b$.
\end{theorem}
\ifproof
\begin{proof}
\ifreleasedproof%
  We classify the  inequalities depending on the coefficient  $a_{i0}$
  of $x_0$.   Let $U$ be  the indices of inequalities with $a_{i0}>0$,
  $E$ those with    $a_{i0}=0$ and  $L$ the  rest.    We multiply  all
  inequalities in $L$ and $U$ by $\frac{1}{|a_{i0}|}$.

  Now we  can eliminate $x_0$ by  adding any pair of inequalities from
  $L$ and $U$.   That is, our new system  of  inequalities consists of
  the inequalities
  \begin{align}
    {a'_j}^tx'+{a'_k}^tx'& \le b_j+b_k&&
    \text{for } j\in L, k\in U \label{combinedfmineq}\\
    {a'_l}^tx'&\le b_l&&\text{for } l\in E.\notag
  \end{align}
  Any solution $x$ of the original system yields a solution of the new
  system by  just forgetting  the  first coordinate.   We have at most
  $|L|\cdot|U|+|E|$ many inequalities, which proves the bound.

  Now  assume,   our   new    system  has  a  solution     $x'$.  Then
  \eqref{combinedfmineq} implies
  \begin{align}
    {a'_k}^tx'-b_k&\le b_j-{a'_j}^tx'&&\text{for    all }j\in  L, k\in
    U\notag%
    \intertext{which implies} %
    \max_{k\in       L}({a'_k}^tx'-b_k)&\le\min_{j\in     U}(\le   b_j
    -{a'_j}^tx') \label{minmaxfm}%
    \intertext{and if we take for $x_0$ any value in between, then}
    x_0+{a'_k}^tx'&\le b_k&&\text{for all }k\in U\notag\\
    -x_0+{a'_j}^tx'&\le b_j&&\text{for all }j\in L\notag
  \end{align}
  For all inequalities in  $E$,  the coefficient  of $x_0$  is  $0$,
  hence, they  are also satisfied by  $x=(x_0,x')$ and we have found a
  solution of $Ax\le b$.
%
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{remark}
  \begin{compactenum}
  \item If $A, b$ are rational, then so are $A', b'$.
  \item Any inequality  in the new system  is  a conic combination  of
    inequalities of the original system. This implies  that there is a
    matrix $U\ge 0$ such that $A'=UA$ and $b'=Ub$.
%%%%%%%%%%%%% Margin figure
    \marginnote{\includegraphics[width=.25\textwidth]{fourier-motzkin_flipped}    
      \begin{minipage}{\marginparwidth}
      \captionof{figure}{An example of \textsc{Fourier-Motzkin} projection}        
      \end{minipage}
  \label{fig:fouriermotzkin}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \item $U$ or $L$ may be empty. In this case only the inequalities in
    the     set $E$ survive.         More specifically, suppose   that
    $L=\varnothing$.  Then, given any solution  $x'$ of the  projected
    system, we can choose
    \begin{align*}
      x_0:=\min_{i:a_{i0}>0}\frac{b_i-\sum_{k=1}^na_{ik}x_k}{a_{i0}}
    \end{align*}
    and obtain a   solution   to the  original  system.   Further,  if
    $a_{i0}>0$ for all $i$, then the  projected system is empty, hence
    any point $x'\in\R^{n-1}$ lifts to a solution of $Ax\le b$.
  \end{compactenum}
\end{remark}
% \begin{figure}[htb]
%   \centering
%   \includegraphics[height=.15\textheight]{fourier-motzkin}
%   \caption{Geometry of \textsc{Fourier-Motzkin} elimination.}
%   \label{fig:fouriermotzkin}
% \end{figure}
\begin{example}
%   Geometrically, the solution set of the new system is a projection of
%   the old system. Consider e.g.\
%   \begin{align*}
%     -x+\phantom{2}y&\le-2\\
%     \phantom{-}x+2y&\ge-3
%   \end{align*}
%   The projections  of this  set  are $x\ge   1$  and  $y\ge -1$,   see
%   Figure~\ref{fig:fouriermotzkin}.
  Geometrically, the solution set of the new system is a projection of
  the old system. Consider e.g.\
  \begin{align*}
        \phantom{2}x-y&\le-2\\
        2x+y&\ge-3
  \end{align*}
  The projections  of this  set  are $x\ge   1$  and  $y\ge -1$,   see
  Figure~\ref{fig:fouriermotzkin}.
\end{example}
Now that we have studied our new tool, let us get back to cones.
\begin{theorem}[\textsc{Weyl}'s Theorem]\mdef{\textsc{Weyl}'s Theorem}
  If a non-empty cone   $C$ is finitely   generated, then it   is also
  polyhedral.
\end{theorem}
\ifproof
\begin{proof}
\ifreleasedproof
  Let $C=\{\lambda^tA\mid \lambda\ge 0\}$ be a finitely generated cone
  with a matrix of row vectors $A\in \R^{r\times n}$. Then
    \begin{alignat*}{4}
      C&=\{x\in\R^n&&\mid\exists\,\lambda\in\R^n\,:\; 
      x^t=\lambda^t A, \lambda\ge 0\}\\
      &=\{x\in \R^n&&\mid\exists\,\lambda\in\R^n\,:\; 
      x^t-\lambda^t A\le 0,\;
      -x^t+\lambda^t A\le 0,\;
      -\lambda\le 0
      \}
    \end{alignat*}
    This set is the projection onto the first $n$ coordinates of the set
    \begin{align}
      C':=\{(x,\lambda)\in\R^{n+m}\mid   x^t-\lambda^t   A\le     0,\;
      -x^t+\lambda^t A\le 0,\; -\lambda\le 0 \}\;.\label{weyl}
    \end{align}
    By \textsc{Fourier-Motzkin} elimination we can write this as 
    \begin{align*}
      C&=\{x\in \R^n\;\mid Ax\le 0\}
    \end{align*}
    where    the       system $Ax\le       0$   is     obtained    via
    \textsc{Fourier-Motzkin}-Elimination  by eliminating the variables
    $\lambda_1,  \ldots,  \lambda_r$ from the  system in~(\ref{weyl}).
    \else \texttt{missing} \fi
\end{proof}
\fi
Using this, we  can now prove a first  variant of the  \textsc{Farkas}
Lemma.
\begin{theorem}[\textsc{Farkas} Lemma, Geometric Version]\mdef{\textsc{Farkas} Lemma}
  For any matrix $A\in\R^{m\times n}$   and vector $b\in\R^m$  exactly
  one of the following holds:
  \begin{compactenum}
  \item the is $y\in\R^m$ such that $y^tA=b$, $y\ge 0$, or
  \item there is $x\in \R^n$ such that $Ax\le 0$ and $b^tx>0$.
  \end{compactenum}
\end{theorem}
Geometrically, this theorem means   the following.  Given a  cone  $C$
generated by the rows of $A$ and some vector $b$,
\begin{compactenum}
\item either $b$ is in  the cone, in which  case there are non-negative
  coefficients that give a representation of $b$ using the rows of $A$,
\item or $b$  is not in the  cone  $C$, in which   case we can find  a
  hyperplane $H_y$ given  by its normal $y$,  such that $b$ ad $C$ are
  on different sides of $H_y$.
\end{compactenum}
\ifproof
\begin{proof}[Proof of the \textsc{Farkas} Lemma]
  \ifreleasedproof %
  The  two statements cannot hold    simultaneously: Assume there   is
  $y\ge0$ s.t.\ $y^tA=b$ and $x$ s.t.\ $Ax\le 0$ and $b^tx>0$. Then
  \begin{align*}
    0<b^tx=(y^tA)x=y^t(Ax)\le 0.
  \end{align*}
  
  Let
  \begin{align}
    C:=\{y^tA\mid y\ge 0\}\,.\label{eq:cone1}
  \end{align}
  Then there  is  $y\ge 0$,  $y^tA=b$  if and  only  if $b\in C$.   By
  \textsc{Weyl}'s  Theorem the cone $C$  is polyhedral  and there is a
  matrix $M$ such that
  \begin{align}
    C=\{x\mid Mx\le 0\}\,.\label{eq:cone2}
  \end{align}

  $b\not\in C$ holds if and only  if there is a  row vector $x$ of the
  matrix   $M$ (interpreted as  a column  vector)  such that $b^tx>0$.
  Using the unit vectors for $y$ we can deduce from the representation
  in (\ref{eq:cone1}) that each row of $A$ is in $C$.  Hence, from the
  representation of $C$ as in (\ref{eq:cone2}) we obtain $Ax\le 0$. So
  $x$ is as desired. \else \texttt{missing} \fi
\end{proof}
\fi

\begin{definition}
  The  \emph{polar  (dual)}\mdef{polar  dual   of  a cone} of  a  cone
  $C\subset \R^n$ is the set
  \begin{align*}
    C^*:=\{z\in\R^n\mid x^tz\le 0\;\text{ for all }\;x\in C\}.
  \end{align*}
\end{definition}
%%%%%%%%%%%%% Margin figure
    \marginnote{\includegraphics[width=.25\textwidth]{dual_cone}    
      \begin{minipage}{.25\textwidth}
      \captionof{figure}{A cone and its dual}        
      \end{minipage}
  \label{fig:dualcone}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}\label{prop:coneprops}
  We have the following facts about cones $C,D$ in $\R^n$:%
  \begin{compactenum}
  \item $C\subseteq D$ implies $D^*\subseteq C^*$
  \item $C\subseteq C^{**}$
  \item $C^*=C^{***}$
  \item\label{prop:item:finite} If $C=\{\lambda^tA\mid \lambda\ge 0\}$
    then $C^*=\{x\mid Ax\le 0\}$.
\item\label{prop:item:double} if $C=\{x\mid Ax\le 0\}$, then $C=C^{**}$.
  \end{compactenum}
\end{proposition}
\ifproof
\begin{proof}
\ifreleasedproof
  \begin{compactenum}
  \item $y\in D^*$ means $x^ty\le 0$ for all $x\in D\subseteq C$.
  \item $x\in C$ means $x^ty\le 0$ for all $y\in C^*$.
  \item $y\in C^{***}$ means $x^ty\le  0$ for all $x\in C^{**}$, which
    means $y\in C^*$.
  \item Clearly,   the inequalities are necessary.   Let   $y\in C$ be
    $y=\sum\lambda_i a_i$. Then $y^tx=\sum\lambda_ia_i^tx\le 0$.
  \item  Let  $D:=\{\lambda^tA\mid \lambda\ge  0\}$.  By $(4)$ $C=D^*$
    and by $(3)$: $C^{**}=D^{***}=D^*=C$.  
  \end{compactenum}
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{lemma}
  \begin{align*}
    (1)\;\; C=\{\lambda^t A\mid \lambda\ge 0\}&\ \Rightarrow\ 
    C^{**}=C&
    (2)\;\;C=\{X\mid Nx\le 0\}&\ \Rightarrow\ 
    C^*=\{\lambda^t N\mid \lambda\ge 0\}\,.
  \end{align*}
\end{lemma}
\ifproof
\begin{proof}
\ifreleasedproof
  \begin{compactenum}
  \item  Let  $C=\{\lambda A\mid \lambda\ge   0\}$.  Then $C^*=\{x\mid
    Ax\le 0\}$. We need only prove $C^{**}\subseteq C$.
    
    If $b\not\in C$, then, by  the \textsc{Farkas} Lemma, there is $x$
    such that $Ax\le  0$, $b^tx>0$, i.e.\  $x\in C^*$ and hence $b\not
    \in C^{**}$.
  \item $C$ is the dual of $D:=\{\lambda^t N\mid \lambda\ge0\}$, i.e.\
    $C=D^*$.  By dualizing     again  and   using   $(1)$   we    have
    $C^*=D^{**}=D$.
  \end{compactenum}
\else
\texttt{missing}
\fi
\end{proof}
\fi

\begin{theorem}[\textsc{Minkowski}'s Theorem]\mdef{\textsc{Minkowski}'s Theorem}
  A polyhedral cone is non-empty and finitely generated.
\end{theorem}
\ifproof
\begin{proof}
\ifreleasedproof
  Let $C=\{x\mid Nx\le 0\}$. Then $0\in C$ and $C$ is not empty. 
  
  Let  $D:=\{\lambda^tN\mid   \lambda\ge  0\}$.   By   \textsc{Weyl}'s
  Theorem, $D$ is polyhedral. Hence $D^*$ is  finitely generated.  But
  $D^*=C$, and so $C$ is finitely generated.
\else
\texttt{missing}
\fi
\end{proof}
\fi

Combining  this    with     \textsc{Weyl}'s Theorem    we   get    the
\emph{\textsc{Weyl-Minkowski}-Duality}\mdef{\textsc{Weyl-Minkowski}-Duality}
for cones.
\begin{theorem}
  A cone is polyhedral if and only if it is finitely generated.\mqed
\end{theorem}

Here  are   some variants  of the   \textsc{Farkas}  lemma that follow
directly from the above geometric version.
\begin{proposition}\label{prop:farkas-variants}
  Let $A\in \R^{m\times n}$ and $b\in \R^n$. 
  \begin{compactenum}
  \item Either $Ax=b$,  $x\ge 0$ has a  solution or there is $y$  such
    that $y^tA\le 0$ and $y^tb>0$, but not both.
  \item Either $Ax\le b$ has  a solution,  or there  is $y\le 0$  such
    that $y^tA=0$ and $y^tb>0$, but not both.
  \item Either $Ax\le b$, $x\ge 0$ has a solution or there is $y\le 0$
    such that $y^tA\le 0$ and $y^tb>0$, but not both.\mqed
  \end{compactenum}
\end{proposition}

We    want    to finish  this    chapter    with  an  application   of
\textsc{Fourier-Motzkin}   elimination        to   linear programming.
\textsc{Fourier-Motzkin} elimination allows us to 
\begin{compactenum}
\item decide  whether a linear  program is feasible, and
\item determine an optimal solution.
\end{compactenum}
Let a
linear program
\begin{align*}
  \text{maximize}\qquad c^tx&&\text{subject to}\qquad Ax\le b
\end{align*}
be given, with $A\in\R^{m\times n}$, $b\in\R^m$, and $c\in\R^n$.

I f we  apply \textsc{Fourier-Motzkin}  elimination $n$-times to   the
system $Ax\le b$, then no variable is left and we have inequalities of
the form
\begin{align}
  0\le \alpha_j\label{fm-linear1}
\end{align}
for  right  hand    sides   $\alpha_1, \ldots,  \alpha_k$.    By   the
\textsc{Fourier-Motzkin} theorem, the   system has a solution  if  and
only if all inequalities in  (\ref{fm-linear1}) have a solution, i.e.\
if all $\alpha_j$ are non-negative.

To  obtain the optimal value, we  add an additional variable $x_{n+1}$
and extend our system as follows
\begin{align*}
  B:=\left(
    \begin{array}{rr}
      A&0\\-c^t&1
    \end{array}\right)\qquad d:=\left(
    \begin{array}{r}
      b\\0
    \end{array}\right)\;.
\end{align*}
In  the  system  $B(x,x_{n+1})\le  d$ we     eliminate the first   $n$
variables, and we  obtain upper and lower  bounds on $x_{n+1}$ in  the
form
\begin{align*}
  \alpha_j\le x_{n+1}\le \beta_j
\end{align*}
The  minimum over the $\beta_j$  is our optimal  value, as this is the
largest possible value such that there is $x\in\R^n$ with
\begin{align*}
  -c^tx+x_{n+1}\le 0\qquad\Longleftrightarrow\qquad x_{n+1}\le c^tx\;.
\end{align*}
If we need  the minimum over $c^tx$ then   we add the row $(c^t,  -1)$
instead.

Observe however, that this procedure is far from practical, the number
of  inequalities may grow   exponentially in the  number of eliminated
variables (can you find an example for this?).


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 
