\chapter{Complexity of Rational Polyhedra}
\label{cha:compl-sizes-rati}

In this  chapter we  want to  look  at algorithms, their  running time
behavior, at the complexity of  the interior and exterior description
of polyhedra,   and the complexity  of solutions   of linear programs.
Here,  we have to give  up the choice of  $\R$  as our ground field, as
numbers in $\R$  do not have a  finite coding length. The usual choice
here  is the  field $\Q$  of rational  numbers.   So from now  on, all
numbers, vectors and matrices have entries from $\Q$. Clearly, if
\begin{align*}
  P=\{\vx\mid A\vx\le \vb\}=\conv(X)+\cone(Y)+\lin(Z)\,,
\end{align*}
then
\begin{align*}
  \text{$A$, $\vb$ rational}&&\Longrightarrow&&&\text{$X, Y, Z$
    can chosen to be rational}\\
  \text{$X,  Y,  Z$  rational}&&\Longrightarrow&&&\text{$A,  \vb$  can
    chosen to be rational}\,.
\end{align*}
The most successful  distinction in complexity over the  last 50 years
has been between problems that can be solved in polynomial time in the
size  of the  input, and  those  for which  such an  algorithm is  not
known. The  first type of  problems is subsumed  in the class  $\P$ of
polynomially  solvable  problems.   Often,  one  sets  these  problems
against the so called $\NP$-complete problems.

Clearly, except for very simple problems, the \textit{running time} of
an algorithm will depend on the size of the  input.  So we have to fix
some measure for the input.  Implementations on a computer usually use
the  binary encoding, which  we  will  choose in  the following.
\begin{definition}
  The  \emph{size}\mdef{size\\coding length}  or  \emph{coding length}
  $\size s$ of an object $s$ is the minimum length of a $0$-$1$-string
  that represents it.
\end{definition}

\begin{table}[htb]
  \centering
\renewcommand{\arraystretch}{2.2}
\begin{tabular}{l|l}
  \toprule
  $z\in\Z$&$\size{z}=1+\lceil\log_2(|z|+1)\rceil$\\
  $r\in\Q$&
\begin{minipage}[t]{.6\linewidth}
  $r=\frac  ab$,   $a,b\in\Z$,   $b\ge 0$    with  $\gcd(a,b)=1$,  then\\
  $\size{r}=1+\lceil\log_2(|a|+1)\rceil+\lceil\log_2(|b|+1)\rceil$
\end{minipage}\\
$\vv\in\Q^n$&$\size{\vv}=\sum\size{\vv_i}$\\
$A\in\Q^{m\times n}$&$\size{A}=\sum\size{a_{ij}}$.\\
\bottomrule
\end{tabular}
\renewcommand{\arraystretch}{1}
  \caption{A list    of  some    common    objects  and   their  sizes}
  \label{tab:commonsizes}
\end{table}
There are many other common  choices for such a measure. However, most
of them are ``polynomially  equivalent'', so changing the measure does
not change ``polynomial  solvability'' of a problem, which  is what we
are interested  in (the notable  exception of this is  the \emph{unary
  encoding}\mdef{unary encoding}  of numbers).  A list  of some common
objects    and   their    sizes    that   we    will    use   is    in
Table~\ref{tab:commonsizes}.

Now that we can measure the size  of our input data, we can define the
complexity of  solving a \textit{problem}.   Usually one distinguishes
between two different types of problems.
\begin{definition}
  \begin{compactenum}
  \item  In \emph{a decision  problem}\mdef{decision problem\\feasible
      solution\\instance\\\textit{yes}-instance\\\textit{no}-instance\\optimization
      problem\\objective  function\\minimization problem\\maximization
      problem}  $\Delta$  we  have   a  set  $S_I$  of  \emph{feasible
      solutions} for every \emph{instance} $I\in\Delta$ of the problem
    $\Delta$.   We have to  decide whether  $S_I$ contains  a solution
    that  satisfies given properties.   If there  is such  a solution,
    then   $I$   is   a  \emph{\textit{yes}-instance},   otherwise   a
    \emph{\textit{no}-instance}.

  \item  In an \emph{optimization problem}  $\Pi$, we again have a set
    $S_I$   of  \emph{feasible  solutions}  for  every \emph{instance}
    $I\in\Pi$   of  the problem $\Pi$,  but   additionally we  have an
    \emph{objective function}  $c_I: S_I\longrightarrow \Q$ associated
    with the instance.   The task is to  find  an optimal  solution in
    $S_I$ with respect  to this  function, so  either a  solution $\ov
    x\in  S_I$  with $c_I(\ov  x)\le c_I(x)$ for   all $x\in S_I$ in a
    \emph{minimization problem},   or $c_I(\ov x)\ge  c_I(x)$  for all
    $x\in S_I$ in a \emph{maximization problem}.
  \end{compactenum}
\end{definition}
\begin{example}
  Examples of decision problems are
  \begin{compactenum}
  \item  Does a  system of  linear inequalities  $A\vx\le \vb$  have a
    solution?
  \item Does a graph have a Hamiltonian cycle?

    (A \emph{Hamiltonian  cycle} is a  cycle in the graph  that visits
    each vertex exactly once)
  \end{compactenum}
  Examples for an optimization problem are
  \begin{compactenum}
  \item Find  an optimal solution of  $\max(\vc^t\vx\mid A\vx\le \vb)$
    or determine that the system is infeasible.
  \item Find a shortest path between two nodes in a graph.
  \end{compactenum}
\end{example}
An  \emph{algorithm}\mdef{algorithm}  $\Alg$   is  a  finite  list  of
instructions   that   perform  operations   on   some  data.    $\Alg$
\textit{solves} a  problem $\Lambda$  if, for any  given input  $y$ of
$I$, it  determines in finite  time an \textit{output} $z\in  S_I$, or
stops without an output, if there is no solution.

An  important  characteristic of  an  algorithm  is its  \emph{running
  time}\mdef{running  time}.  This  should clearly  not depend  on the
specific hard- or software used, so we need an intrinsic measure.
\begin{definition}
  The \emph{running  time function}\mdef{running time  function} of an
  algorithm   $\Alg$   for  a   problem   $\Lambda$   is  a   function
  $f_\Alg:\N\longrightarrow\N$ such that
  \begin{align*}
    f_\Alg(s)=\max_{y  \text{  with  }\size{y}\le  s}(\text{number  of
      elementary operations performed by $\Alg$ on the input $y$})
  \end{align*}
  \emph{Elementary operations}\mdef{elementary operation} are
  \begin{center}
    addition,   subtraction, multiplication, division, comparison, and
    assignment.
\end{center}
An    algorithm    is    said    to   be    a    \emph{polynomial-time
  algorithm}\mdef{polynomial  (time)}, or  just  \emph{polynomial}, if
its running time function  $f$ satisfies $f=\bigo(g)$ for a polynomial
$g$.      A     problem     $\Lambda$     is     \emph{polynomial-time
  solvable}\mdef{polynomial-time  solvable} if  there is  a polynomial
algorithm that solves it.
\end{definition}
The notation $\bigo$ for the estimate  of $f$ in the definition is one
of the \emph{\textsc{Landau}-symbols}\mdef{\textsc{Landau}-symbol}:
\begin{align*}
  f&=\bigo(g)\qquad   \exists  C\in R,\,\exists n_0\in\N:\quad f(n)\le
  Cg(n)\; \text{ for all } n\ge n_0\,,
\end{align*}
which is useful  here  as we are   only interested  in  the asymptotic
dependence of the running time, so we want to neglect coefficients and
lower order terms.

In the definition   of an  algorithm we  assumed  that the  elementary
operations can  be done in  constant (unit) time.   This  clearly is a
simplification, as the  time   needed for the multiplication   of  two
numbers   does depend on   their size,  but it is   a polynomial  time
operation, so this choice does not affect polynomial-time solvability.

Decision  problems  are   categorized  into  several  \emph{complexity
  classes}\mdef{complexity class}  with respect to  their (asymptotic)
running time.
\begin{definition}
  \begin{compactenum}
  \item $\P$  is the set  of  all decision  problems $\Delta$ that are
    polynomial-time solvable.
  \item $\NP$ is the collection of all decision problems $\Delta$ that
    have  an  associated  problem  $\Delta'\in\P$ such  that  for  any
    \textit{yes}-instance     $I\in     \Delta$     there     is     a
    \emph{certificate}\mdef{certificate} $C(I)$  of size polynomial in
    the size of  $I$ such that $(I, C(I))$  is a \textit{yes}-instance
    of $\Delta'$.
  \item $\coNP$ is the collection of all problems that have a
    polynomial-time checkable certificate for any
    \textit{no}-instance.
  \end{compactenum}
\end{definition}
An example of  an $\NP$-problem is the question  whether a given graph
is Hamiltonian.   A   positive answer  can be  certified  by  giving a
Hamiltonian cycle, and   the correctness of  this  can  be checked  in
polynomial time.   Note, that there  is  no known certificate  for the
opposite answer whether a  given graph is \textit{not} Hamiltonian, so
it is unknown whether the problem is in $\coNP$.

Problems in $\NP\cap \coNP$ are those  for which both a positive and a
negative answer have  a certificate that can be  checked in polynomial
time.         Such        problems        are       also        called
\emph{well-characterized}\mdef{well-characterized}.  We  will see some
examples  later.   Clearly,  we  have $\P\subseteq  \NP\cap  \coNP\,$.
There are  only very few problems  known that are  in $\NP\cap \coNP$,
but not  known to be  in $\P$.  One  of the most famous  and important
questions in complexity theory is whether
\begin{align*}
  \P\ne \NP\,.
\end{align*}
It is widely  believed that the class $\NP$ is  much larger than $\P$.
However, there is  no proof known.  Any answer,  positive or negative,
to this would have far reaching consequences:
\begin{compactenum}
\item If $\P=\NP$, then it may  either imply a  completely new type of
  polynomial-time algorithm,  or  it  may prove   that the concept  of
  \textit{polynomial-time}  does not serve  as a good characterization
  for algorithms.
\item  If $\P\ne\NP$,  then  this  will  likely give  insight why some
  problems seem to be so much harder than others.
\end{compactenum}

\begin{definition}
  A   problem   $\Delta$  is   said   to  be   \emph{(polynomial-time)
    reducible}\mdef{(polynomial-time)   reducible}    to   a   problem
  $\Delta'$ if there is  a polynomial-time algorithm that returns, for
  any instance $I\in \Delta$ an instance $I'\in\Delta'$ such that
  \begin{align*}
    \text{$I$ is a \textit{yes}-instance}\quad\Longleftrightarrow\quad
    \text{$I'$ is a \textit{yes}-instance}\,.
  \end{align*}
\end{definition}
This implies, that if $\Delta$ is reducible to $\Delta'$ and
\begin{compactenum}
\item $\Delta'\in\P$, then also $\Delta\in \P$.
\item $\Delta\in\NP$, then also $\Delta'\in \NP$.
\end{compactenum}
A  problem $\Delta$  is  \emph{$\NP$-complete}\mdef{$\NP$-complete} if
any  other problem  $\Delta'\in\NP$ can  be reduced  to  $\Delta$.  If
$\Delta$ is  reducible to  $\Delta'$ and $\Delta'$  is $\NP$-complete,
then so  is $\Delta$.  $\NP$-complete  problems are in some  sense the
hardest  $\NP$-problems.   There   in  fact  do  exist  $\NP$-complete
problems,  e.g.\  the  \textsc{Traveling  Salesman Problem  (TSP)}  or
integer linear programming.

Let $\Pi$  be an optimization  problem.  We  can associate a  decision
problem $\Delta$ to $\Pi$ as follows:
\begin{align*}
  \text{Given $r\in \Q$ and an instance  $I$, is there $z\in S_I$ with
    $c_I(z)\ge r$\,?}
\end{align*}
An  optimization problem $\Pi$  is \emph{$\NP$-hard}\mdef{$\NP$-hard},
if the associated decision  problem is $\NP$-complete.  Now we analyze
the complexity of linear systems of equations and inequalities.
\begin{lemma}\label{lemma:sizeofdet}
  Let $A\in \Q^{n\times n}$ of size $s$.  Then $\size{\det(s)}\le 2s$.
\end{lemma}
\ifproof%
\begin{proof}%
  \ifreleasedproof    Let         $A=(\frac{p_{ij}}{q_{ij}})_{ij}$     and
  $\det(A)=\frac{p}{q}$    for   completely    reduced       fractions
  $\frac{p_{ij}}{q_{ij}}$,     $\frac{p}{q}$  and $q_{ij},q>0$. We can
  assume that $n\ge 2$ as otherwise $\det(A)=A$ and
  \begin{align*}
    \sum \size{p_{ij}}\ge n\,,
  \end{align*}
  as otherwise $\det(A)=0$.  Clearly, we have
  \begin{align*}
    q+1&\le  \prod_{i,j=1}^n(q_{ij}+1)  \le 2^{\sum  \log_2(q_{ij}+1)}
    \le 2^{\sum \lceil\log_2(q_{ij}+1)\rceil} \le 2^{s-n}
  \end{align*}
  By the definition of the determinant via the Laplace formula
  \begin{align*}
    |\det(A)|&\le \prod_{i,j=1}^n(|p_{ij}|+1)
  \end{align*}
  Further we have that
  \begin{align*}
    |p|+1=|\det(A)|\; q+1&\le \prod_{i,j=1}^n(|p_{ij}|+1)\,(q_{ij}+1)
    \le   2^{\sum     \log_2(|p_{ij}|+1)+\log_2(q_{ij}+1)}\\
    &  \le 2^{\sum \lceil\log_2(|p_{ij}|+1)+\log_2(q_{ij}+1)\rceil}\le
    2^s-n^2 \;.
  \end{align*}
  Combining         this    gives\        \    $\size{\det(A)}      =1
  +\lceil\log_2(|p|+1)\rceil  +\lceil\log_2(q+1)\rceil <2s$.     \else
  \texttt{missing} \fi
\end{proof}%
\fi
\begin{corollary}\label{cor:sizeoflineareq}
  Let $A\in \Q^{m\times n}$, $\vb\in\Q^m$, and assume that the rows of
  the  matrix $(A|\vb)$  have size  $\le  \phi$. If  $A\vx=\vb$ has  a
  solution, then it has one of size at most $4n^2\phi$.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof We may assume that $\rank A=m$. Then we can reorder
  the columns of $A$ such that $\ov A:=A_{*[m]}$ is non-singular.

  Then $x:=(\ov \vx,0)$ for $\ov  \vx:=\ov A^{-1}\vb$ is a solution of
  the system, and by \textsc{Cramer}'s  rule, the entries of $\ov \vx$
  are
  \begin{align*}
    \ov x_i=\frac{\det(\ov A_j)}{\det(\ov A)}\,.
  \end{align*}
  where  $\ov A_j$  is the  matrix  obtained by  replacing the  $j$-th
  column of  $\ov A$ by  $\vb$. Now the  size of these matrices  is at
  most $m\phi$, so their determinants  have size at most $2m\phi$, and
  hence,
  \begin{align*}
    \size{\ov x_i}\le 4m\phi.
  \end{align*}
  So $\size{\vx}\le 4mn\phi\le 4n^2\phi$.  \else \texttt{missing} \fi%
\end{proof}%
\fi

This  implies  that the  problem  $\Lambda_1$  of  deciding whether  a
rational  system of  linear  equations $A\vx=\vb$  has  a solution  is
well-characterized:
\begin{compactenum}
\item  If  $A\vx=\vb$  has  a  solution,  then  it  has  one  of  size
  polynomially bounded by  the sizes of $A$ and  $\vb$, and given such
  an  $\vx$, we  can  check  in polynomial  time  that $A\vx=\vb$.  So
  $\Lambda_1\in\NP$.
\item If  $A\vx=\vb$ has  no solution, then  there is $\vy$  such that
  $\vy^tA=\0$ and  $\vy^t\vb\ne 0$.  Again,  we can assume  that $\vy$
  has size  polynomial in that of  $A$ and $\vb$,  and the correctness
  can be verified in polynomial time. So $\Lambda_1\in\coNP$.
\end{compactenum}
In  fact, this problem is  in  $\P$, as can  be  shown by proving that
\textsc{Gaussian} elimination is in $\P$.

\begin{lemma}
  If a  system of  linear inequalities $A\vx\le  \vb$ has  a solution,
  then it has one of size  polynomially bounded in the size of $A$ and
  $\vb$.
\end{lemma}
\ifproof%
\begin{proof}%
  \ifreleasedproof  Let  $A\in\Q^{m\times  n}$, $P:=\{\vx\mid  A\vx\le
  \vb\}$ and $I\subset[m]$  such that $\{\vx\mid A_{I*}\vx=\vb_I\}$ is
  a minimal  face of  $P$.  By Corollary  \ref{cor:sizeoflineareq}, it
  contains  a vector  of size  polynomially  bounded in  the sizes  of
  $A_{I*}$ and $\vb_I$. These are in  turn bounded by the sizes of $A$
  and $\vb$.  \else \texttt{missing} \fi%
\end{proof}%
\fi

This implies that the problem $\Lambda_2$ of deciding whether a system
of rational  linear inequalities  $A\vx\le \vb$ has  a solution  is in
$\NP$.  

By \textsc{Farkas} Lemma $A\vx\le \vb$  has no solution if and only if
there  is  a  vector  $\vy\in\Q^m$  such that  $\vy\ge  \0$,  $\vy^tA=\0$  and
$\vy^t\vb<0$. Again, we can choose  this vector to be polynomially bounded
in  the sizes  of  $A$  and $\vb$,  so  $\Lambda_2\in\coNP$.  In  total,
$\Lambda_2$  is   well-characterized.   This  implies   the  following
proposition.
\begin{proposition}
  Let $A\in     \Q^{m\times  n}$,   $\vb\in\Q^m$,   $\vc\in   \Q^n$,   and
  $\delta\in\Q$. Then the following problems are well-characterized:
  \begin{compactenum}
  \item Decide whether $A\vx\le \vb$ has a solution.
  \item Decide whether $A\vx=\vb$ has a nonnegative solution.
  \item  Decide   whether  $A\vx\le  \vb$,   $\vc^t\vx>\delta$  has  a
    solution.
  \item Decide whether $\max(\vc^t\vx\mid A\vx\le \vb)$ is infeasible,
    bounded feasible or unbounded feasible.
  \end{compactenum}
\end{proposition}
\ifproof%
\begin{proof}%
  \ifreleasedproof $(1)$    follows  directly  from   the above,   the
  arguments  for $(2)$ and  $(3)$ are similar, for  $(4)$ use the dual
  program and the duality theorem.
  \else \texttt{missing} \fi
\end{proof}%
\fi%
Further, it is easy to see that the problems $(1)$, $(2)$ and $(3)$ of
the previous  proposition  are  polynomially  equivalent,  which means
that, if one can  solve any of  the problems in polynomial  time, then
one can solve the others  in polynomial time. 

The  four   problems  are  in   fact  in  $\P$,  which   follows  from
\textsc{Kachiyan}'s  \textit{ellipsoid   method}  for  solving  linear
programs.  However,  this is currently not a  practical algorithm. The
simplex method, which we will meet  in the next chapter, is still both
numerically more stable and faster. However, the simplex method is not
a polynomial time algorithm.

\begin{definition}
  Let $P\subseteq\Q^n$ be a rational polyhedron.
  \begin{compactenum}
  \item The  \emph{facet complexity}\mdef{facet complexity}  of $P$ is
    the smallest number $\phi$ such  that $\phi\ge n$ and there exists
    a rational system $Ax\le b$ that describes $P$ and each inequality
    has size bounded by $\phi$.
  \item The \emph{vertex complexity}\mdef{vertex complexity} of $P$ is
    the smallest  number $\nu$ such  that $\nu\ge n$ and  there exists
    rational matrices $X$ and  $Y$ such that $P=\conv(X)+\cone(Y)$ and
    each row of $X$ and $Y$ has size bounded by $\nu$.
  \end{compactenum}
\end{definition}

\begin{proposition}
  Let $P\subset\Q^n$ be  a rational  polyhedron with facet  complexity
  $\phi$ and  vertex complexity $\nu$.  Then there is a constant $C>0$
  such that
  \begin{align*}
    \nu&\le Cn^2\phi& \phi&\le Cn^2\nu\,.
  \end{align*}
\end{proposition}
\ifproof%
\begin{proof}%
\ifreleasedproof
  \begin{compactenum}
  \item $\nu\le 4n^2\phi$: Let $P=\{Ax\le b\}$. The  claim is a direct
    consequence of the representation of $P$ as
    \begin{align*}
      P=\conv(X)+\cone(Y)+\lin(Z)\,.
    \end{align*}
    where all rows  of $B, C$ and $D$ are  solutions of some subsystem
    of        $A\vx\le        \vb$,        which        are,        by
    Corollary~\ref{cor:sizeoflineareq}, bounded by $4n^2\phi$.
  \item $\phi\le 4n^2\nu$:

    Assume $P=\conv(X)+\cone(Y)$  for   rational matrices $X$  and  $Y$
    whose rows have size at most $\nu$.

    Suppose   first that $\dim P  =  n$.  Then each   facet  of $P$ is
    determined by a linear  equation of the form  (where $\xi$ is  the
    variable)
    \begin{align*}
      \det\left[
      \begin{array}{lll}
        1& \mathbf \xi^t\\
        \mathbf 1_{|I|}& X_{I*}\\
        \mathbf 0_{|J|}& Y_{J*}\\
      \end{array}\right]\ =\ 0\,.
    \end{align*}
    Expanding by the first row we obtain
    \begin{align*}
      \sum_{i=1}^n(-1)^i\,(\det(D_i))\, \xi_i=-\det(D_0)\,,
    \end{align*}
    for the $(n\times n)$-sub-matrices  $D_i$ obtained by deleting the
    first  row  and   the  $i$-th column.   $D_0$  has   size at  most
    $2n(\nu+1)$, and each $D_i$ has  size at most $2n\nu$.  Therefore,
    the  equation and the  corresponding inequality for the facet have
    size at most $n\cdot(2n(\nu+1)+2n\nu\le 6n^2\nu$.

    So assume $\dim P < n$.  By adding  a subset of $\{0, e_1, \ldots,
    e_n\}$, where  $0$ is the zero  vector and  $e_j$ are the standard
    unit vectors, to  the polyhedron, one  can obtain a new polyhedron
    $Q$  with $\dim  Q=n$ such that   $P$ is  a  face  of  $Q$. By the
    previous  argument, this gives us $n-\dim  P$ equations of size at
    most $6n^2\nu$ defining the affine hull.

    Further, we can  choose $n-\dim(P)$ coordinate  directions so that
    the projection $P'$ is full-dimensional and affinely equivalent to
    $P'$.  $P'$ has vertex complexity at  most $\nu$.  By the previous
    argument, we may describe  $P'$ by linear  inequalities of size at
    most $6(n-\dim(P))^2\nu$.  Extend  these inequalities  by zeros to
    obtain inequalities valid for $P$.    Together with the  equations
    for  the  affine hull,  we  obtain  a description of  the required
    size.
  \end{compactenum}
\else
\texttt{missing}
\fi%
\end{proof}%
\fi

A direct  corollary of this theorem concerns  the size of solutions of
linear programs.
\begin{corollary}
  Let $A\in\Q^{m\times n}$, $\vb\in \Q^m$, $\vc^t\in\Q^n$. Assume that
  each entry  of $A, \vb$, and $\vc$  is bounded by $s$,  and that the
  optima of
  \begin{align*}
    \delta:=\max(\vc^t\vx\mid        A\vx\le        \vb,        \vx\ge
    \0)=\min(\vy^t\vb\mid \vy^tA\ge \vc, \vy\ge \0)
  \end{align*}
  are finite.   Then $\size{\delta}$  is bounded  by $n$  and $s$, and
  both  the primal and  the dual  program have an  optimal solution of
  size bounded by $n$ and $s$.
\end{corollary}
\ifproof%
\begin{proof}%
  \ifreleasedproof%
  Each  linear  inequality  of  $A\vx\le  \vb$  has  size  bounded  by
  $t:=(n+1)s$, so the facet complexity is at most $t$.  The optimum of
  the primal  program is  attained at  a vertex, so  it is  bounded by
  $4n^2t$.   The bound  on the  dual solution  follows  similar.  Both
  imply a polynomial bound on the optimal value.
  %
  \else\texttt{missing}\fi%
\end{proof}%
\fi%
Refining  this, you could even  prove that if $\delta=\nicefrac{p}{q}$
is a completely reduced fraction, then $q$ is bounded by $n$ and $s$.
So   given any lower  bound  on  $\delta$, you   could prove that  the
solution space of a rational linear program is finite.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggo"
%%% End: 
